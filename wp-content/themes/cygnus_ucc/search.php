<div class="hero-classic-container" style="height: 315px;">
  <div class="hero-classic-img" style="background-image: url('https://www.ucc-coffee.co.uk/wp-content/uploads/2021/03/01_what_we_do_header_1600x912.jpg')"></div>
    <a class="brand" href="https://uccuk.uccstage.co.uk/">
        <img style="filter:brightness(200);" alt="UCC Coffee STAGE" data-src="https://uccuk.uccstage.co.uk/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg" class="logo lazyloaded" src="https://uccuk.uccstage.co.uk/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg"><noscript><img class="logo" src="https://uccuk.uccstage.co.uk/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg" alt="UCC Coffee STAGE"></noscript>
    </a>
    </div>
</div>

<div class="container-fluid container-fluid-cap mt-5 mb-3">
  <div class="row">
    <div class="col col-sm-12 thick-pad-l thick-pad-r search-heading">
      <?php get_template_part('templates/page', 'header'); ?>
    </div>
  </div>
</div>


<div class="container-fluid container-fluid-cap">
  <div class="row">
    <div class="col col-sm-12 thick-pad-l thick-pad-r">
      <?php if (!have_posts()) : ?>
        <div class="alert alert-warning">
          <?php _e('Sorry, no results were found.', 'sage'); ?>
        </div>
        <?php get_search_form(); ?>
      <?php endif; ?>
      <?php $types = array('page', 'post', 'case_studies', 'equipment', 'coffee', 'side_products');
    foreach( $types as $type ){
        ?><div class="search-container for-<?php echo $type; ?>"> <h5 style="margin-top:50px"><?php $obj = get_post_type_object( $type ); echo $obj->labels->singular_name; ?></h5> <hr /><?php
        while( have_posts() ){
            the_post();
            if( $type == get_post_type() ){
              get_template_part('templates/content', 'search');
            }
        }
        rewind_posts();
        ?></div> <?php
    } ?>


      <?php the_posts_navigation(); ?>
    </div>
  </div>
</div>
