<section class="scrollable-section">
    <div class="container-fluid container-fluid-cap">

        <div class="hero-classic-container">
            <div class="hero-classic-img"
                 style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/404-bg-image.jpg')"></div>

            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>

            <div class="hero-classic-content">
                <h1 class="title red-title">Sorry, but the page you were trying to view does not exist.</h1>
                <div class="line-container thick" data-hook="1">
                    <div class="fill-line red-line"></div>
                </div>
            </div>
        </div>

    </div>
</section>