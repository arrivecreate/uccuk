<?php
/**
 * Single-post.php to override the single.php page.
 */

while (have_posts()) : the_post();

    ?>

    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <div class="hero-classic-container">
                <div class="hero-classic-img"
                     style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/blog-page-pattern.png')"></div>

                <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
                </a>

                <div class="current-page-container">
                    <?php
                        if(strlen(get_the_title()) > 12){
                            $breadcrumb = substr(get_the_title(), 0 , 12) . "...";
                        }else{
                            $breadcrumb = get_the_title();
                        }
                    ?>

                    <span class="page-breadcrumb">Post - <?php echo $breadcrumb; ?></span>
                </div>

                <div class="hero-classic-content">
                    <p class="date"><?php the_date("j F Y"); ?></p>
                    <div class="line-container thick">
                        <span class="fill-line red-line"></span>
                    </div>
                    <h1 class="title"><?php echo get_the_title(); ?></h1>
                    <?php if (get_field('primary_content')) : ?>
                        <div class="subcontent"> <?php echo get_field('primary_content'); ?> </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </section>

    <section class="section-scrollable single-post-section post-main-container">
        <div class="container-fluid container-fluid-cap">
            <div class="row">

                <?php
                //we retrieve the featured image link
                $thumb_id = get_post_thumbnail_id();
                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                $thumb_url = $thumb_url_array[0];

                //and we check if the thumb is going to default and we have also a secondary image,
                //then we will prefer it to create the mirror quote in this case
                $secondary_image = get_field('secondary_image');
                if(strpos($thumb_url, "wp-includes/images/media/default.png") && !empty($secondary_image)){
                    $image = $secondary_image;
                }else{
                    $image = $thumb_url;
                }

                if(!strpos($image, "wp-includes/images/media/default.png")){
                    //we change the rules for content-col so that it takes right amount of space

                    ?>

                    <div class="image-col col col-12 col-md-6 thick-pad">
                        <?php
                        $text_alignment = "left";
                        $quote = get_field('quote');
                        $vertical_alignment = "bottom";
                        $box_color = "dark-lines";
                        include('templates/partials/components/mirror-quote.php');
                        ?>

                        <?php
                        $rows_loop_name = "links";
                        $color_class = "dark-btn";
                        include('templates/partials/components/link-row-container.php');
                        ?>
                    </div>

                <?php
                    $content_col_class = "col col-12 col-md-6 right-pad-for-menu";
                    } else {
                    $content_col_class = "full-width-col col col-11 col-md-9";
                }?>
                <div class="content-col <?php echo $content_col_class; ?>">
                    <div class="subcontent">
                        <?php
                            if (get_field('secondary_content')) :
                                echo get_field('secondary_content');
                            else:
                                if (get_the_content()) :
                                    the_content();
                                endif;
                            endif;
                        ?>
                    </div>

                    <div class="prev-and-next-container">
                        <?php previous_post_link('%link', '<span class="prev-post-link" data-hover="Previous"><i class="fas fa-angle-right"></i></span>'); ?>
                        <?php next_post_link('%link', '<span class="next-post-link"  data-hover="Next"><i class="fas fa-angle-left"></i></span>'); ?>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <?php
endwhile;