<?php
/**
 * Template Name: Maintenance Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Two Column Vertical stuff here!
     */
    $outer_container_required = true;
    include('templates/partials/image-hero.php');

    get_template_part('templates/partials/two-column-vertical', 'two-column-vertical');


endwhile; ?>
