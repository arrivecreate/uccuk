<?php
/**
 * Template Name: Branding and Insights Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Two Column Vertical stuff here!
     */

    $outer_container_required = true;
    include('templates/partials/image-hero.php');

    $two_column_regular = true;
    include('templates/partials/two-column-vertical.php');

endwhile; ?>
