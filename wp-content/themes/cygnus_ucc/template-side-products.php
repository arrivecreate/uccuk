<?php
/**
 * Template Name: Side Products Template
 */
?>

<?php while (have_posts()) :
    the_post();

    /**
     * Call the Hero and Sub Hero
     */
    ?>

    <div class="container-fluid container-fluid-cap">
        <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
    </div>

    <?php
    $left_right_switch = "left";
    include('templates/partials/sub-hero.php');
    ?>

    <section class="section-scrollable categories-section">
        <div class="container-fluid container-fluid-cap category-container-fluid">
            <div class="row category-row thick-pad">
                <?php
                $terms = get_custom_categories_list('side-products-category');
                $section_index = 0;
                foreach ($terms as $term) :

                    $cat = get_custom_categories_info('side-products-category', $term)[0];

                    /*******************************************
                     * We prepare an array that will contain all the subcategories as we will need the loop in a couple of places
                     * but in different containers
                     */


                    /**
                     * Loop through the custom post type and display listing information
                     */
                    $loop = new WP_Query(
                        array(
                            'post_type' => 'side_products',
                            'post_status' => 'publish',
                            'posts_per_page' => -1,
                            'tax_query' => array(
                                array(
                                    'taxonomy' => 'side-products-category',
                                    'field' => 'name', // term_id, slug
                                    'terms' => $term
                                ),
                            ),
                        )
                    );

                    $cat_children = array();  //the container for all the subcategories


                    if ($loop->have_posts()) :
                        while ($loop->have_posts()) : $loop->the_post();

                            if (have_rows('listing_information')):
                                while (have_rows('listing_information')) : the_row();

                                    $side_brand = array();
                                    $side_brand['site_product_title'] = get_the_title();
                                    $side_brand['post_link'] = get_permalink();
                                    $side_brand['main_image'] = get_image_url(get_sub_field('main_image'));
                                    $side_brand['quote'] = get_sub_field('quote');
                                    $side_brand['logo_image'] = get_image_url(get_sub_field('logo_image'));
                                    $side_brand['links'] = array();


                                    if (have_rows('links')):
                                        while (have_rows('links')) : the_row();
                                            $link = array();
                                            $link['link_type'] = get_sub_field('link_type');
                                            $link['link_text'] = get_sub_field('link_text');
                                            $link['link'] = get_sub_field($link['link_type'] . 'ternal_link');

                                            array_push($side_brand['links'], $link);
                                        endwhile;
                                    endif;

                                    array_push($cat_children, $side_brand);

//                        $rows_loop_name = "links";
//                        $color_class = "dark-btn";
//                        include('templates/partials/components/link-row-container.php');

                                endwhile;
                            endif;

                        endwhile;
                    endif;


                    /*************************************************
                     * Using both the information from the category and the array with the posts
                     * we template everything
                     ************************************************/

                    $additional_classes = "";

                    if ($section_index < 2) {
                        $additional_classes .= "first-two-side-products-category";
                    }
                    ?>

                    <div class="col col-12 col-md-6">
                        <div class="section-scrollable side-products-category <?php echo $additional_classes; ?>">
                            <div class="container-fluid container-fluid-cap">
                                <div class="row">
                                    <div class="image-col col col-12 col-md-6">
                                        <div class="image-container" style="background-image: url('<?php echo $cat_children[0]['main_image']; ?>');"></div>
                                        <div class="mirror-images-collector">
                                            <?php
                                            $j = 0;
                                            foreach ($cat_children as $subcat) {
                                                if ($j == 0) {
                                                    $active = "active";
                                                } else {
                                                    $active = "";
                                                } ?>
                                                <div class="image-container <?php echo $active; ?>" data-image="<?php echo $subcat['main_image']; ?>"></div>
                                                <?php $j++;
                                            } ?>
                                        </div>
                                    </div>
                                    <div class="content-col col col-12 col-sm-10 col-md-6">
                                        <?php $title = $cat->name;
                                        if (!empty($title)):?>
                                            <h2 class="title"><?php echo $title; ?></h2>
                                        <?php endif; ?>

                                        <?php //DESCRIPTION
                                        $description = $cat->description;
                                        if (!empty($description)):?>
                                            <div class="subcontent"><?php echo $description; ?></div>
                                        <?php endif; ?>

                                        <div class="row cta-row">
                                            <?php if (count($cat_children) >= 2) { ?>
                                                <div class="select-col col align-self-center">
                                                    <div class="select-subcategory-container">
                                                        <div class="select-a-brand-placeholder">
                                                            <p class="select-a-brand-p">Select a brand</p>
                                                        </div>
                                                        <ul class="select-a-brand-list">
                                                            <?php
                                                            $i = 0;
                                                            foreach ($cat_children as $subcat) {
                                                                if ($i == 0) {
                                                                    $active = "active";
                                                                } else {
                                                                    $active = "";
                                                                };
                                                                $i++;
                                                                echo "<li class='brand " . $active . "'>" . $subcat['site_product_title'] . "</li>";
                                                            }
                                                            ?>
                                                        </ul>
                                                    </div>
                                                </div>
                                            <?php } else { ?>
                                                <div class="select-col col align-self-center no-brands-selection"></div>
                                            <?php } ?>


                                            <div class="logo-col col">
                                                <div class="logo-lift">
                                                    <?php foreach ($cat_children as $subcat) { ?>
                                                        <div class="logo-container" style="background-image:url('<?php echo $subcat['logo_image']; ?>');"></div>
                                                    <?php } ?>
                                                </div>
                                            </div>

                                            <?php /* ?>
                            <div class="select-the-range-col col align-self-center d-none d-sm-none d-md-none d-lg-block">
                                <?php foreach ($cat_children as $subcat) { ?>
                                    <a href="<?php echo $subcat['post_link']; ?>" class="red-bg-text select-the-range-anchor">
                                        <div class="link-btn white-btn">
                                            <span class="link-text">See the range</span>
                                        </div>
                                    </a>
                                <?php } ?>
                            </div>
                            <?php */ ?>
                                        </div>


                                        <div class="link-switch-container">
                                            <div class="sliding-row-outer-lift">
                                                <?php
                                                foreach ($cat_children as $subcat) {
                                                    $links = $subcat['links'];

                                                    if (!empty($subcat['links'])) {
                                                        ?>
                                                        <div class="sliding-row-container">
                                                            <div class="row link-row-container">
                                                                <?php foreach ($links as $link) {
                                                                    $link_type = $link['link_type'];
                                                                    $link_text = $link['link_text'];
                                                                    $link = $link['link'];

                                                                    $rows_loop_name = "links";
                                                                    $color_class = "white-btn dark-text-hover";
                                                                    $display_class = "d-inline-block";
                                                                    include('templates/partials/components/link-btn.php');
                                                                } ?>
                                                            </div>
                                                        </div>
                                                    <?php }
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    wp_reset_postdata();
                    $section_index++;

                endforeach;
                ?>

            </div>
        </div>
    </section>
    <?php

    /**
     * Call the divided colour block - single ACFs
     */
    get_template_part('templates/partials/divided-colour-block', 'divided-colour-block');

endwhile; ?>

