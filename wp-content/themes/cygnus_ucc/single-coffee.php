<?php

/**
 * This single is specific to the Coffee
 */

while (have_posts()) : the_post();

    //Loop through the main information group
    if (have_rows('main_information')):
        while (have_rows('main_information')) : the_row();

          //Get the field type for custom template layouts
          $field_type = get_field("template_type");
          switch ($field_type) {
            case 'grand_cafe': ?>
                 <style>
                  /* background image */
                  body {
                    background-image: url("<?php echo get_template_directory_uri().'/dist/images/ucc_textured_background.jpg' ?>");
                  }
                  p {
                    font-family: neue-haas-grotesk-display, sans-serif;
                    font-size: 0.82rem;
                    color: #51494B;
                    line-height: 1.1rem;
                  }
                  .footer-red p {
                    color:white !important;
                  }
                @font-face {
                    font-family: 'veneerfontthree';
                    src: url('<?php echo get_template_directory_uri(); ?>/dist/fonts/VeneerThree/font.woff2') format('woff2'),
                            url('<?php echo get_template_directory_uri(); ?>"/dist/fonts/VeneerThree/font.woff') format('woff');
                    font-weight: normal;
                    font-style: normal;
                    font-display: block;
                }
                </style>
              <?php break;
          }

          /* We add here an style tag that will allow us to change the colour of the components if available*/
            if(!empty(get_sub_field("bg-colour"))) { $bg_colour = get_sub_field("bg-colour"); } else { $bg_colour = "#333d47"; }
            if(!empty(get_sub_field("titles-colour"))) { $titles_colour = get_sub_field("titles-colour"); } else { $titles_colour = "#9B9B9B"; }
            if(!empty(get_sub_field("text-colour"))) { $text_colour = get_sub_field("text-colour"); } else { $text_colour = "#FFFFFF"; }
            if(!empty(get_sub_field("component-colour"))) { $component_colour = get_sub_field("component-colour"); } else { $component_colour = "#9B9B9B"; }
            ?>

            <style>
                <?php if ($field_type == 'default') { ?>
                  /* Common text colour and common background*/
                  .single-coffee section.parent-sliding-section,
                  .single-coffee section.product-type-section {
                      background-color : <?php echo $bg_colour; ?>;
                      color            : <?php echo $text_colour; ?>;
                  }

                /* Component colours */
                .custom-cpt-hero .line-container .fill-line,
                .first-coffee-case-section .mirror-box .mirror-box-line {
                  background-color : #51494B;
                }
                /* Hover change of text color on hover */
                .parent-sliding-section .link-btn:hover,
                .product-type-section .link-btn:hover {
                  color : <?php echo $bg_colour; ?>;
                }


                <?php } ?>
                <?php if ($field_type == 'grand_cafe') { ?>
                  @media screen and (max-width: 767px) {
                    .single-coffee .custom-cpt-hero .hero-classic-content .coffee-logo {
                      margin-top: 8em;
                    }
                  }
                .single-coffee section.last-rail-section {
                  padding-top:0px;
                }
                  .veneer-text {
                    font-family: veneerfontthree,Trade Gothic,sans-serif !important;
                    color: #493722;
                  }
                  .product-type-section p, .ssubcontent p, .ssubcontent h3 {
                    color: #493722;
                  }
                  .custom-cpt-hero.grand_cafe .content-col .hero-classic-content {
                    background: transparent !important;
                  }
                  .grand-cafe a.brand {
                    padding: 6.5rem 15px 4.5rem 2.5rem;
                  }
                  .grand-cafe .custom-cpt-hero .hero-classic-content .coffee-logo, .grand-cafe .custom-cpt-hero .hero-classic-content .equipment-logo {
                    max-width: 268px;
                    max-height: 224px;
                  }
                  .custom-cpt-hero .hero-classic-content {
                    min-height: 18rem;
                    padding: 6.5rem 15px 1.5rem 2.5rem;
                  }
                  .custom-cpt-hero .hero-line-container {
                    top: 6.5rem;
                    max-height: 312px;
                  }
                  .custom-cpt-hero .hero-classic-content.increase-hero-width {
                      width: 96%;
                      padding: 5.5rem 15px 5.5rem 2.5rem;
                  }
                  .custom-cpt-hero .hero-classic-content.increase-hero-width h1.title {
                    color: #51494B;
                    font-size: 2.2rem;
                    line-height: 2.3rem;
                    letter-spacing: 1px;
                    max-width: 550px;
                  }
                  .custom-cpt-hero .line-container .fill-line,
                  .first-coffee-case-section .mirror-box .mirror-box-line {
                    background-color : #51494B;
                  }
                  .single-coffee .custom-cpt-hero .subcontent {
                    margin-top: 1rem;
                  }
                  .custom-cpt-hero .hero-classic-content .title, .single-coffee .first-coffee-case-section .content-col .title {
                    font-family: Trade Gothic,sans-serif;
                    color: #51494B !important;
                    font-size: 1.375rem;
                    line-height: 1.4rem;
                    letter-spacing: 1px;
                  }
                  .link-btn .link-text {
                    font-family: Trade Gothic,sans-serif;
                  }
                  .image-col.col.col-12.col-md-5.order-1.order-md-1.thick-pad-l .mirror-quote-container .mirror-box,
                  .image-col.col.col-12.col-md-5.order-1.order-md-1.thick-pad-l .mirror-quote-container p.quote {
                    display: none;
                  }
                  .right-pad-for-menu .coffee-logo.img-fluid.lazyloaded {
                    margin-bottom: 2.5rem;
                  }
                  .right-pad-for-menu h3.title {
                    color: #51494B;
                    margin-bottom: 1rem;
                  }
                  /* Hover change of text color on hover */
                  .parent-sliding-section .link-btn:hover,
                  .product-type-section .link-btn:hover {
                    color : #ffffff;
                  }
                  .content-col.col.col-12.col-md-7.order-1.order-md-2.thick-pad-l.right-pad-for-menu {
                    top: 5rem;
                  }
                  .content-col.col.col-12.col-md-7.order-1.order-md-2.thick-pad-l.right-pad-for-menu h3.title {
                    margin-bottom: 1.5rem;
                  }
                  .product-type-section p {
                    line-height: 1.2rem !important;
                    margin-bottom: 1.3rem;
                  }
                  .single-coffee .product-type-section .content-col .title {
                    font-size: 1.3rem;
                    letter-spacing: 0;
                    line-height: 2.1rem;
                  }
                  .single-coffee #sliding-coffee-element .image-container {
                    width: 100%;
                    padding-bottom: 100%;
                  }
                  .single-coffee .parent-sliding-section .content-col .coffee-logo {
                    max-height: 225px;
                  }
                  .subcontent.inline-images {
                    text-align: right;
                  }
                  .subcontent.inline-images img {
                    display: inline-block;
                    margin: 2.5rem 0 1rem 2rem;
                  }

                  .container-fluid.container-fluid-cap[data-colour] {
                      position: relative;
                  }

                  .container-fluid.container-fluid-cap[data-colour]:after {
                      width: 20px;
                      height: 100%;
                      position: absolute;
                      content: '';
                      right: -15px;
                      top: 0px;
                      background-color: #7bb48c;
                      opacity: 0.65;
                  }

                  /* NEW Styles */

                  @media(min-width:768px) {
                    .minus-margin { margin-bottom: -1rem; }
                    .no-height { height: 0px; }
                  }
                  @media(max-width:768px) {
                    .single-coffee .product-type-section .image-col div.image-container {
                      width:100%;
                      max-width:100%;
                      padding-bottom:100%;
                    }
                    html, body {
                      max-width:100vw;
                      overflow-x:hidden;
                    }
                    .single-coffee .product-type-section .content-col .coffee-details-table,
                    .single-coffee .product-type-section .content-col .subcontent,
                    .single-coffee .product-type-section .content-col .subtitle {
                      width:100% !important;
                    }
                    .custom-cpt-hero .content-col .hero-classic-content {
                      padding:0px;
                    }
                    div.subcontent p {
                      font-size:1.25rem !important;
                      line-height:1.4 !important;
                    }
                    .single-coffee .product-type-section .content-col .title {
                      font-size: 3.3rem;
                      letter-spacing: 0;
                      line-height: 3.1rem;
                    }
                    .single-coffee .parent-sliding-section .content-col .coffee-logo+.title {
                      font-size: 12px;
                      line-height: 15px;
                    }
                    .parent-sliding-section div#parent-image-container {
                      width: 100%;
                      position: relative !important;
                      padding-bottom: 55%;
                    }
                    .title br {
                      display:inline-block;
                    }
                    .mobile-reverse {
                      flex-direction: row-reverse;
                    }
                    .mobile-col-reverse {
                      flex-direction: column-reverse;
                    }
                    .single-coffee .product-type-section .image-col div.inner-padding {
                      padding-bottom:100%;
                    }
                    .coffee-logo {
                      max-width:100% !important;
                    }
                    .custom-cpt-hero .hero-classic-content.increase-hero-width {
                      width:100% !important;
                    }
                    .row.no-height + .row .inner-padding, .row.z-index-top + .row .content-col {
                      padding-top: 0px;
                    }
                    .hero-classic-content.leaf-overlay:after {
                      top: auto;
                      bottom: -90px;
                      transform: rotate(331deg);
                      right: -40px;
                      left: auto;
                    }
                    .single-coffee .first-coffee-case-section .content-col .subcontent,
                    .single-coffee .first-coffee-case-section .content-col .title {
                      width:100% !important;
                    }
                  }
                  .minus-margin.no-height h1.title  {
                    font-size:4.7rem !important;
                    line-height: 1.1 !important;
                  }
                    .single-coffee .parent-sliding-section .content-col .title {
                      font-size: 1.5rem !important;
                      line-height: 2.6rem !important;
                      letter-spacing: 3px !important;
                  }
                    .product-type-section .subcontent p strong {
                      font-size:1rem;
                      font-weight:800;
                    }
                  .relative  {position:relative;}
                  .z-index-top {z-index:100;}
                  .align-self-flex-end {
                    -ms-flex-item-align: flex-end!important;
                    align-self: flex-end !important;
                  }


                  h3.title.veneer-text  {
                    font-size:3.5rem !important;
                    line-height: 1.1 !important;
    font-weight: 100;
                  }

                  .leaf-overlay:after {
                    content:'';
                    z-index:99;
                    background-image: url('/wp-content/uploads/2021/03/leaf-overlay.png');
                    width:62%;
                    height:calc(100% + 60px);
                    position:absolute;
                    top:-30px;
                    background-size:contain;
                    background-repeat:no-repeat;
                  }
                  .leaf-overlay, .up-leaf-overlay, .long-leaf-overlay {
                    position:relative;
                  }


                  .long-leaf-overlay:after {
                    content:'';
                    z-index:99;
                    background-image: url('/wp-content/uploads/2021/03/long-leaf-overlay.png');
                    width:62%;
                    height:calc(100% + 60px);
                    position:absolute;
                    background-size:contain;
                    background-repeat:no-repeat;
                  }

                  .up-leaf-overlay:after {
                    content:'';
                    z-index:99;
                    background-image: url('/wp-content/uploads/2021/03/up-leaf-overlay.png');
                    width:62%;
                    height:calc(100% + 60px);
                    position:absolute;
                    background-size:contain;
                    background-repeat:no-repeat;
                  }

                  .bottom-right-overlay:after {
                      left: 150%;
                      transform: translateX(-41%);
                      bottom: -60%;
                      right:auto;
                  }

                  @media(max-width:768px) {
                    .bottom-right-overlay:after {
                      right:-50px;
                      transform:translateX(-41%);
                      bottom:-50px;
                      left:auto;
                    }

                    .product-type-section div[class*="overlay"]:after {
                      right: auto !important;
                      left: 73% !important;
                      height: 90% !important;
                      transform: rotate(328deg);
                      top: 39%;
                    }
                  }

                  .left-overlay:after {
                    left:0px;
                    transform:translateX(-41%);
                  }

                  .leaf-2-overlay:after {
                    content:'';
                    z-index:99;
                    background-image: url('/wp-content/uploads/2021/03/leaf-overlay-2.png');
                    width:100%;
                    padding-top:54%;
                    position:absolute;
                    top:-30px;
                    background-size:contain;
                    background-repeat:no-repeat;
                  }
                  .leaf-2-overlay {
                    position:relative;
                  }

                  .bottom-left-overlay:after {
                    left: 0px;
                    transform: translateX(-14%);
                    bottom: -60px;
                    top: auto;
                  }

                  .single-coffee .parent-sliding-section {
                      padding-top: 5rem;
                  }

                  .single-coffee .product-type-section .image-col .inner-padding {
                    padding-bottom:55%;
                  }

                  .main, div[role=document], footer {
                    overflow-x: visible;
                  }

                  .subcontent p {
                    margin-bottom: .6rem;
                    font-size: 0.72rem;
                    line-height: 1.4;
                    letter-spacing: 0;
                  }


                  /* End NEW Styles */

                  .single-coffee .product-type-section .content-col .coffee-details-table, .single-coffee .product-type-section .content-col .subcontent, .single-coffee .product-type-section .content-col .subtitle, .single-coffee .product-type-section .content-col .title {
                    width: 75%;
                  }
                  .single-coffee .first-coffee-case-section .content-col .secondary-image {
                    background-position-y: center;
                  }
                  .single-coffee .first-coffee-case-section .image-col .mirror-quote-container {
                    bottom: -15rem;
                  }

                  @media screen and (max-width: 767px) {
                    .single-coffee .first-coffee-case-section .image-col .mirror-quote-container {
                      bottom: 0;
                    }
                  }

                  footer .phone-number-section .mirror-quote-container .quote {
                    font-family: Trade Gothic,sans-serif;
                    font-weight: 100;
                    font-size: 2.2rem;
                    line-height: 2.2rem;
                    text-align: right;
                  }
                <?php } ?>
                /* Banner title and first section title colour */
                .custom-cpt-hero .hero-classic-content .title,
                .single-coffee .first-coffee-case-section .content-col .title {
                    color : <?php echo $titles_colour; ?>;
                }

                .single-coffee .first-coffee-case-section .image-col .mirror-quote-container .quote {
                    color : <?php echo $component_colour; ?>;
                }

                .single-coffee .first-coffee-case-section .content-col .secondary-image {
                  height: 36rem;
                  bottom: -26rem;
                }

                /* Black tables */
                .single-coffee .product-type-section .content-col .coffee-details-table .table {
                      color: #000;
                  }

            </style>

            <?php
            //Loop through the hero section
            if (have_rows('hero_section')):
                while (have_rows('hero_section')) : the_row(); ?>

                    <div class="custom-cpt-hero <?= $field_type; ?>">
                        <div class="container-fluid container-fluid-cap">
                          <?php switch($field_type) {
                            case 'grand_cafe': ?>
                              <div class="row">
                                  <div class="current-page-container">
                                      <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                                  </div>

                                  <div class="container-fluid container-fluid-cap">
                                      <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                                          <img class="logo" src="<?php echo get_template_directory_uri() ?>/dist/images/ucc_grey_logo.svg" alt="<?php bloginfo('name'); ?>">
                                      </a>
                                  </div>

                                  <div class="line-col col col-12 col-md-2 order-2 order-md-1">
                                      <div class="line-container vertical-line hero-line-container thick">
                                          <div class="fill-line dark-line"></div>
                                      </div>
                                  </div>

                                  <div class="content-col col col-4 col-md-3 col-lg-3 order-3 order-md-2 align-self-center">
                                      <div class="inner-padding">
                                          <div class="hero-classic-content">
                                              <?php if (!empty(get_sub_field("logo"))) { ?>
                                                  <img src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="<?php echo get_sub_field('title'); ?> Logo" class="coffee-logo img-fluid">
                                              <?php } ?>
                                          </div>
                                      </div>
                                  </div>
                                  <div class="content-col col col-8 col-md-7 order-3 order-md-2 align-self-center d-block d-sm-block d-md-none">
                                    <div class="inner-padding">
                                      <div class="hero-classic-content increase-hero-width p-0 left-overlay leaf-overlay" data-offset="-170">
                                        <img class="mirror-image" style="max-width:100%" src="/wp-content/uploads/2021/03/forest-image.jpg">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="content-col col col-12 col-md-7 order-3 order-md-2 align-self-center d-none d-sm-none d-md-block">
                                    <div class="inner-padding">
                                      <div class="hero-classic-content" style="text-align: center;">
                                        <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/06/grand_cafe_group_shots_1.png" alt="Grande Cafe Group Shot">
                                      </div>
                                    </div>
                                  </div>

                                  <div class="content-col col col-12 col-md-7 order-3 order-md-2 align-self-center d-block d-sm-block d-md-none">
                                    <div class="inner-padding">
                                      <div class="hero-classic-content pt-0 pb-0" style="text-align: center; width:100%">
                                        <img style="max-width:100%;" src="<?php echo get_site_url(); ?>/wp-content/uploads/2018/06/grand_cafe_group_shots_1.png" alt="Grande Cafe Group Shot">
                                      </div>
                                    </div>
                                  </div>
                              </div>

                              <div class="row no-height minus-margin relative z-index-top">
                                  <div class="col col-md-8 offset-md-2">
                                    <h1 class="title veneer-text"><?php echo get_sub_field('title'); ?></h1>
                                  </div>
                              </div>

                              <div class="row">
                                <div class="content-col col col-12 col-md-7 order-3 order-md-2 align-self-center d-none d-sm-none d-md-block">
                                  <div class="inner-padding">
                                    <div class="hero-classic-content increase-hero-width p-0 left-overlay leaf-overlay" data-offset="-170">
                                      <img class="mirror-image" style="max-width:100%" src="/wp-content/uploads/2021/03/forest-image.jpg">
                                    </div>
                                  </div>
                                </div>
                                <div class="content-col col col-12 col-md-4 order-3 order-md-2 align-self-flex-end pt-0 pt-sm-0 pt-md-5">
                                  <div class="inner-padding pt-0 pt-sm-0 pt-md-5">
                                    <div class="hero-classic-content increase-hero-width p-0">
                                      <div class="subcontent subtitle"><?php echo get_sub_field('sub_content'); ?></div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <?php break;

                            default: ?>
                              <div class="row">
                                  <div class="image-col col col-12 col-md-3 order-1 order-md-3 common-pad-r">
                                      <div class="hero-classic-img" style="background-image: url('<?php echo get_image_url(get_sub_field('side_image')); ?>')"></div>
                                  </div>

                                  <div class="current-page-container">
                                      <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                                  </div>

                                  <div class="container-fluid container-fluid-cap">
                                      <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                                          <?php switch($field_type) {
                                            case 'grand_cafe':
                                              $logo_url = "/dist/images/ucc_grey_logo.svg";
                                              break;
                                            default:
                                              $logo_url = "/dist/images/ucc-logo.svg";
                                          } ?>
                                          <img class="logo" src="<?php echo get_template_directory_uri().$logo_url; ?>" alt="<?php bloginfo('name'); ?>">
                                      </a>
                                  </div>

                                  <div class="line-col col col-12 col-md-3 order-2 order-md-1">
                                      <div class="line-container vertical-line hero-line-container thick">
                                          <div class="fill-line dark-line"></div>
                                      </div>
                                  </div>

                                  <div class="content-col col col-12 col-md-8 col-lg-6 order-3 order-md-2 align-self-center">
                                      <div class="inner-padding">
                                          <div class="hero-classic-content">
                                              <?php if (!empty(get_sub_field("logo"))) { ?>
                                                  <img src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="<?php echo get_sub_field('title'); ?> Logo"
                                                       class="coffee-logo img-fluid">
                                              <?php } ?>
                                              <h1 class="title"><?php echo get_sub_field('title'); ?></h1>
                                              <div class="line-container thick">
                                                  <div class="fill-line dark-line"></div>
                                              </div>
                                              <div class="subcontent subtitle"><?php echo get_sub_field('sub_content'); ?></div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            <?php } ?>
                        </div>
                    </div>

                <?php endwhile;
endif;

//Loop through the first section
if (have_rows('section_one')):
    while (have_rows('section_one')) : the_row(); ?>


    <?php switch($field_type) {
      case 'grand_cafe': ?>

        <section class="section-scrollable first-coffee-case-section light-background">
            <div class="container-fluid container-fluid-cap">


              <div class="row relative z-index-top pt-5 pl-0 d-none d-sm-none d-md-block">
                  <div class="col col-md-8 offset-md-1">
                    <h3 class="title veneer-text"><?php echo get_sub_field('title'); ?></h3>
                  </div>
              </div>

                <div class="row mobile-col-reverse">

                  <div class="content-col col col-12 col-md-5 thick-pad-l right-pad-for-menu pl-md-0 pr-md-0 pt-md-2 pb-0 offset-md-1">

                      <h3 class="title veneer-text mt-5 d-block d-sm-block d-md-none"><?php echo get_sub_field('title'); ?></h3>
                      <p><a href="https://www.conservation.org/" target="_blank"><img width="110" src="/wp-content/uploads/2020/11/GC_Conserv-Logo.png" alt="Conservation International Logo"></a></p>
                      <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                      <?php // LINKS
                      $rows_loop_name = "links";
                      $color_class = "dark-btn";
                      include('templates/partials/components/link-row-container.php');
                      ?>

                      <!-- <div class="secondary-image slide-in-img slide-from-bottom-img" style="background-image:url('<?php echo get_image_url(get_sub_field("secondary_image")); ?>');"></div> -->

                      <!-- <div class="subcontent inline-images">
                        <p>
                          <img width="260" src="/wp-content/uploads/2018/06/GC_Climate-Logo.png" alt="Climate Neutral Group Logo">
                        </p>
                      </div> -->
                  </div>

                  <div class="col col-12 col-md-6">
                    <div class="hero-classic-content increase-hero-width p-0 bottom-left-overlay leaf-2-overlay mt-5 mt-sm-5 mt-md-0" data-offset="-170">
                      <img class="mirror-image" style="max-width:100%" src="/wp-content/uploads/2021/03/man-in-forest-image.jpg">
                    </div>
                  </div>
                </div>
            </div>
        </section>

        <!-- <section class="section-scrollable first-coffee-case-section light-background mt-5">
          <div class="container-fluid container-fluid-cap">
            <div class="row">
              <div class="col col-12 col-md-6 offset-md-1 align-self-center">
                <img style="max-width:100%;" src="<?php echo get_template_directory_uri() ?>/dist/images/grand_cafe_group_shots.png" alt="Grande Cafe Group Shot">
              </div>
              <div class="col col-12 col-md-5 align-self-center">

                <img src="/wp-content/uploads/2018/06/gcc_logo_big.png" alt="Grand Cafe Logo" class="coffee-logo img-fluid">
                <h5 class="mt-3 title" style="font-weight: 800;text-transform: uppercase;">Explore our range</h5>

              </div>
            </div>
          </div>
        </section>
-->
        <?php break;

      default: ?>


      <section class="section-scrollable first-coffee-case-section light-background">
          <div class="container-fluid container-fluid-cap">
              <div class="row">
                  <div class="image-col col col-12 col-md-5 order-1 order-md-1 thick-pad-l">
                      <?php
                      $text_alignment = "left";
                      $image = get_image_url(get_sub_field('image'));
                      $quote = get_sub_field('quote');
                      $vertical_alignment = "bottom";
                      $data_offset = "-170";
                      include('templates/partials/components/mirror-quote.php');
                      ?>
                  </div>
                  <div class="content-col col col-12 col-md-7 order-1 order-md-2 thick-pad-l right-pad-for-menu">

                      <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                      <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                      <?php // LINKS
                      $rows_loop_name = "links";
                      $color_class = "dark-btn";
                      include('templates/partials/components/link-row-container.php');
                      ?>

                      <div class="secondary-image slide-in-img slide-from-bottom-img" style="background-image:url('<?php echo get_image_url(get_sub_field("secondary_image")); ?>');"></div>

                      <div class="subcontent inline-images">
                        <p>
                          <img width="260" src="/wp-content/uploads/2020/11/GC_Climate-Logo.png" alt="Climate Neutral Group Logo">
                          <img width="183" src="/wp-content/uploads/2020/11/GC_Conserv-Logo.png" alt="Conservation International Logo">
                        </p>
                      </div>
                  </div>

              </div>
          </div>
      </section>

      <?php } ?>

    <?php endwhile;
endif;

//we find out if a quote is available for the last rail section, if not, we are going to assign
//the last rail class to the last element in the product list
$third_section_content = "";
if (have_rows('section_three')):
    while (have_rows('section_three')) : the_row();
        $third_section_content = get_sub_field('content');
    endwhile;
endif;

//Loop through the sub sections
if (have_rows('section_two')):
    while (have_rows('section_two')) : the_row();

        $main_image = get_image_url(get_sub_field('main_image'));
        if (is_array($main_image)) { //$main_image might be an array or an image, we check for that
            $main_image = $main_image['url'];
        }

        // Loop the Parent Section Repeater
        if (have_rows('parent_section')):
            while (have_rows('parent_section')) : the_row(); ?>

                <?php
                $parent_image = get_image_url(get_sub_field('background_image'));
                if (is_array($parent_image)) { //$parent_image might be an array or an image, we check for that
                    $parent_image = $parent_image['url'];
                }
                ?>
                <?php if($field_type == "grand_cafe") { ?>
                <section class="section-scrollable parent-sliding-section">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row mobile-reverse">
                            <div class="image-col col col-8 col-md-6 offset-md-1">
                                <div class="inner-padding">
                                    <div id="parent-bg-image-container" class="background-image-container"
                                         style="background-image: url('<?php echo $parent_image; ?>')"></div>
                                    <div id="parent-image-container" class="image-container" style="background-image: url('<?php echo $main_image; ?>')"></div>
                                    <div id="sliding-coffee-element"></div>
                                </div>
                            </div>
                            <div class="content-col col col-4 col-md-4 pt-0 pb-0 pr-0 align-self-center">
                                <img src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Logo" class="coffee-logo img-fluid">
                                <h3 class="title mt-2 veneer-text"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php if(get_sub_field('secondary_title')){ ?><h3 class="title secondary-title"><?php echo get_sub_field('secondary_title'); ?></h3><?php } ?>
                                <?php if(get_sub_field('secondary_sub_content')){?><div class="subcontent secondary-subcontent"><?php echo get_sub_field('secondary_sub_content'); ?></div><?php } ?>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "dark-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                              </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php } else { ?>
                <section class="section-scrollable parent-sliding-section">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="image-col col col-12 col-md-6">
                                <div class="inner-padding">
                                    <div id="parent-bg-image-container" class="background-image-container"
                                         style="background-image: url('<?php echo $parent_image; ?>')"></div>
                                    <div id="parent-image-container" class="image-container" style="background-image: url('<?php echo $main_image; ?>')"></div>
                                    <div id="sliding-coffee-element"></div>
                                </div>
                            </div>
                            <div class="content-col col col-12 col-md-6 right-pad-for-menu">
                                <img src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Logo" class="coffee-logo img-fluid">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php if(get_sub_field('secondary_title')){ ?><h3 class="title secondary-title"><?php echo get_sub_field('secondary_title'); ?></h3><?php } ?>
                                <?php if(get_sub_field('secondary_sub_content')){?><div class="subcontent secondary-subcontent"><?php echo get_sub_field('secondary_sub_content'); ?></div><?php } ?>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "dark-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                              </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php } ?>

                <?php
            endwhile;
        endif;


        $last_rail = ($third_section_content) ? "" : "last-rail-section";
        $product_list_count = 0;
        if (have_rows('product_list')):
            while (have_rows('product_list')) : the_row(); $product_list_count++; endwhile;
        endif;

        // Loop the Product List Repeater
        if (have_rows('product_list')): $i = 0;
            while (have_rows('product_list')) : the_row(); ?>

                <section class="section-scrollable product-type-section <?php if($field_type == "grand_cafe") { ?>my-5<?php } ?> <?php echo $i; ?> <?php echo ($i == $product_list_count - 1) ? $last_rail : ""; ?>">
                    <div class="container-fluid container-fluid-cap product-<?php echo $i; ?>">
                       <?php if($field_type == "grand_cafe") { ?>
                         <div class="row d-none d-sm-none d-md-block" style="position: relative;z-index: 1;">
                           <div class="col col-md-5 offset-md-6">
                             <h3 class="veneer-text title mb-0"><?php echo get_sub_field('title'); ?></h3>
                           </div>
                         </div>
                       <?php } ?>
                          <div class="row">
                          <?php if($field_type == "grand_cafe") { 
                            $class = "col-md-6 offset-md-1 p-0"; 
                            if($i % 2 == 0) { 
                              $class .= " leaf-overlay left-overlay";
                            } else {
                              $class .= " bottom-right-overlay";
                              if($i == 1) {
                                $class .= " up-leaf-overlay";
                              } else if($i == 3) {
                                $class .= " long-leaf-overlay";
                              }
                            }
                          } else { 
                            $class = "col-md-6"; 
                          } ?>
                            <div class="image-col col col-12 <?php echo $class; ?>">
                                <div class="inner-padding">
                                    <div class="bg-image" style="background-image: url('<?php echo get_image_url(get_sub_field('overlay_image')); ?>');">
                                        <?php
                                        $line_bg_color = get_sub_field("colour");
                                        if (!empty($line_bg_color)) {
                                            echo "<style>.coffee-type-line .fill-line[data-colour='" . $line_bg_color . "']{ background-color: #" . $line_bg_color . " }</style>"
                                            ?>
                                            <div class="line-container coffee-type-line">
                                                <div class="fill-line" data-colour="<?php echo $line_bg_color; ?>"></div>
                                            </div>
                                            <?php
                                        } ?>
                                    </div>
                                    <div class="image-container"
                                         style="background-image: url('<?php echo get_image_url(get_sub_field("product_image")); ?>')"></div>
                                </div>
                            </div>
                            <?php if($field_type == "grand_cafe") { $class = "col-md-4"; } else { $class = "col-md-6 right-pad-for-menu"; } ?>
                            <div class="content-col col col-12 <?php echo $class; ?>">
                                <h3 class="title <?php if($field_type == "grand_cafe") { ?>veneer-text d-block d-sm-block d-md-none<?php } ?>"><?php echo get_sub_field('title'); ?></h3>
                                <h4 class="subtitle"><?php echo get_sub_field('sub_title'); ?></h4>
                                <div class="subcontent <?php if($field_type == "grand_cafe") { ?>ml-md-5 ml-sm-0 ml-0<?php } ?>"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                if (have_rows('info')):?>
                                    <div class="coffee-details-table">
                                        <div class="table">
                                            <?php while (have_rows('info')) : the_row(); ?>
                                                <div class="trow">
                                                    <div class="td td-label">
                                                        <?php echo get_sub_field('column_one'); ?>
                                                    </div>
                                                    <div class="td td-details">
                                                        <?php echo get_sub_field('column_two'); ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>

                                <?php if($field_type != "grand_cafe") { ?>
                                  <?php if(get_the_ID() == "2093") { ?>
                                    <div class="row link-row-container flex-row ">
                                      <div class="col-float">
                                        <a href="#" aria-label="Join our mission" class="link-btn dark-btn d-inline-block let-s-talk-link">
                                          <span class="link-text">Join our mission</span>
                                        </a>
                                      </div>
                                    </div>
                                  <?php } else { ?>
                                    <div class="row link-row-container flex-row ">
                                      <div class="col-float">
                                        <a href="#" aria-label="Let's Talk" class="link-btn dark-btn d-inline-block let-s-talk-link">
                                          <span class="link-text">Let's Talk</span>
                                        </a>
                                      </div>
                                    </div>
                                  <?php } ?>
                                <?php } ?>

                            </div>
                        </div>
                    </div>
                </section>
                <?php if($field_type == "grand_cafe") { ?>
                <style>
                  .product-<?php echo $i; ?> {
                    position:relative; 
                  }
                  .product-<?php echo $i; ?>:after {
                      width: 20px;
                      height: 100%;
                      position: absolute;
                      content: '';
                      right: -15px;
                      top: 0px;
                      background-color: <?php echo get_sub_field('colour'); ?>;
                      opacity: 0.65;
                  }
                  @media(max-width:768px) {
                    .product-<?php echo $i; ?>:after {
                      width: 100%;
                      height: 20px;
                      position: absolute;
                      content: '';
                      right: 0px;
                      top: 0px;
                      background-color: <?php echo get_sub_field('colour'); ?>;
                      opacity: 0.65;
                    }
                </style>
                <?php } ?>
                <?php
            $i++;
            endwhile;
        endif;

    endwhile;
endif;

if (have_rows('section_three')):
    while (have_rows('section_three')) : the_row(); ?>

        <?php if(get_sub_field('content')): ?>
        <section class="section-scrollable last-rail-section light-background">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col col col-12 col-md-6">
                        <div class="inner-padding">
                            <div class="image-container" style="background-image: url('<?php echo get_image_url(get_sub_field("image")); ?>')"></div>
                        </div>
                    </div>
                    <div class="content-col col col-12 col-md-5 offset-md-1 right-pad-for-menu align-self-center">

                        <div class="subcontent">
                            <div class="quote-content">
                                <span class="quote quote-open fa fa-quote-left"></span>
                                <?php echo get_sub_field('content'); ?>
                                <span class="quote quote-close fa fa-quote-right"></span>
                            </div>
                            <p class="name"><?php echo get_sub_field('name'); ?></p>
                            <p class="job-role"><?php echo get_sub_field('job_title'); ?></p>
                            <?php
                            $rows_loop_name = "links";
                            $color_class = "dark-btn";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
            endif;
    endwhile;
endif;

endwhile;
endif;

endwhile;
