/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function () {
                // JavaScript to be fired on all pages

                /*******************************************************
                 *CYGNUS JS - GENERAL
                 ******************************************************/

                /*===============================================================
                 * *SIDENAV
                 ==============================================================*/
                var header = $("header.main-header"),
                    headerContainer = header.children(".container-fluid"),
                    nav = $("#side-nav"),
                    fullScreenTrigger = $(".invisible-trigger.navigation-xxl-trigger");

                //we immediately set the right positioning for the nav, and we set to do so on resize
                repositionNav();
                $(window).resize(function () {
                    repositionNav();
                });

                //small intro (the nav start with css opacity on 0, this way, there is no jump
                //when we set the right position
                TweenLite.fromTo(nav, 0.55, {
                    autoAlpha: 0,
                    y: "-55%",
                    x: "50%"
                }, {
                    autoAlpha: 1,
                    y: "-50%",
                    x: "50%"
                });

                function repositionNav() {
                    if (fullScreenTrigger.is(":visible")) {
                        TweenLite.set(nav, {
                            right: ($(window).width() - headerContainer.width()) / 2
                        });
                    } else {
                        TweenLite.set(nav, {
                            right: "3.6rem"
                        });
                    }
                }


                /*===============================================================
                 * MENU
                 ==============================================================*/
                var $body = $("body"),
                    menuBtn = $("#menu-trigger"),
                    mainMenu = $("#main-menu"),
                    menuClose = mainMenu.find(".close-btn"),
                    navFirstGeneration = mainMenu.find(".main-nav > li"),
                    menuBgAnimator = $("#menu-bg-animator"),
                    menuBtnCoordinates = {
                        top: 0,
                        right: 0
                    },
                    menuTimeline = new TimelineMax();


                /*=========================
                 ****  *PREPARE MENU  ****/
                var subMenus = mainMenu.find(".sub-menu");
                $("<span class='plus-opener'><i class='fas fa-plus'></i></span>").insertBefore(subMenus);


                /*
                 This bit prepares an array of elements that specifies which elements should be open
                 so that when we open the menu we can click on them automatically and present them to the user
                 */
                var currentOpeningTree = [],
                    startingPoint = $(".current-menu-item");
                if (startingPoint.length) {
                    findClosestOpenParent(startingPoint);
                    currentOpeningTree.reverse();
                }
                function findClosestOpenParent(elem) {
                    var closestLiParent = elem.parent().closest("li.menu-item-has-children");
                    if (closestLiParent.length) {
                        currentOpeningTree.push(elem.parent().closest("li.menu-item-has-children"));
                        findClosestOpenParent(closestLiParent);
                    }
                }


                /***********************/
                /*  NAV DOTS HANDLING  */
                var navSectionsController = new ScrollMagic.Controller(),
                    sideNav = $("nav#side-nav"),
                    dotsList = sideNav.find(".dots"),
                    sectionsPh = $("section.section-scrollable");

                // change behaviour of controller to animate scroll instead of jump
                navSectionsController.scrollTo(function (newpos) {
                    TweenMax.to(window, 0.5, {scrollTo: {y: newpos}});
                });

                sectionsPh.each(function(index, elem){
                    //for each section retrieved we add a button in the nav
                    dotsList.append("<li></li>");
                });

                //we set the first element as active (if the page is already at a major scrolling point, it will be overwritten)
                dotsList.children("li").first().addClass("active");

                sectionsPh.each(function (index, elem) {
                    var $elem = $(elem);

                    //we assign the event both on enter and on leave
                    var scene = new ScrollMagic.Scene({
                        triggerElement: $elem.get(0),
                        triggerHook: 0.25
                    })
                        .on('enter', function (event) {
                            var currentIndex = sectionsPh.index($elem);
                            dotsList.children("li").removeClass("active").eq(currentIndex).addClass("active");


                        })
                        .on('leave', function (event) {
                            var currentIndex = sectionsPh.index($elem) - 1;
                            if(currentIndex < 0){
                                currentIndex = 0;
                            }
                            dotsList.children("li").removeClass("active").eq(currentIndex).addClass("active");
                        })
                        .addTo(navSectionsController);
                });

                var dotsLi = dotsList.children("li");
                dotsLi.click(function(e){
                    e.preventDefault();

                    var $this = $(this),
                        currentIndex = dotsLi.index($this);
                    console.log(sectionsPh.eq(currentIndex));
                    if (sectionsPh.eq(currentIndex).length > 0) {

                        console.log(sectionsPh.eq(currentIndex).offset().top);
                        var top = sectionsPh.eq(currentIndex).offset().top;
                        // trigger scroll
                        TweenLite.to(window, 0.5, {
                            scrollTo: {y: top}
                        });
                    }
                });







                /*======================
                 ****  *OPEN MENU  ****/
                menuBtn.click(function (e) {
                    e.preventDefault();

                    blockScrolling();
                    mainMenu.show();

                    getMenuTriggerCoordinates();

                    menuTimeline
                    //We animate the bgAnimator to start from the nav Burger Btn
                    //position to full screen
                        .set(menuBgAnimator, {
                            width: "30px",
                            height: "30px",
                            y: menuBtnCoordinates.top,
                            x: menuBtnCoordinates.right,
                            border: "3px solid #FFFFFF",
                            backgroundColor: "transparent",
                            autoAlpha: 0
                        })

                        //we hide burger btn first and nav after
                        .to(menuBtn, 0.50, {
                            autoAlpha: 0
                        })
                        .to(nav, 0.50, {
                            autoAlpha: 0
                        })

                        //the small square becomes a grown background
                        .to(menuBgAnimator, 0.75, {
                            width: "100%",
                            height: "100%",
                            y: 0,
                            x: 0,
                            border: "0px solid transparent",
                            backgroundColor: "#555555",
                            ease: Power2.easeInOut
                        }, "-=0.85")
                        .to(menuBgAnimator, 0.45, {
                            autoAlpha: 0.90,
                            ease: Power4.easeOut
                        }, "-=0.75")
                        .to(menuClose, 0.45, {
                            autoAlpha: 1,
                            ease: Power2.easeOut
                        }, "-=0.15")

                        //we then let the first generation come in
                        .staggerFromTo(navFirstGeneration, 0.25, {
                            x: 40,
                            autoAlpha: 0
                        }, {
                            x: 0,
                            autoAlpha: 1
                        }, 0.1, "-=0.5");

                    $(currentOpeningTree).each(function (index, elem) {
                        elem.children(".plus-opener").trigger("click", [{speed: "-=0.08"}]);
                    });

                    //TODO: Animate the close btn
                });


                /*=======================================
                 ****  *NAVIGATE MENU, OPEN SUBMENU  ****/

                /*
                 A small disclaimer: the open class and the active class do different things.
                 We will check if a ul is open or not based on the "active" class on the PLUS OPENER.
                 This is because that is the only trigger point for the lists.
                 An "active" class on an anchor will make it get full opacity.
                 An "active" class on a list will make all the li get a reduced opacity
                 An "active" on a plus opener is used as flag
                 An "open" on a list is used as a flag (when we close for instance, to find all the open lists but not the ones with active style)
                 */

                mainMenu.on('click', '.menu-item-has-children > .plus-opener', function (e, data) {
                    var $this = $(this),
                        subMenu = $this.siblings(".sub-menu");

                    /* we call the function checking if an external trigger is passing data in */
                    if (data && data.speed) {
                        toggleSubMenu($this, subMenu, data.speed);
                    } else {
                        toggleSubMenu($this, subMenu, "");
                    }

                    function toggleSubMenu($this, subMenu, offset) {
                        if ($this.hasClass("active")) {
                            /** RECURSIVELY CLOSING EVERYTHING */

                                //we retrieve the menu associated with the plus opener clicked
                                //and all the submenus inside the associated list
                            var currentOpenList = $this.siblings("ul"),
                                currentOpenListItems = currentOpenList.children("li"),
                                openedSubMenus = currentOpenList.find("ul.open"),
                                numberOfMenusToClose = 1;


                            /* We check if the function has been called by an external trigger */
                            /* If yes, we are going to use the speed inserted as a multiplier to make the animation faster */
                            if (offset) {
                                var anticipate = offset;
                            } else {
                                var anticipate = 1;
                            }

                            //we check and put in the queue all the submenus opened first, starting from the deepest one
                            if (openedSubMenus.length > 0) {
                                numberOfMenusToClose += openedSubMenus.length;
                                for (var i = openedSubMenus.length - 1; i >= 0; i--) {
                                    var subMenu = openedSubMenus.eq(i),
                                        subMenuLi = subMenu.children("li");

                                    //we queue the submenuClosing
                                    closeList(subMenu, subMenuLi, numberOfMenusToClose, anticipate);
                                }
                            }

                            //then we close the menu associated with the anchor clicked
                            closeList(currentOpenList, currentOpenListItems, numberOfMenusToClose, anticipate);

                            //we remove the active status from the plus opener, from the link, the associated list and the parent list
                            menuTimeline.add(function () {
                                $this.removeClass("active");
                                $this.siblings("a").removeClass("active");
                                currentOpenList.removeClass("active");
                                $this.closest("ul").removeClass("active");
                            }, 0.1);

                            if ($(".invisible-trigger.xs").is(":visible") || $(".invisible-trigger.sm").is(":visible") || $(".invisible-trigger.md").is(":visible")) {
                                menuTimeline
                                    .to("#menu-mainmenu", 0.5, {
                                        x: "-=100%"
                                    }, "-=0.12");
                            }

                            function closeList(subMenu, subMenuElements, speedDivision, anticipate) {
                                console.log("anticipate3", anticipate);

                                //we remove the active status from a potential open list and all the anchors inside
                                subMenu.removeClass("active");
                                subMenuElements.children(".plus-opener").removeClass("active");
                                subMenuElements.children("a").removeClass("active");

                                menuTimeline
                                    .staggerTo(subMenuElements, 0.25 / (speedDivision * anticipate), {
                                        x: 40,
                                        autoAlpha: 0
                                    }, 0.1 / speedDivision, "+=0", hideSubMenu);

                                //TODO: find out why if we click too fast the plus openers we can lose the visibility on a submenu

                                function hideSubMenu() {
                                    subMenu.removeClass("open").hide();
                                }
                            }

                        } else {
                            /** OPENING SUBMENU (POTENTIALLY CLOSING ALL THE OTHER SIBLINGS OPENED FIRST) */

                            /*
                             Here we check if any other submenu is already open,
                             if yes, we close them all first and them we proceed with closing the main menu
                             */
                            var openSubMenus = $this.closest("ul").children("li").children("a.active");
                            if (openSubMenus.length > 0) {
                                openSubMenus.siblings(".plus-opener").click();
                            }

                            //we add the class to the plus-opener and the anchor, it will show the underline
                            $this.addClass("active");
                            $this.siblings("a").addClass("active");

                            //we assign an active class to the list so that the direct children can have less opacity
                            $this.closest("ul").addClass("active");

                            //we flag the the associated list is open
                            $this.siblings("ul").addClass("open").show();

                            //we show the submenu
                            subMenu.show();
                            if (offset) {
                                var anticipate = offset;
                            } else {
                                var anticipate = 0;
                            }
                            menuTimeline
                                .to(subMenu, 0.45, {
                                    transformOrigin: "right center",
                                    y: "-50%",
                                    scale: 1
                                }, "-=" + anticipate)
                                .staggerFromTo(subMenu.children("li"), 0.12, {
                                    x: 15,
                                    autoAlpha: 0
                                }, {
                                    x: 0,
                                    autoAlpha: 1
                                }, 0.08, "-=0.45");
                            if ($(".invisible-trigger.xs").is(":visible") || $(".invisible-trigger.sm").is(":visible") || $(".invisible-trigger.md").is(":visible")) {
                                menuTimeline
                                    .to("#menu-mainmenu", 0.5, {
                                        x: "+=100%"
                                    }, "-=0.12");
                            }
                        }
                    }
                });


                /*=======================
                 ****  *CLOSE MENU  ****/
                menuClose.click(function (e) {
                    e.preventDefault();

                    //Here we check if any submenu is already open, if yes, we close them all first and them we proceed with closing the main menu
                    var openSiblingSubs = navFirstGeneration.children("a.active"),
                        delayFirstGenerationAnim = 0;

                    if (openSiblingSubs.length > 0) {
                        delayFirstGenerationAnim = (1 + navFirstGeneration.find(".plus-opener.active").length) * 0.26;
                        openSiblingSubs.siblings(".plus-opener").trigger("click", [{speed: 1.5}]);
                    }

                    getMenuTriggerCoordinates();
                    menuTimeline
                        .set(nav, {
                            backgroundColor: "transparent",
                            autoAlpha: 1
                        })
                        .set(nav.find("ul.dots"), {
                            autoAlpha: 0
                        })
                        .to(menuClose, 0.45, {
                            autoAlpha: 0,
                            ease: Power2.easeOut
                        })
                        .staggerTo(navFirstGeneration, 0.125, {
                            x: 40,
                            autoAlpha: 0
                        }, 0.06, "-=" + delayFirstGenerationAnim)
                        .to(menuBgAnimator, 0.45, {
                            width: "30px",
                            height: "30px",
                            y: menuBtnCoordinates.top,
                            x: menuBtnCoordinates.right,
                            border: "3px solid #d51216",
                            backgroundColor: "transparent",
                            autoAlpha: 0
                        }, "-=0.20")
                        .to(menuBtn, 0.35, {
                            autoAlpha: 1,
                            onComplete: function () {
                                //we hide the menu so that we can click again and we reactivate scrolling
                                mainMenu.hide();
                                restoreScrolling();
                            }
                        }, "-=0.15")
                        .to(nav, 0.45, {
                            backgroundColor: "#d51216"
                        }, "-=0.20")
                        .to(nav.find("ul.dots"), 0.45, {
                            autoAlpha: 1
                        }, "-=0.45");

                    if ($(".invisible-trigger.xs").is(":visible") || $(".invisible-trigger.sm").is(":visible") || $(".invisible-trigger.md").is(":visible")) {
                        menuTimeline
                            .to("#menu-mainmenu", 0.5, {
                                x: "0%"
                            }, "-=" + (0.60 + delayFirstGenerationAnim));
                    }
                });

                function getMenuTriggerCoordinates() {
                    menuBtnCoordinates.top = menuBtn.offset().top - $(window).scrollTop();
                    menuBtnCoordinates.right = menuBtn.offset().left;
                }


                /*===============================================================
                 * SCROLL MAGIC CONTROLLER
                 ==============================================================*/

                /* Main controller */
                var scrollingController = new ScrollMagic.Controller();


                /*===============================================================
                 * HEADER BAR ON MOBILE
                 ==============================================================*/
                //This will make come the white background when the header is on mobile, after 75px of scrolling
                var headerCurtain = new ScrollMagic.Scene({
                    triggerElement: "body",
                    triggerHook: 0,
                    offset: 75
                })
                    .setTween(".col-header-mobile-background", 0.5, {y: "0%", ease: Power1.easeOut})
                    .addTo(scrollingController);


                /*=============================
                 * LINES ANIMATION
                 ============================*/
                $(".line-container").each(function (index, elem) {
                    var $elem = $(elem),
                        triggerHook = 0.75;

                    //we set which property to animate according to the class, if vertical or not
                    if ($elem.hasClass("vertical-line")) {
                        var tween = TweenMax.to($elem.find(".fill-line"), 1, {
                            height: "100%"
                        });
                    } else {
                        var tween = TweenMax.to($elem.find(".fill-line"), 1, {
                            width: "100%"
                        });
                    }

                    //if the element specifies a different trigger point, we change the hook
                    if ($elem.data("hook")) {
                        triggerHook = $elem.data("hook");
                    }

                    //we create the scene for the element
                    new ScrollMagic.Scene({
                        triggerElement: elem,
                        triggerHook: triggerHook,
                        reverse: false
                    })
                        .setTween(tween)
                        .addTo(scrollingController);
                });


                /*=============================
                 * MULTI LINE ANIMATION
                 ============================*/
                $(".line-container-multi-line").each(function (index, elem) {
                    var $elem = $(elem),
                        triggerHook = 0.75;

                    var lines = $(elem).children(".fill-line").sort(function(a,b){

                    });
                    console.log(lines);

                    //we set which property to animate according to the class, if vertical or not
                    if ($elem.hasClass("vertical-line")) {
                        var tween = TweenMax.to($elem.find(".fill-line"), 1, {
                            height: "100%"
                        });
                    } else {
                        var tween = TweenMax.to($elem.find(".fill-line"), 1, {
                            width: "100%"
                        });
                    }

                    //if the element specifies a different trigger point, we change the hook
                    if ($elem.data("hook")) {
                        triggerHook = $elem.data("hook");
                    }

                    //we create the scene for the element
                    new ScrollMagic.Scene({
                        triggerElement: elem,
                        triggerHook: triggerHook,
                        reverse: false
                    })
                        .setTween(tween)
                        .addTo(scrollingController);
                });


                /*=============================
                 * SLIDE IN IMAGES ANIMATION
                 ============================*/
                $(".slide-in-img").each(function (index, elem) {
                    var $elem = $(elem),
                        triggerHook = 0.66,
                        tween = new TimelineMax(),
                        movementValue = 65;

                    if($elem.hasClass("slide-from-left-img")){
                        movementValue = -movementValue;
                    }

                    tween
                        .fromTo($elem, 0.75,{
                            x : movementValue
                        },{
                            x : 0
                        });

                    //if the element specifies a different trigger point, we change the hook
                    if ($elem.data("hook")) {
                        triggerHook = $elem.data("hook");
                    }

                    //we create the scene for the element
                    new ScrollMagic.Scene({
                        triggerElement: elem,
                        triggerHook: triggerHook,
                        reverse: false
                    })
                        .setTween(tween)
                        .addTo(scrollingController);

                });


                /*=============================
                 * MIRROR QUOTE ANIMATION
                 ============================*/

                $(".mirror-quote-container").each(function (index, elem) {
                    var $elem = $(elem),
                        triggerHook = 0.66,
                        mirrorBox = $elem.find(".mirror-box"),
                        mirrorImageContainer = $elem.find(".mirror-image-container"),
                        tween = new TimelineMax(),
                        movementValue = 28,
                        x = 0,
                        y = 0;

                    if ($elem.hasClass("text-right")) {
                        x = movementValue;
                    } else {
                        x = -movementValue;
                    }

                    if ($elem.hasClass("top-quote")) {
                        y = movementValue;
                    } else {
                        y = -movementValue;
                    }
                    tween
                        .from(mirrorBox, 0.75, {
                            x: x,
                            y: y,
                            ease: Power2.easeInOut
                        })
                        .to(mirrorBox, 0.55, {
                            autoAlpha: 1
                        }, "-=0.75")
                        .from(mirrorImageContainer, 0.75, {
                            x: -x,
                            y: -y,
                            ease: Power2.easeInOut
                        }, "-=0.55")
                        .to(mirrorImageContainer, 0.55, {
                            autoAlpha: 1,
                            ease: Power1.easeIn
                        }, "-=0.75")
                        .to($elem.find(".top, .bottom"), 1, {
                            width: "100%"
                        }, "-=0.55")
                        .to($elem.find(".left, .right"), 1, {
                            height: "100%"
                        }, "-=1");

                    //if the element specifies a different trigger point, we change the hook
                    if ($elem.data("hook")) {
                        triggerHook = $elem.data("hook");
                    }

                    //we create the scene for the element
                    new ScrollMagic.Scene({
                        triggerElement: elem,
                        triggerHook: triggerHook,
                        reverse: false
                    })
                        .setTween(tween)
                        .addTo(scrollingController);
                });


                /*=============================
                 * *NUMBERS COMPONENT
                 ============================*/
                var numberController = new ScrollMagic.Controller();

                $(".number-placeholder").each(function (index, elem) {

                    //we prepare element, initialValue and difference that needs to be covered
                    var $elem = $(elem),
                        initialVal = parseInt($elem.text()),
                        difference = parseInt(initialVal / 100 * 40);

                    //we find out the direction and we address the startingVal
                    if ($elem.data("direction") === "increase") {
                        var startingVal = initialVal - difference;
                    } else {
                        var startingVal = initialVal + difference;
                    }

                    //we prepare the odometer with the starting val
                    $elem.text(startingVal);
                    var od = new Odometer({
                        el: $elem.get(0),
                        value: startingVal,
                        theme: ""
                    });


                    //we trigger the opacity and the odometer when we reach the element
                    var scene = new ScrollMagic.Scene({
                        triggerElement: $elem.get(0),
                        reverse: false
                    })
                        .on("start", function () {
                            od.update(initialVal);
                            TweenLite.to($elem, 0.3, {
                                autoAlpha: 1
                            })
                        })
                        .addTo(numberController);

                    //when the odometer is finished, if the value starts with a 0 we animate that as well
                    $elem.on("odometerdone", function (e) {
                        console.log("DONE");
                        console.log(e);
                        if (startingVal > 10 && initialVal < 10) {
                            console.log("lets add zero")
                            var span = "<div class='col' style='flex-grow:0;'><span class='temp-zero cypher d-inline-block'>0</span></div>";
                            $(span).prependTo($elem.closest(".row"));
                            var tempZero = $elem.closest(".row").find(".cypher.temp-zero");
                            TweenLite.to(tempZero, 0.42, {
                                transformOrigin: "left center",
                                scale: 0,
                                width: 0
                            })
                        }
                    })
                });


                /*=============================
                 * *UTILITY
                 ============================*/
                function blockScrolling() {
                    //We retrieve the scrollbarWidth if any
                    var scrollBarWidth = getScrollBarWidth();
                    if (scrollBarWidth) {
                        //we keep the same amount of space on the right so that there is no jump
                        $body.css({marginRight: scrollBarWidth});

                        /*
                         We elaborate in order:
                         -the initial right positioning value of the nav
                         -we transform it to Rem
                         */
                        var oldRight = parseFloat(nav.css("right")),
                            oldRightRem = $(oldRight).toRem({unit: ""}),
                            scrollBarWidthRem = $(parseFloat(scrollBarWidth)).toRem({unit: ""}),
                            sum = parseFloat(oldRightRem) + parseFloat(scrollBarWidthRem);
                        nav.css({
                            "right": sum + "rem"
                        });
                    }
                    $("html,body").addClass("no-scroll");
                }

                function restoreScrolling() {
                    $("html,body").removeClass("no-scroll");
                    $body.attr("style", "");
                    repositionNav();
                }

                function getScrollBarWidth() {
                    var inner = document.createElement('p');
                    inner.style.width = "100%";
                    inner.style.height = "200px";

                    var outer = document.createElement('div');
                    outer.style.position = "absolute";
                    outer.style.top = "0px";
                    outer.style.left = "0px";
                    outer.style.visibility = "hidden";
                    outer.style.width = "200px";
                    outer.style.height = "150px";
                    outer.style.overflow = "hidden";
                    outer.appendChild(inner);

                    document.body.appendChild(outer);
                    var w1 = inner.offsetWidth;
                    outer.style.overflow = 'scroll';
                    var w2 = inner.offsetWidth;
                    if (w1 == w2) w2 = outer.clientWidth;

                    document.body.removeChild(outer);

                    return (w1 - w2);
                }

                /*
                 * to transform values in Rem (the function was initially built to transform into EM
                 */
                $.fn.toRem = function (settings) {
                    settings = jQuery.extend({
                        scope: 'body',
                        unit: "rem"
                    }, settings);
                    var that = parseInt(this[0], 10),
                        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
                        scopeVal = scopeTest.height();
                    scopeTest.remove();
                    if (settings.unit !== "") {
                        return (that / scopeVal).toFixed(8) + settings.unit;
                    } else {
                        return (that / scopeVal).toFixed(8);
                    }
                };

                /* to transform values in PX */
                $.fn.toPx = function (settings) {
                    settings = jQuery.extend({
                        scope: 'body'
                    }, settings);
                    var that = parseFloat(this[0]),
                        scopeTest = jQuery('<div style="display: none; font-size: 1em; margin: 0; padding:0; height: auto; line-height: 1; border:0;">&nbsp;</div>').appendTo(settings.scope),
                        scopeVal = scopeTest.height();
                    scopeTest.remove();
                    return Math.round(that * scopeVal) + 'px';
                };

                /**
                 * Button Lazy Loader
                 */
                var blogLoadMore = $(".blog-load-more");
                $('.results').on('click', '.blog-load-more a', function(e) {
                    e.preventDefault();

                    var element = $(this);
                    var loading = $(this).siblings('div');
                    var target = $(this).attr('href');
                    var container = $('.posts-load .main-more-news-col .posts-row');

                    $(element).hide();
                    $(loading).show();

                    $.ajax({
                        url:target,
                        type:'GET',
                        success: function(data) {

                            var contents = $(data).find('.posts-load').html();
                            var loadMoreBlock = $(data).find(".post-wrapper").html();
                            console.log(loadMoreBlock);

                            // hide our button
                            $(loading).parents('.post-wrapper').remove();
                            $(loading).closest('.blog-load-more').remove();
                            $(loading).hide();

                            $(contents).appendTo(container).hide().slideDown(function(){
                                $(window).trigger("resize");
                            });

                            $(container).append(loadMoreBlock);

                            // update address bar
                            // history.pushState(contents, title, target);

                        }
                    });

                });

            },
            finalize: function () {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },

        /*******************************************************
         *CYGNUS JS - HOME
         ******************************************************/
        'page_template_template_home': {
            init: function () {
                jQuery(document).ready(function($){

                    /*=============================
                     * *FIRST IMAGE RESIZING
                     ============================*/
                    var homeFirstSectionImg = $("#home-section-1-img"),
                        firstSection = $(".section-1");

                    //since js kicked in, we change the padding to a small amount
                    homeFirstSectionImg.css({
                        paddingBottom : "20%"
                    });

                    function resizeFirstImage(){
                        homeFirstSectionImg.height(firstSection.outerHeight());
                    }
                    resizeFirstImage();

                    $(window).resize(function(){
                        resizeFirstImage();
                    });
                });
            },
            finalize: function () {
                // JavaScript to be fired on the home page, after the init JS
            }
        },

        /*******************************************************
         *CYGNUS JS - Who We Are / Maintenance / Bespoke Brands
         ******************************************************/
        'page_template_template_who': {
            init: function(){
                twoColumnVerticalLogic();
            },
            finalize: function(){
            }
        },
        'page_template_template_maintenance': {
            init: function(){
                twoColumnVerticalLogic();
            },
            finalize: function(){
            }
        },
        'page_template_template_bespoke': {
            init: function(){
                twoColumnVerticalLogic();
            },
            finalize: function(){
            }
        },



        /*******************************************************
         *CYGNUS JS - COFFEE SINGLE CPT
         ******************************************************/
        'single_coffee': {
            init: function(){
                jQuery(document).ready(function($){

                    /*******************************/
                    /*  RESIZE FIRST COFFEE IMAGE  */
                    /*******************************/
                    // The first image needs to show the text in the next section but to extend precisely at the top
                    // up to the beginning of the text
                    resizeFirstCoffeeImage();

                    $(window).resize(function () {
                        resizeFirstCoffeeImage();
                    });

                    function resizeFirstCoffeeImage() {
                        var firstCoffeeMirrorQuoteImage = $(".first-coffee-case-section .mirror-quote-container .mirror-image"),
                            contentCol = $(".first-coffee-case-section .content-col");
                        firstCoffeeMirrorQuoteImage.height(contentCol.outerHeight());
                    }


                    /*******************************/
                    /*      COFFEE SLIDE DOWN      */
                    /*******************************/
                    var slidingCoffeeElement = $("#sliding-coffee-element"),  //the product that will slide down
                        mainParentBgImage = $("#parent-bg-image-container"),  // the first background container the user will encounter
                        mainParentImage = $("#parent-image-container"),  //the first product image the user will encounter (and that will be appended as first one in the sliding element)
                        productTypesSection = $(".product-type-section"), //all the products variant "in the middle" after the main one and before the end of rail
                        lastImageEndOfRail = $(".last-rail-section .image-container"), // the last section, where the product will stop scrolling down
                        coffeeBrandController = new ScrollMagic.Controller(),  //the controller that will "receive" all the scenes for this page
                        slidingScene = new ScrollMagic.Scene({
                            /*
                             This scene makes the main parent image "Pinned" when we scroll down
                             */
                            triggerElement: mainParentImage.get(0),
                            offset: (mainParentImage.outerHeight() / 2),
                            duration: retrieveDuration()
                        })
                            .setPin("#sliding-coffee-element")
                            .addTo(coffeeBrandController);

                    //INIT FUNCTIONS
                    populateSlidingCoffeeElement();
                    resizeMainCoffeeSlideDownDimensions();

                    //FUNCTIONS that need to be CALLED again ON RESIZING to keep the effect in place
                    $(window).resize(function () {
                        resizeMainCoffeeSlideDownDimensions();
                        slidingScene.duration(retrieveDuration());
                    });

                    function retrieveDuration() {
                        //helps retrieving the end point for the scrolling effect
                        //calculates the difference in pixels between the first element and the end of rail
                        return lastImageEndOfRail.offset().top - mainParentImage.offset().top;
                    }

                    function populateSlidingCoffeeElement() {
                        /*
                         We prepare all the necessary style
                         --we retrieve the first element image and append it to the slidingElement
                         --we retrieve all the images in the variants, in the middle, and append them as well
                         --we create a scene that will trigger when the image needs to be changed for each variants
                         --we create a scene that will restore the initial img when we scroll up in the main parent section
                         */

                        //we retrieve the parent item, the first, we append the image contained in it in the element that will slide down
                        var style = mainParentImage.attr("style");
                        slidingCoffeeElement.append("<div class='image-container' style=\"" + style + "\"></div>");

                        //we check all the other product types sections and we retrieve their images
                        productTypesSection.each(function (index, elem) {
                            var $elem = $(elem),
                                style = $elem.find(".image-container").attr("style");

                            //we get the style from the image container and we append it to our sliding element container
                            slidingCoffeeElement.append("<div class='image-container' style=\"" + style + "\"></div>");

                            //we prepare the scenes for when we are going to meet the elements
                            //both when we are entering a new one (scrolling down - sceneEntering) or reentering an old one
                            //(scrolling up - sceneLeaving)
                            var sceneEntering = new ScrollMagic.Scene({
                                triggerElement: $elem.get(0),
                                offset: 0
                            })
                                .on('start', function () {
                                    TweenLite.to(slidingCoffeeElement.children(), 0.3, {
                                        autoAlpha: 0,
                                        immediateRender: false
                                    });
                                    TweenLite.to(slidingCoffeeElement.children().eq(index + 1), 0.3, {
                                        autoAlpha: 1,
                                        immediateRender: false
                                    }, "-=0.3");
                                })
                                .addTo(coffeeBrandController);

                            var sceneLeaving = new ScrollMagic.Scene({
                                triggerElement: $elem.get(0),
                                offset: $elem.height() * 4 / 5
                            })
                                .on('leave', function () {
                                    TweenLite.to(slidingCoffeeElement.children(), 0.3, {
                                        autoAlpha: 0,
                                        immediateRender: false
                                    });
                                    TweenLite.to(slidingCoffeeElement.children().eq(index + 1), 0.3, {
                                        autoAlpha: 1,
                                        immediateRender: false
                                    }, "-=0.3");
                                })
                                .addTo(coffeeBrandController);
                        });

                        //we also assign the scene for when we reenter the main parent section
                        var firstSceneLeaving = new ScrollMagic.Scene({
                            triggerElement: mainParentImage.get(0),
                            offset: mainParentImage.outerHeight() * 4 / 5
                        })
                            .on('leave', function () {
                                TweenLite.to(slidingCoffeeElement.children(), 0.3, {
                                    autoAlpha: 0,
                                    immediateRender: false
                                });
                                TweenLite.to(slidingCoffeeElement.children().eq(0), 0.3, {
                                    autoAlpha: 1,
                                    immediateRender: false
                                }, "-=0.3");
                            })
                            .addTo(coffeeBrandController);

                        //we make visible the first element and we are ready togo
                        TweenLite
                            .to(slidingCoffeeElement.children().first(), 1, {
                                autoAlpha: 1,
                                immediateRender: false
                            });
                    }

                    function resizeMainCoffeeSlideDownDimensions() {
                        slidingCoffeeElement.width(mainParentBgImage.outerWidth());
                        slidingCoffeeElement.height(mainParentBgImage.outerHeight());
                    }
                });
            },
            finalize: function(){
            }
        },

        /*******************************************************
         *CYGNUS JS - COFFEE WORKS
         ******************************************************/
        'page_template_template_coffee_works': {
            init: function() {

                jQuery(document).ready(function($){

                    /*******************************/
                    /*    RED TITLES ANIMATION     */
                    /*******************************/
                    var redTitles = $(".red-bg-text, .hero-classic-content .title-col"),
                        pathController = new ScrollMagic.Controller();

                    redTitles.each(function (index, elem) {
                        var $elem = $(elem),
                            tween = new TimelineLite().add(function () {
                                $elem.addClass("active");
                            }),
                            backgroundSlidingIn = new ScrollMagic.Scene({
                                triggerElement: $elem.get(0),
                                offset: -($(window).height() / 4)
                            })
                                .setTween(tween)
                                .addTo(pathController);
                    });


                    /******************************/
                    /*      GRAPH ANIMATIONS      */
                    /******************************/
                    var ctx = $(".graph");
                    ctx.each(function(index,elem){
                        var value = $(elem).data("activeValue"),
                            data = [value, 100-value],
                            doughnutChart = new Chart($(elem).get(0), {
                            type: 'doughnut',
                            data: {
                                datasets: [{
                                    data: data,
                                    backgroundColor : ["#d51216", "#dddddd"]
                                }]
                            },
                            options: {
                                "cutoutPercentage": 88
                            }
                        });
                    });

                });
            },
            finalize: function(){
            }
        },

        /*******************************************************
         *CYGNUS JS - BLOG
         ******************************************************/
        'blog': {
            init: function() {
                jQuery(document).ready(function($){

                    /***********************************/
                    /*  CUSTOM SUBCONTENT MATCHHEIGHT  */
                    /***********************************/
                    var invisibleTriggerXs = $(".invisible-trigger.xs"),
                        invisibleTriggerSm = $(".invisible-trigger.sm");

                    //We check if we are on desktop, otherwise we remove the styles from all the subcontent
                    $(window).resize(function(){
                       if(!invisibleTriggerXs.is(":visible") || !invisibleTriggerSm.is(":visible")){
                           customMatchHeight();
                       }else{
                           $(".subcontent").attr("style","");
                       }
                    });

                    //we call the function as soon as the document is ready
                    customMatchHeight();

                    function customMatchHeight(){
                        //we reset the style so that we can calculate the height correctly (if there is already a style it might be wrong)
                        $("article .subcontent").attr("style", "");
                        $("section.more-news .col:odd .subcontent").each(function(index, elem){
                            var $elem = $(elem),
                                $elemHeight = $(elem).height(),
                                siblingSubcontent = $elem.closest(".col").next(".col").find(".subcontent"),  //we retrieve the "even" subcontent
                                siblingSubcontentHeight = siblingSubcontent.height();

                            //we check if the height is major between the "odd" subcontent or the "even" one.
                            //we then assign the biggest height to the smallest element
                            if(siblingSubcontentHeight){
                                if($elemHeight > siblingSubcontentHeight){
                                    siblingSubcontent.height($elemHeight);
                                }else{
                                    $elem.height(siblingSubcontentHeight);
                                }
                            }
                        });
                    }
                });
            },
            finalize: function(){
            }
        },


        /*******************************************************
         *CYGNUS JS - SIDE PRODUCTS
         ******************************************************/
        'page_template_template_side_products': {
            init: function() {

                jQuery(document).ready(function($){

                    /*******************************/
                    /*    RED TITLES ANIMATION     */
                    /*******************************/
                    var redTitles = $(".red-bg-text, .hero-classic-content .title-col"),
                        pathController = new ScrollMagic.Controller();

                    redTitles.each(function (index, elem) {
                        var $elem = $(elem),
                            tween = new TimelineLite().add(function () {
                                $elem.addClass("active");
                            }),
                            backgroundSlidingIn = new ScrollMagic.Scene({
                                triggerElement: $elem.get(0),
                                offset: -($(window).height() / 4)
                            })
                                .setTween(tween)
                                .addTo(pathController);
                    });

                });
            },
            finalize: function(){
            }
        },



        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function () {
                // JavaScript to be fired on the about us page
            }
        }
    };


    /************************************************************
     *  CODE IN COMMONS
     */

    function twoColumnVerticalLogic(){
        jQuery(document).ready(function ($) {
            var pathController = new ScrollMagic.Controller();

            $(".path-container").each(function (index, elem) {
                var $elem = $(elem),
                    fillLine = $elem.find(".fill-line"),
                    scene = new ScrollMagic.Scene({
                        triggerElement: $elem.get(0),
                        duration: $elem.height() * 0.6
                    })
                        .setTween(fillLine, 1.5, {
                            height: "100%",
                            ease: Sine.easeOut
                        })
                        .addTo(pathController);
            });

            function resizeSideImages() {
                $(".side-image").each(function (index, elem) {
                    var $elem = $(elem),
                        section = $elem.closest("section.two-column-vertical-section");
                    if (section.hasClass("first-section") || section.hasClass("last-section")) {
                        $elem.height(section.height() + (section.width() * 0.16));
                    } else {
                        $elem.height(section.outerHeight() + (section.width() * 0.04));
                    }
                });
            }

            resizeSideImages();

            $(window).resize(function () {
                resizeSideImages();
            })

        });
    }

        // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function (func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function () {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery);
