<?php
/**
 * Template Name: Black&White 4 Landing Template
 */
?>

<!-- fullPage.js --->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.css" type="text/css" media="all" />

<!-- Used for PDF download --->
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery-ajax-native@1.0.2/src/jquery-ajax-native.min.js"></script>

<!-- fullPage.js --->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/fullpage.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.4/vendors/scrolloverflow.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($) {
        $.fn.changeElementType = function (newType) {
            var attrs = {};

            $.each(this[0].attributes, function (idx, attr) {
                attrs[attr.nodeName] = attr.nodeValue;
            });

            this.replaceWith(function () {
                return $("<" + newType + "/>", attrs).append($(this).contents());
            });
        }

        // Moving elements around so material design can be applied to the form
        $(".material-theme .wpcf7-form-control-wrap").each(function (i) {
            $(this).parent().find(".bar").insertAfter($(this).find("input"));
            $(this).parent().find("label").insertAfter($(this).find("input"));
        });

        $(".material-theme .wpcf7-checkbox .wpcf7-list-item").each(function (i) {
            $(this).find("input").after("<i class='helper'></i>");
        });

        $(".material-theme .wpcf7-checkbox .wpcf7-list-item").changeElementType("label");

        // Setting all form fields as required
        $(".material-theme .wpcf7-form-control").attr("required", "required");

        // Used to aid with applying material design to the form
        function checkForInput(element) {
            if ($(element).val().length > 0) {
                $(element).addClass("has-value");
            } else {
                $(element).removeClass("has-value");
            }
        }

        $(".material-theme .wpcf7-form-control").on("change keyup", function () {
            checkForInput(this);
        });

        new fullpage('#fullPage', {
            //options here
            licenseKey: '4669A1ED-39254C9C-A0B726A6-F58E184B',
            autoScrolling: true,
            fitToSection: false,
            navigation: true,
            navigationPosition: 'left',
            responsive: true,
            scrollOverflow: true,
            scrollOverflowOptions: {
                click: false,
                preventDefaultException: { tagName:/^(A|INPUT|TEXTAREA|BUTTON|SELECT|LABEL)$/ }
            },
            afterLoad: function(origin, destination, direction) {
                if (destination.isFirst || destination.index === 1 || destination.isLast) {
                    jQuery("#fp-nav").removeClass("show");
                } else {
                    jQuery("#fp-nav").addClass("show");
                }

                if (destination.index === 1 || destination.isLast) {
                    jQuery("#fixed-contact-area").fadeOut(1000);
                } else {
                    jQuery("#fixed-contact-area").fadeIn(1000);
                }
            }
        });

        function downloadPDF() {
            const url = "/wp-content/themes/cygnus_ucc/dist/docs/UCC_B&W4_Marketing Brochure_DIGITAL.pdf";
            $.ajax({
                dataType: 'native',
                url: url,
                xhrFields: {
                    responseType: 'blob'
                },
                success: function(blob){
                    var link=document.createElement('a');
                    link.href=window.URL.createObjectURL(blob);
                    link.download="UCC_B&W4_Marketing Brochure_DIGITAL.pdf";
                    link.click();
                }
            });
        }

        $("#contact-btn, #fixed-contact-btn").on("click", function() {
            fullpage_api.moveTo('download');
        });

        $(".download-btn").on("click", downloadPDF);

        document.addEventListener( 'wpcf7submit', function( event ) {
            // Re-build fullPage.js so the form scroll section is re-sized to accommodate error messages
            fullpage_api.reBuild();
        }, false );

        document.addEventListener( 'wpcf7mailsent', function( event ) {
            // Hide Form area
            $("#form-area").hide();
            // Show Download area
            $("#download-area").show();
            // Re-build fullPage.js so the form scroll section is re-sized to accommodate new content
            $("#download-area").promise().done(function() {
                fullpage_api.reBuild();
                fullpage_api.silentMoveTo('download');
            });
        }, false );
    });
</script>

<div id="fullPage">
    <div id="intro-section" class="section active" data-anchor="intro">
        <section id="intro">
            <div><img src="/wp-content/themes/cygnus_ucc/dist/images/ucc-logo-white.svg" alt="" class="logo"></div>
            <h1>Introducing</h1>
            <?php echo file_get_contents(__DIR__ . "/dist/images/bw4/bw4-logo.svg"); ?>
            <div class="content">
                <p>The next generation super automatic espresso machine designed for consistent, quality coffee at&nbsp;scale.</p>
            </div>
        </section>
        <section id="hero">
            <img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/hero.jpg" alt="" class="hero">
        </section>
    </div>
    <div id="video-section" class="section" data-anchor="video">
        <section id="bw4-heading-video">
            <img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Precision-Performance-Power.gif" alt="" class="Precision-Performance-Power_animated">
            <div class="modal-video-container" id="video-modal-AVq2t8lC2Ws" data-video-id="AVq2t8lC2Ws">
                <div class="background-overlay"></div>
                <div class="video-container">
                    <div class="close-btn"><i class="fa fa-times"></i></div>
                    <video id="AVq2t8lC2Ws" class="video-js" controls preload="auto" width="640" height="264"
                           data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=AVq2t8lC2Ws"}] }'>
                        <p class="vjs-no-js">
                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                        </p>
                    </video>
                </div>
            </div>

            <a href="#video-modal-AVq2t8lC2Ws" aria-label="Play" class="video-modal-link">
                <?php echo file_get_contents(__DIR__ . "/dist/images/countdown/play-icon.svg"); ?>
            </a>
            <div id="contact-area">
                <button class="download-btn"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span>Download brochure</span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></button>
                <a href="/wp-content/themes/cygnus_ucc/dist/docs/UCC_B&W4_Marketing Brochure_DIGITAL.pdf" target="_blank" class="download-btn_ie btn-bw4"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span>Download brochure</span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></a>
                <button id="contact-btn"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span>Request more info</span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></button>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-1">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoShot-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoShot-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/ThermoShot.svg"); ?></div>
                <h4>Intelligent <br />shot quality</h4>
                <p>The smartest super-automatic espresso extraction ever&nbsp;made.</p>
                <p>True intelligence delivering shot&nbsp;after&nbsp;shot.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-2">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoFoam-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoFoam-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/ThermoFoam.svg"); ?></div>
                <h4>Superior <br />milk</h4>
                <p>Perfect taste, texture and temperature, every&nbsp;time.</p>
                <p>Up to 41% faster dispense&nbsp;time.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-3">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoProof-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoProof-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/ThermoProof.svg"); ?></div>
                <h4>Temperature <br />stability</h4>
                <p>Unrivalled temperature stability for consistent extraction, cup&nbsp;after&nbsp;cup.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-4">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoConnect-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoConnect-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/ThermoConnect.svg"); ?></div>
                <h4>Two-way <br />telemetry</h4>
                <p>Complete estate management and performance visibility managed by the UCC&nbsp;Telemetry&nbsp;Hub.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-5">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Maintenance-Made-Easy-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Maintenance-Made-Easy-icon.gif" alt=""></div>
                <div class="feature-title two-lines"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/Maintenance-Made-Easy.svg"); ?></div>
                <h4>Quicker, easier <br />&amp;&nbsp;more&nbsp;efficient</h4>
                <p>30% decrease in machine&nbsp;downtime.</p>
                <p>Instantly exchangeable&nbsp;modules.</p>
            </div>
        </section>
    </div>

    <div class="section bw4-feature-section" data-anchor="feature-6">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Efficient-Design-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Efficient-Design-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/Efficient-Design.svg"); ?></div>
                <h4>Efficient <br />delivery</h4>
                <p>15% more energy efficient.</p>
                <p>78% less water used.</p>
                <p>37% quicker auto-cleaning cycle.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-7">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Dynamic-Menu-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Dynamic-Menu-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/Dynamic-Menu.svg"); ?></div>
                <h4>Upgraded <br />touchscreen</h4>
                <p>Infinite menu options on 10”&nbsp;(HD)&nbsp;touchscreen.</p>
                <p>Drink batching capability for quick beverage&nbsp;selection.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-8">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/High-Street-Approved-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/High-Street-Approved-icon.gif" alt=""></div>
                <div class="feature-title two-lines"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/High-Street-Approved.svg"); ?></div>
                <h4>Precision <br />engineered</h4>
                <p>Bestselling coffees at industry leading&nbsp;speed.</p>
                <p>Benchmarked across UK leading non&#8209;specialist&nbsp;chains.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-9">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoCycle-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/ThermoCycle-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/ThermoCycle.svg"); ?></div>
                <h4>The fastest coffee <br />preparation on <br />the high&nbsp;street</h4>
                <p>Up to 20% faster coffee preparation with Thermo<strong>Cycle<sup>&trade;</sup></strong>&nbsp;technology.</p>
                <p>Auto-adjustable spout.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-feature-section" data-anchor="feature-10">
        <div class="bg"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Maximum-Capacity-bg.jpg" alt=""><div class="overlay"></div></div>
        <section class="bw4-feature">
            <div class="content">
                <div class="feature-icon"><img data-src="/wp-content/themes/cygnus_ucc/dist/images/bw4/Maximum-Capacity-icon.gif" alt=""></div>
                <div class="feature-title"><?php echo file_get_contents(__DIR__ . "/dist/images/bw4/Maximum-Capacity.svg"); ?></div>
                <h4>Compact <br />footprint</h4>
                <p>100s of extra coffees every&nbsp;day.</p>
                <p>20mm slimmer than the&nbsp;competition.</p>
                <p>28% potential capacity&nbsp;increase.</p>
            </div>
        </section>
    </div>
    <div class="section bw4-form-footer-section" data-anchor="download">
        <section id="bw4-form">
            <div class="content">
                <div id="form-area">
                    <h2>Download the Black&amp;White4 brochure <br />and register your interest.</h2>

                    <?php echo do_shortcode('[contact-form-7 id="4784" title="Black&White4 Download Form"]'); ?>
                </div>
                <div id="download-area">
                    <h2>Thank you for registering your interest in the Black&amp;White4.</h2>
                    <h2>Please click the button below to download the&nbsp;brochure.</h2>
                    <button class="download-btn"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span>Download brochure</span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></button>
                    <a href="/wp-content/themes/cygnus_ucc/dist/docs/UCC_B&W4_Marketing Brochure_DIGITAL.pdf" target="_blank" class="download-btn_ie btn-bw4"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span>Download brochure</span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></a>
                </div>
            </div>
        </section>
        <footer id="bw4-footer" class="footer-event">
            <div class="copyright">&copy;2019 UCC Coffee UK Ltd</div>
            <div class="legal-links">
                <a href="https://www.ucc-coffee.co.uk/privacy/" target="_blank">Privacy Policy</a>
                <a href="https://www.ucc-coffee.co.uk/legal-docs/" target="_blank">Terms of Use</a>
            </div>
        </footer>
    </div>
</div>
<div id="fixed-contact-area"><button id="fixed-contact-btn"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_left.png" alt=""><span class="text">Request more info</span><span class="icon"><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/contact-icon.svg" alt=""></span><img src="/wp-content/themes/cygnus_ucc/dist/images/bw4/btn-border_right.png" alt=""></button></div>
