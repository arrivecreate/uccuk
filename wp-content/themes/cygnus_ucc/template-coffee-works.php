<?php
/**
 * Template Name: Coffee Works Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="container-fluid container-fluid-cap">
        <?php get_template_part('templates/partials/classic-image-hero-coffee-works', 'classic-image-hero'); ?>
    </div>

    <?php
    //Loop the Group
    if (have_rows('first_section')):
        while (have_rows('first_section')) : the_row();

            if (!empty(get_sub_field('description')) || !empty(get_sub_field('image') || !empty(get_sub_field('video_id')))) :

                $overlapping = "";
                if ((get_sub_field('display_video_or_image') == "image" && !empty(get_sub_field('image')))
                    || (get_sub_field('display_video_or_image') == "video" && !empty(get_sub_field('video_id')))) {
                    $overlapping = "overlapping-element";
                }

                ?>
                <section class="section-scrollable video-section <?php echo $overlapping; ?>">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="image-video-col col-12 col-md-9 thick-pad-l">
                                <div class="inner-padding">
                                    <?php
                                    if (!empty(get_sub_field('description'))) {
                                        echo "<div class=\"prevideo-desc subcontent\">" . get_sub_field('description') . "</div>";
                                    }

                                    if (!empty(get_sub_field('video_call_to_action'))) {
                                    ?>
                                      <h3 class="title"><?php echo get_sub_field('video_call_to_action'); ?></h3>
                                    <?php
                                    }

                                    if (!empty(get_sub_field('display_video_or_image')) && get_sub_field('display_video_or_image') == "image") {
                                        if (!empty(get_sub_field('image'))) {
                                            echo "<div class=\"image-fallback\">" .
                                                "<div class=\"image-container\" style=\"background-image: url('" . get_sub_field('image') . "')\"></div>" .
                                                "</div>";
                                        }
                                    } else {
                                        if (!empty(get_sub_field('video_id'))) { ?>
                                            <style>
                                                .vjs-youtube-mobile .vjs-poster {
                                                    background-image : url("https://img.youtube.com/vi/<?php echo get_sub_field('video_id'); ?>/maxresdefault.jpg");
                                                    display          : inline-block !important;
                                                    z-index          : 35;
                                                }
                                            </style>
                                            <div class="video-container">
                                                <video id="coffee-works-video" class="video-js" controls preload="auto" width="640" height="264"
                                                       data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=<?php echo get_sub_field('video_id'); ?>"}], "customControlsOnMobile": false }'>
                                                    <p class="vjs-no-js">
                                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                    </p>
                                                </video>
                                            </div>
                                        <?php }
                                    } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <section class="section-scrollable gray-background-section top-gray">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="image-col col col-12 col-md-4 slide-in-img slide-from-bottom-img">
                            <?php if (!empty(get_sub_field('background_image_top'))) { ?>
                                <div class="image-container" style="background-image: url('<?php echo get_sub_field('background_image_top') ?>');"></div>
                            <?php } ?>
                        </div>
                        <div class="content-col col col-12 col-md-8">
                            <h2 class="title section-title red-bg-text">
                                <span><?php echo get_sub_field('section_title'); ?></span>
                            </h2>
                            <div class="row">
                                <div class="col col-12 col-md-8 offset-md-1 col-xl-7 offset-xl-2">
                                    <?php
                                    while (have_rows('info_title')) : the_row();
                                        if (get_row_layout() == 'standard_sub_block'):
                                            echo "<h3 class='title'>" . get_sub_field('title') . "</h3>";
                                        elseif (get_row_layout() == 'number_sub_block'):
                                            $micro_title = get_sub_field('micro_title');
                                            $number = get_sub_field('number');
                                            $unit_of_measure = get_sub_field('number_unit');;
                                            $number_text = get_sub_field('number_text');
                                            include('templates/partials/components/numbers.php');
                                        endif;
                                    endwhile;

                                    if (get_sub_field("content_block_one_text_or_styled_number_list") == "styled_number_list") { ?>
                                        <div class="row">
                                            <?php
                                            $subcontent = get_sub_field('content_block_one_styled_number_list_paragraph');
                                            if (!empty($subcontent)):
                                                echo "<div class='styled-number-list-subcontent-col col col-12'><div class='subcontent'>" . $subcontent . "</div></div>";
                                            endif;
                                            ?>
                                        </div>

                                        <div class="row styled-number-list">
                                            <?php
                                            if (have_rows("content_block_one_styled_number_list")):
                                                while (have_rows("content_block_one_styled_number_list")) : the_row(); ?>
                                                    <div class="col col-6 col-sm-4 col-md-6 col-lg-4">
                                                        <div class="number-container">
                                                            <div class="number-image"
                                                                 style="background-image: url('<?php echo get_sub_field("bg_image") ?>')"></div>
                                                            <div class="number-label"><?php echo get_sub_field("list_element_text"); ?></div>
                                                        </div>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                    <?php } else {
                                        $subcontent = get_sub_field('content_block_one');
                                        if (!empty($subcontent)):
                                            echo "<div class='subcontent'>" . $subcontent . "</div>";
                                        endif;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="content-col bottom-content-col col col-12 col-md-6 order-2 order-md-1 thick-pad-l">
                            <?php if (get_sub_field('content_block_two_text_or_styled_list') == "text"): ?>
                                <div class="subcontent"><?php echo get_sub_field('content_block_two'); ?></div>
                            <?php else: ?>
                                <div class="subcontent subcontent-list">
                                    <?php
                                    if (have_rows("content_block_two_styled_list")):
                                        while (have_rows("content_block_two_styled_list")) : the_row(); ?>
                                            <p class="content-two-style-p"> <?php echo get_sub_field("text"); ?></p>
                                        <?php endwhile;
                                    endif;
                                    ?>
                                </div>
                            <?php endif; ?>
                        </div>
                        <div class="image-col bottom-image-col col col-12 col-md-5 offset-md-1 order-1 order-md-2 slide-in-img slide-from-bottom-img">
                            <div class="image-container" style="background-image:url('<?php echo get_sub_field('background_image_two'); ?>');"></div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="works-intro">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-7 title-col">
                            <?php if (get_sub_field('display_title_or_link') == "title" && !empty(get_sub_field('box_title'))) { ?>
                                <h2 class="title section-title red-bg-text">
                                    <span><?php echo get_sub_field('box_title'); ?></span>
                                </h2>
                            <?php } else {
                                if (have_rows('links')):
                                    while (have_rows('links')) : the_row();
                                        $link_type = get_sub_field('link_type');
                                        $link_text = get_sub_field('link_text');
                                        $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                        if ($link_type === "lt") {
                                            $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                            $scroll_down_class = "let-s-talk-link";
                                        } else {
                                            $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                            if ($link_type === "in") {
                                                if (get_post_type($link) === "attachment") {
                                                    $link = wp_get_attachment_url($link);
                                                    $target_blank = true;
                                                } else {
                                                    if (get_post_type($link) !== false) {
                                                        $link = get_the_permalink($link);
                                                    }
                                                }
                                            }
                                        }
                                        if (isset($link) && !empty($link) && !empty($link_text)) { ?>
                                            <a href="<?php echo $link; ?>" class="title section-title red-bg-text">
                                                <span><?php echo $link_text; ?></span>
                                            </a>
                                        <?php }
                                    endwhile;
                                endif;
                            } ?>
                        </div>
                        <div class="col col-12 col-md-7 subtitle-col">
                            <?php
                            $subtitle = get_sub_field('sub_title');
                            if (!empty($subtitle)) {
                                echo "<h3 class='subtitle'>" . $subtitle . "</h3>";
                            }
                            ?>
                        </div>
                        <div class="col col-12 col-md-8 subcontent-col">
                            <?php
                            $subcontent = get_sub_field('sub_content');
                            if (!empty($subcontent)) {
                                echo "<div class='subcontent'>" . $subcontent . "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </section>

        <?php
        endwhile;
    endif; ?>


    <?php
    //Loop the repeater
    if (have_rows('main_content')):
        //we reset the index and the alignment
        $i = 0;
        $left_right_switch = "left";

        while (have_rows('main_content')) : the_row();
            $image_placement = get_sub_field('image_placement');

            $additional_classes = "";
            $additional_classes .= "section-aligned-" . $left_right_switch . " ";
            $additional_classes .= "image-" . $image_placement;

            if ($left_right_switch == "left") {
                $image_col_class = "col col-12 col-md-6 order-1 order-md-1";
                $content_col_class = "col col-12 col-md-6 order-2 order-md-2";
            } else {
                $image_col_class = "col col-12 col-md-5 order-1 order-md-2";
                $content_col_class = "col col-12 col-md-7 order-1 order-md-1";
            }
            ?>

            <section class="section-scrollable coffee-works-vertical-section <?php echo $additional_classes; ?>">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12">
                            <div class="main-col-container">
                                <div class="row">
                                    <div class="image-col <?php echo $image_col_class; ?>">
                                        <div class="image-container" style="background-image: url('<?php echo get_sub_field('main_image'); ?>');"></div>
                                    </div>
                                    <div class="content-col <?php echo $content_col_class; ?>">
                                        <div class="content-inner-container">
                                            <?php
                                            $title = get_sub_field('title');
                                            if (!empty($title)):?>
                                                <h2 class="title"><?php echo $title; ?></h2>
                                            <?php endif; ?>

                                            <?php //Loop repeater
                                            if (have_rows('information')):
                                                echo "<div class='row no-gutters information-graphs'>";
                                                while (have_rows('information')) : the_row();

                                                    if (get_sub_field('percentage') == "yes") {
                                                        ?>
                                                        <div class='col statistic percentage-true'>
                                                            <div class="canvas-container">
                                                                <canvas class="graph" width="100" height="100"
                                                                        data-active-value="<?php echo get_sub_field('value') ?>"></canvas>
                                                            </div>

                                                            <?php
                                                            $micro_title = "";
                                                            $number = get_sub_field('value');
                                                            $unit_of_measure = "%";
                                                            $number_text = "";
                                                            include('templates/partials/components/numbers.php');
                                                            ?>
                                                            <div class="label-underneath"><?php echo get_sub_field('label'); ?></div>
                                                        </div>

                                                    <?php } else {
                                                        echo "<div class='col statistic percentage-false'>";
                                                        $micro_title = "";
                                                        $number = get_sub_field('value');
                                                        $unit_of_measure = "";
                                                        $number_text = "";
                                                        include('templates/partials/components/numbers.php');
                                                        echo "<div class='label-underneath'>" . get_sub_field('label') . "</div>";
                                                        echo "</div>";
                                                    }

                                                endwhile;
                                                echo "</div>";
                                            endif; ?>


                                            <?php
                                            if ($image_placement == "back") {
                                                $description = get_sub_field('description');
                                                if (!empty($description)) {
                                                    echo "<div class='description subcontent'>" . $description . "</div>";
                                                }

                                                // LINKS
                                                $rows_loop_name = "links";
                                                $color_class = "dark-btn";
                                                include('templates/partials/components/link-row-container.php');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($image_placement == "fore") { ?>
                                    <div class="row image-fore-sibling-row">
                                        <div class="placeholder-col <?php echo $image_col_class ?>"></div>
                                        <div class="content-col <?php echo $content_col_class; ?>">
                                            <?php $description = get_sub_field('description');
                                            if (!empty($description)) {
                                                echo "<div class='description subcontent'>" . $description . "</div>";
                                            }

                                            // LINKS
                                            $rows_loop_name = "links";
                                            $color_class = "dark-btn";
                                            include('templates/partials/components/link-row-container.php'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
            //cycle related operations
            if ($left_right_switch == "left") {
                $left_right_switch = "right";
            } else {
                $left_right_switch = "left";
            }

            $i++;
        endwhile;
    endif;


    //Loop the Group
    if (have_rows('bottom_section')):
        while (have_rows('bottom_section')) : the_row(); ?>

            <section class="section-scrollable gray-background-section bottom-gray">
                <div class="container-fluid container-fluid-cap">
                    <?php //Loop the Group
                    if (have_rows('top_block')):
                        while (have_rows('top_block')) : the_row(); ?>

                            <div class="row top-block">
                                <div class="image-col col col-12 col-md-4">
                                    <?php if (!empty(get_sub_field('main_image'))) { ?>
                                        <div class="image-container" style="background-image: url('<?php echo get_sub_field('main_image') ?>');"></div>
                                    <?php } ?>
                                </div>
                                <div class="content-col col col-12 col-md-8">
                                    <h2 class="title section-title red-bg-text">
                                        <span><?php echo get_sub_field('title'); ?></span>
                                    </h2>
                                    <div class="row">
                                        <div class="col col-12 col-md-8 offset-md-2">
                                            <?php
                                            $description = get_sub_field('description');
                                            if (!empty($description)) {
                                                echo "<div class='description subcontent'>" . $description . "</div>";
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endwhile;
                    endif; ?>


                    <?php //Loop the Group
                    if (have_rows('middle_block')):
                        while (have_rows('middle_block')) : the_row(); ?>
                            <div class="row middle-block">
                                <div class="content-col bottom-content-col col col-12 col-md-6 order-2 order-md-1 thick-pad-l">
                                    <h3 class="subtitle"><?php echo get_sub_field('sub_title'); ?></h3>
                                    <div class="description subcontent"><?php echo get_sub_field('description'); ?></div>
                                </div>
                                <div class="image-col bottom-image-col col col-12 col-md-5 offset-md-1 order-1 order-md-2 slide-in-img slide-from-bottom-img">
                                    <div class="image-container" style="background-image:url('<?php echo get_sub_field('image'); ?>');"></div>
                                </div>
                            </div>
                        <?php endwhile;
                    endif; ?>


                    <?php //Loop the Group
                    if (have_rows('bottom_block')):
                        while (have_rows('bottom_block')) : the_row(); ?>
                            <div class="row bottom-block">
                                <div class="image-col bottom-image-col col col-12 col-md-6 order-1 order-md-1">

                                    <div class="mirror-wrapper-overlap">
                                        <?php
                                        $text_alignment = "left";
                                        $image = get_sub_field('image');
                                        $quote = get_sub_field('quote');
                                        $vertical_alignment = "top";
                                        $box_color = "red-lines";
                                        $fixed_mirror_box = "true";
                                        include('templates/partials/components/mirror-quote.php');
                                        ?>
                                    </div>

                                </div>
                                <div class="content-col bottom-content-col col col-12 col-md-6 order-2 order-md-2">
                                    <?php // Loop the Link Repeater?>

                                    <div class="top-links">
                                        <div class="line-container docked-right thick">
                                            <span class="fill-line red-line"></span>
                                        </div>

                                        <?php
                                        $rows_loop_name = "top_links";
                                        $color_class = "dark-btn";
                                        include('templates/partials/components/link-row-container.php');
                                        ?>
                                    </div>

                                    <h3 class="subtitle"><?php echo get_sub_field('sub_title'); ?></h3>
                                    <div class="description subcontent"><?php echo get_sub_field('description'); ?></div>
                                    <?php
                                    $rows_loop_name = "links";
                                    $color_class = "dark-btn";
                                    include('templates/partials/components/link-row-container.php');
                                    ?>
                                </div>
                            </div>
                        <?php endwhile;
                    endif; ?>

                </div>
            </section>


        <?php endwhile;
    endif;

endwhile; ?>
