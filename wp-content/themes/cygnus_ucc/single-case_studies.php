<?php

/**
 * This single is specific to the Case Studies
 */

while (have_posts()) : the_post();
    get_template_part('templates/partials/classic-image-hero-case-study', 'classic-image-hero');

    //Loop through the main information group
    if (have_rows('main_information')): ?>
        <div class="sections-container">
            <?php while (have_rows('main_information')) : the_row();

                //Loop through the first section
                if (have_rows('section_one')):
                    while (have_rows('section_one')) : the_row(); ?>

                        <section class="section-scrollable first-section">
                            <div class="container-fluid container-fluid-cap">
                                <div class="row">
                                    <div class="image-col col col-12 col-md-5 order-1 order-md-1 thick-pad-l">
                                        <div class="mirror-quote-space-holder"></div>
                                        <?php
                                        $text_alignment = "left";
                                        $image = get_sub_field('image');
                                        $quote = get_sub_field('quote');
                                        $vertical_alignment = "bottom";
                                        $box_color = "red-lines";
                                        include('templates/partials/components/mirror-quote.php');
                                        ?>
                                    </div>
                                    <div class="content-col col col-12 col-md-7 order-2 order-md-2 common-pad-l thick-pad-r">
                                        <p class="micro-title"><?php echo get_sub_field("micro_title"); ?></p>

                                        <?php //Sub Flex Content - content
                                        if (have_rows('content')):
                                            while (have_rows('content')) : the_row();

                                                if (get_row_layout() == 'standard_sub_block'):?>
                                                    <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                                <?php elseif (get_row_layout() == 'number_sub_block'):
                                                    $number = get_sub_field('number');
                                                    $unit_of_measure = get_sub_field('number_unit');;
                                                    $number_text = get_sub_field('number_text');
                                                    include('templates/partials/components/numbers.php');
                                                endif;

                                            endwhile;
                                        endif; ?>

                                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>
                                        <?php
                                        $rows_loop_name = "links";
                                        $color_class = "dark-btn";
                                        include('templates/partials/components/link-row-container.php'); ?>

                                    </div>
                                </div>
                            </div>
                        </section>

                        <?php
                    endwhile;
                endif;


                //Loop through the sub sections
                if (have_rows('sub_sections')):
                    $left_right_switch = "right";
                    $i = 0;

                    while (have_rows('sub_sections')) : the_row();
                        if (get_row_layout() == 'option_one'):

                            if ($left_right_switch === "left") {
                                $image_col_class = "col col-12 col-md-4 order-1 order-md-1 common-pad-l";
                                $content_col_class = "col col-12 col-md-6 offset-md-1 order-1 order-md-2 common-pad-l thick-pad-r";
                            } else {
                                $image_col_class = "col col-12 col-md-4 offset-md-1 order-1 order-md-2 common-pad-r";
                                $content_col_class = "col col-12 col-md-7 order-1 order-md-1 thick-pad-l";
                            } ?>

                            <section class="section-scrollable sub-section option-1 <?php echo $left_right_switch; ?>-aligned">
                                <div class="container-fluid container-fluid-cap">
                                    <div class="row">
                                        <div class="image-col <?php echo $image_col_class; ?> slide-in-img slide-from-bottom-img">
                                            <div class="image-container" style="background-image: url('<?php echo get_sub_field('image'); ?>');"></div>
                                        </div>
                                        <div class="content-col <?php echo $content_col_class; ?>">
                                            <h3 class="micro-title"><?php echo get_sub_field('micro_title'); ?></h3>
                                            <h2 class="title <?php echo get_sub_field('title_colour'); ?>"><?php echo get_sub_field('title'); ?></h2>
                                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                            <?php
                                            //LINKS
                                            $rows_loop_name = "links";
                                            $color_class = "white-btn dark-text-hover";
                                            include('templates/partials/components/link-row-container.php');
                                            ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?php elseif (get_row_layout() == 'option_two'):

                            if ($left_right_switch === "left") {
                                $image_col_class = "col col-12 col-md-6 order-1 order-md-1 thick-pad";
                                $content_col_class = "col col-12 col-md-6 order-2 order-md-2 right-pad-for-menu align-self-center";
                            } else {
                                $image_col_class = "col col-12 col-md-6 order-1 order-md-2 right-pad-for-menu";
                                $content_col_class = "col col-12 col-md-6 order-1 order-md-1 thick-pad align-self-center";
                            } ?>

                            <section class="section-scrollable sub-section option-2 <?php echo $left_right_switch; ?>-aligned">
                                <div class="container-fluid container-fluid-cap">
                                    <div class="row">
                                        <div class="image-col <?php echo $image_col_class; ?>">
                                            <div class="inner-padding">
                                                <?php if( get_sub_field('title') ){
                                                    $text_alignment = $left_right_switch;
                                                    $image = get_sub_field('image');
                                                    $quote = get_sub_field('title');
                                                    $vertical_alignment = "bottom";
                                                    $box_color = "red-lines";
                                                    $data_hook = "0.82";
                                                    include('templates/partials/components/mirror-quote.php');
                                                }else { ?>
                                                    <div class="image-container" style="background-image: url('<?php echo get_sub_field("image"); ?>')"></div>
                                                <?php }?>
                                            </div>
                                        </div>
                                        <div class="content-col <?php echo $content_col_class; ?>">

                                                <div class="quote-content">
                                                    <span class="quote quote-open fa fa-quote-left"></span>
                                                    <?php echo get_sub_field('sub_content'); ?>
                                                    <span class="quote quote-close fa fa-quote-right"></span>
                                                </div>
                                                <p class="name"><?php echo get_sub_field('name'); ?></p>
                                                <p class="job-role"><?php echo get_sub_field('job_title'); ?></p>
                                                <?php
                                                $rows_loop_name = "links";
                                                $color_class = "white-btn dark-text-hover";
                                                include('templates/partials/components/link-row-container.php');
                                                ?>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        <?php endif;
                        if ($left_right_switch == "left") {
                            $left_right_switch = "right";
                        } else {
                            $left_right_switch = "left";
                        }

                    endwhile;
                endif;

            endwhile; ?>
        </div>
    <?php endif;

endwhile;