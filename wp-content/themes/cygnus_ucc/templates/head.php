<head>
    <script id="Cookiebot" src="https://consent.cookiebot.com/uc.js" data-cbid="a3d8a988-050a-496f-b80f-0bf393c3d0b2" data-blockingmode="manual" type="text/javascript"></script>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="ahrefs-site-verification" content="e9c4167aec4a5fd28f3b8c17cf829762536635e35ce05f1abd418ca6a22f8113">
    <meta name="ahrefs-site-verification" content="079278b5067b642d97332b09db822ae998d0450a1fe4b5bfca9ba8d52d2c60de">
    <script>
        var _urconfig = { sid: "c99d1155-4956-444a-bcc1-a8cc3f0b15c6", aip: 0, usePageProtocol: false };
        (function (d, s) {
            var js = d.createElement(s),
                sc = d.getElementsByTagName(s)[0];
            js.src = "https://hit.uptrendsdata.com/rum.min.js";
            js.async = "async";
            sc.parentNode.insertBefore(js, sc);
        }(document, "script"));
    </script>

    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="">

    <link rel="preload" href="/wp-content/themes/cygnus_ucc/dist/fonts/2bd4e485-6e2d-478c-94c7-9a7cabf8e98d.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/wp-content/themes/cygnus_ucc/dist/fonts/0078f486-8e52-42c0-ad81-3c8d3d43f48e.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/wp-content/themes/cygnus_ucc/dist/fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin>
    <link rel="preload" href="/wp-content/themes/cygnus_ucc/dist/fonts/17b90ef5-b63f-457b-a981-503bb7afe3c0.woff2" as="font" type="font/woff2" crossorigin>
    
    <?php wp_head(); ?>

</head>
