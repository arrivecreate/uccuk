<?php

/**
 * This partial needs to be called within The Loop
 */

//Loop the Group
if (have_rows('two_column_vertical')):
    while (have_rows('two_column_vertical')) : the_row();
        if (have_rows('content')):

            //we reset the index and the alignment
            $i = 0;
            $left_right_switch = "left";

            while (have_rows('content')) : the_row();


                if (get_row_layout() == 'standard_block'):

                    //custom classes according to the index in the loop
                    switch ($i) {
                        case 0:
                            $ordering_class = "first";
                            $red_line_vertical_flag = true;
                            $image_col_class = "col col-12 col-md-5 order-1 order-md-1";
                            $content_col_class = "col col-12 col-md-7 thick-pad right-pad-for-menu order-2 order-md-2";
                            break;
                        case 1:
                            $ordering_class = "central";
                            $red_line_vertical_flag = false;
                            $image_col_class = "col col-12 col-md-4 order-1 order-md-2";
                            $content_col_class = "col col-12 col-md-7 thick-pad right-pad-for-menu order-1 order-md-1 align-self-center";
                            break;
                        case 2:
                            $ordering_class = "central";
                            $red_line_vertical_flag = false;
                            $image_col_class = "col col-12 col-md-4 offset-md-1 order-1 order-md-1";
                            $content_col_class = "col col-12 col-md-7 thick-pad right-pad-for-menu order-2 order-md-2 align-self-center";
                            break;
                        case 3:
                            $ordering_class = "last";
                            $red_line_vertical_flag = true;
                            if (isset($two_column_regular) && $two_column_regular == true) {
                                $image_col_class = "col col-12 col-md-4 order-1 order-md-2";
                                $content_col_class = "col col-12 col-md-7 thick-pad right-pad-for-menu order-1 order-md-1 align-self-center";
                            } else {
                                $image_col_class = "col col-12 col-md-4 order-1 order-md-2";
                                $content_col_class = "col col-12 col-md-8 thick-pad right-pad-for-menu order-1 order-md-1";
                            }
                            break;
                        default:
                            $ordering_class = "central";
                            $red_line_vertical_flag = false;
                            $content_col_class = "col col-12 col-md-7 thick-pad order-1 order-md-1";
                    }

                    $additional_classes = "";
                    $additional_classes .= "section-aligned-" . $left_right_switch . " ";
                    $additional_classes .= $ordering_class . "-section ";
                    if ($i == 2 && !empty(get_sub_field("quote"))) {
                        $additional_classes .= "less-padding-bot ";
                    }
                    if (isset($two_column_regular) && $two_column_regular == true) {
                        $additional_classes .= "regular ";
                    } else {
                        $additional_classes .= "irregular ";
                    }
                    ?>

                    <section
                            class="section-scrollable two-column-vertical-section <?php echo $additional_classes; ?>">

                        <?php if ($i == 0) { ?>
                            <div class="first-path-line-container">
                                <div class="path-container thick vertical-line">
                                    <div class="fill-line red-line"></div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($i == 2 && (isset($two_column_regular) && $two_column_regular == true)) { ?>
                            <div class="last-section-line-container">
                                <div class="path-container thick vertical-line">
                                    <div class="fill-line red-line"></div>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if ($i == 3 && (!isset($two_column_regular) || $two_column_regular == false)) { ?>
                            <div class="last-section-line-container">
                                <div class="path-container thick vertical-line">
                                    <div class="fill-line red-line"></div>
                                </div>
                            </div>
                        <?php } ?>

                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <?php
                                $image = get_image_url(get_sub_field("image"));
                                $quote = get_sub_field("quote");
                                ?>
                                <div class="image-col <?php echo $image_col_class ?><?php if($image && !$quote){ echo " slide-in-img slide-from-bottom-img";} ?>">
                                    <?php
                                    if ($image && !$quote) {
                                        ?>
                                        <div class="mirror-wrapper-overlap">
                                        <?php
                                        $text_alignment = $left_right_switch;
                                        $vertical_alignment = "bottom";
                                        $box_color = "dark-lines";
                                            $fixed_mirror_box = "true";
                                        include('components/mirror-quote.php');
                                        ?>
                                        </div>
                                    <?php } else { ?>
                                        <div class="side-image" style="background-image: url('<?php echo get_image_url(get_sub_field('image')); ?>')"></div>
                                    <?php } ?>
                                </div>

                                <div class="content-col <?php echo $content_col_class ?>">
                                    <?php
                                    if (have_rows('info_title')):
                                        while (have_rows('info_title')) : the_row();
                                            if (get_row_layout() == 'standard_sub_block'): ?>

                                                <h3 class="micro-title"><?php echo get_sub_field('micro_title'); ?></h3>
                                                <h3 class="title <?php echo get_sub_field('title_colour'); ?>"><?php echo get_sub_field('title'); ?></h3>

                                            <?php elseif (get_row_layout() == 'number_sub_block'):

                                                $micro_title = get_sub_field('micro_title');
                                                $number = get_sub_field('number');
                                                $unit_of_measure = get_sub_field('number_unit');;
                                                $number_text = get_sub_field('number_text');
                                                include('components/numbers.php');

                                            endif;
                                        endwhile;
                                    endif;
                                    ?>

                                    <?php //CONDITIONAL RED LINE
                                    $red_line_flag = get_sub_field('line_flag');
                                    if (isset($red_line_flag) && !empty($red_line_flag) && $red_line_flag === "yes") { ?>
                                        <div class="line-container thick">
                                            <div class="fill-line red-line"></div>
                                        </div>
                                    <?php } ?>

                                    <div class="subcontent">
                                        <?php echo get_sub_field('sub_content'); ?>
                                    </div>

                                    <?php
                                        switch(get_sub_field('links_logo_list')) :
                                        case "link":

                                            //LINKS
                                            $rows_loop_name = "links";
                                            $color_class = "dark-btn";
                                            include('components/link-row-container.php');
                                            break;

                                        case "logo":

                                            //LOGO
                                            $logo = get_image_url(get_sub_field('logo'));

                                            if (is_array($logo)) { //$logo might be an array or an image, we check for that
                                                $logo = $logo['url'];
                                            }
                                            if (!empty($logo)) {
                                                echo "<img src=\"" . $logo . "\" alt=\"Client Logo\" class=\"img-fluid client-logo\">";
                                            }
                                            break;

                                        case "list":

                                            //LIST
                                            if(have_rows('list')) :
                                                echo '<ul class="inner-list">';
                                                    while (have_rows('list')) : the_row();
                                                        echo '<li>'.get_sub_field('list_item').'</li>';
                                                    endwhile;
                                                echo '</ul>';
                                            endif;
                                            break;

                                        endswitch;
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>

                <?php elseif (get_row_layout() == 'full_width_block'): ?>

                    <?php
                    //custom classes according to the index in the loop
                    switch ($i) {
                        case 0:
                            $ordering_class = "first";
                            break;
                        case 1:
                            $ordering_class = "central";
                            break;
                        case 2:
                            $ordering_class = "central";
                            break;
                        case 3:
                            $ordering_class = "last";
                            break;
                        default:
                            $ordering_class = "central";
                    }

                    $additional_classes = "";
                    $additional_classes .= "section-aligned-" . $left_right_switch . " ";
                    $additional_classes .= $ordering_class . "-section ";

//                    if($left_right_switch == "left"){
                        $col_class = "col-12 col-md-10";
//                    } else {
//                        $col_class = "col-12 col-md-10 offset-md-2 text-right";
//                    }
                    ?>

                    <section class="section-scrollable two-column-vertical-section <?php echo $additional_classes; ?>">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <div class="col <?php echo $col_class; ?> full-width-col thick-pad-l right-pad-for-menu">
                                    <?php if($left_right_switch == "left"):?>
                                        <div class="path-container thick vertical-line">
                                            <div class="fill-line red-line"></div>
                                        </div>
                                    <?php endif; ?>

                                    <?php if (get_sub_field('title_colour') == "red") {
                                        $color_class = "red-title";
                                    } else {
                                        $color_class = "";
                                    } ?>
                                    <h2 class="title <?php echo $color_class; ?>"><?php echo get_sub_field('title'); ?></h2>
                                    <h3 class="subcontent"><?php echo get_sub_field('sub_title'); ?></h3>
                                </div>
                            </div>
                        </div>
                    </section>

                <?php endif;

                if ($left_right_switch == "left") {
                    $left_right_switch = "right";
                } else {
                    $left_right_switch = "left";
                }

                //we refresh the index and we reset it if necessary
                $i++;
                if ($i >= 4) {
                    $i = 0;
                };
            endwhile;

        endif;

    endwhile;

endif;