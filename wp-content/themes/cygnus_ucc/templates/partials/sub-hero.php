<?php

/**
 * This partial needs to be called within The Loop
 */
if (have_rows('sub_hero')):
    while (have_rows('sub_hero')) : the_row();

    if(get_sub_field('image') && (get_sub_field('title') || get_sub_field('sub_content'))){

        if ($left_right_switch === "left") {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-1";
            $content_col_class = "col col-12 col-md-7 order-2 order-md-2 thick-pad-l align-self-center right-pad-for-menu";
        } else {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-2";
            $content_col_class = "col col-12 col-md-7 thick-pad order-2 order-md-1 align-self-center";
        }
        ?>

        <section class="section-scrollable sub-hero-component <?php echo $left_right_switch; ?>-aligned">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col <?php echo $image_col_class; ?>">
                        <div class="side-image" style="background-image:url('<?php echo get_image_url(get_sub_field('image')); ?>')"></div>
                    </div>
                    <div class="content-col <?php echo $content_col_class ?>">
                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>
                        <?php if (get_page_template_slug( get_queried_object_id() ) === "template-equipment.php") { ?>
                          <style>
                              .sub-hero-component .vjs-youtube-mobile .vjs-poster {
                                  background-image : url("https://img.youtube.com/vi/kRw0ASHLOcE/maxresdefault.jpg");
                                  display          : inline-block !important;
                                  z-index          : 35;
                              }
                          </style>
                          <div class="video-container">
                              <video id="sub-hero-component-video" class="video-js" controls preload="auto" width="640" height="264"
                                      data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=kRw0ASHLOcE"}], "customControlsOnMobile": false }'>
                                  <p class="vjs-no-js">
                                      To view this video please enable JavaScript, and consider upgrading to a web browser that
                                      <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                  </p>
                              </video>
                          </div>
                          <div class="subcontent">
                            <p>Whether it’s a traditional espresso machine, a super-automatic bean-to-cup, batch brew or a precision grinder, we work with the world’s most innovative equipment brands to guarantee we get this right.</p>
                          </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </section>
        <?php
        if ($left_right_switch == "left") {
            $left_right_switch = "right";
        } else {
            $left_right_switch = "left";
        }

    }
    endwhile;
endif;