<?php

/**
 * This partial needs to be called within The Loop
 */


if ( have_rows('classic_hero', get_option('page_for_posts')) ):

    while ( have_rows('classic_hero', get_option('page_for_posts')) ) : the_row();

        ?>

        <div class="hero-classic-container">
            <div class="hero-classic-img"
                 style="background-image: url('<?php echo get_image_url(get_sub_field('background_image', get_option('page_for_posts'))); ?>')"></div>

            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>

            <div class="current-page-container">
                <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
            </div>

            <div class="hero-classic-content">
                <h1 class="title red-title"><?php echo get_sub_field('title', get_option('page_for_posts')); ?></h1>
                <div class="subtitle"><?php echo get_sub_field('sub_title', get_option('page_for_posts')); ?></div>
                <div class="line-container thick" data-hook="1">
                    <div class="fill-line red-line"></div>
                </div>
            </div>
        </div>

        <?php

    endwhile;

endif;