<?php

/**
 * This partial needs to be called within The Loop
 */
if (have_rows('section_selection')):

    while (have_rows('section_selection')) : the_row();
        if (get_row_layout() == 'standard_block'):
            $bg_col = get_sub_field('background_colour_selection'); ?>
            <section
                    class="section-scrollable divided-colour-block-section first-divided-section <?php echo $bg_col; ?>-background">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="image-col col col-12 col-md-5 order-1 order-md-1 thick-pad-l">
                            <?php
                            $text_alignment = "left";
                            $image = get_image_url(get_sub_field('image_first_section'));
                            $quote = get_sub_field('quote');
                            $vertical_alignment = "bottom";
                            if (!isset($bg_col) || $bg_col == "dark") {
                                $box_color = "white-lines";
                            } else {
                                $box_color = "dark-lines";
                            }
                            include('components/mirror-quote.php');
                            ?>
                        </div>
                        <div class="content-col col col-12 col-md-7 order-1 order-md-2 thick-pad-l right-pad-for-menu">
                            <?php
                            $micro_title = get_sub_field('micro_title');
                            $line_flag = get_sub_field('line_flag');

                            if (!empty($micro_title) && $line_flag) { ?>
                                <h4 class="micro-title"><?php echo $micro_title; ?></h4>
                            <?php }
                            if ($line_flag !== "no") { ?>
                                <div class="line-container thick">
                                    <span class="fill-line red-line"></span>
                                </div>
                            <?php } ?>

                            <?php
                            if (have_rows('content_block')):
                                while (have_rows('content_block')) : the_row();
                                    if (get_row_layout() == 'standard_sub_block'): ?>
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                        <div class="subcontent"><?php echo get_sub_field('content'); ?></div>

                                        <?php if (have_rows('links')) { ?>
                                            <div class="row link-row-container flex-row">
                                                <?php while (have_rows('links')) : the_row();// LINKS
                                                    $target_blank = false;
                                                    if (!isset($bg_col) || $bg_col == "dark") {
                                                        $color_class = "white-btn dark-text-hover";
                                                    } else {
                                                        $color_class = "dark-btn";
                                                    }

                                                    if (!isset($display_class) || empty($display_class)) {
                                                        $display_class = "d-inline-block";
                                                    };
                                                    $link_type = get_sub_field('link_type');
                                                    $link_text = get_sub_field('link_text');
                                                    if ($link_type === "lt") {
                                                        $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                                        $scroll_down_class = "let-s-talk-link";
                                                    } else {
                                                        $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                                        if ($link_type === "in") {
                                                            if (get_post_type($link) === "attachment") {
                                                                $link = wp_get_attachment_url($link);
                                                                $target_blank = true;
                                                            } else {
                                                                if (get_post_type($link) !== false) {
                                                                    $link = get_the_permalink($link);
                                                                }
                                                            }
                                                        }
                                                        $scroll_down_class = "";
                                                    }
                                                    include('components/link-btn.php'); ?>
                                                <?php endwhile; ?>
                                            </div>
                                        <?php } ?>
                                    <?php elseif (get_row_layout() == 'number_sub_block'):

                                        $micro_title = get_sub_field('title');
                                        $number = get_sub_field('number');
                                        $unit_of_measure = get_sub_field('number_unit');;
                                        $number_text = get_sub_field('number_text');
                                        include('components/numbers.php'); ?>

                                        <?php
                                        //LOGO
                                        $logo = get_image_url(get_sub_field('logo'));

                                        if (is_array($logo)) { //$logo might be an array or an image, we check for that
                                            $logo = $logo['url'];
                                        }
                                        if (!empty($logo)) { ?>
                                            <div class="logo-container">
                                                <img src="<?php echo $logo; ?>" class="img-fluid">
                                            </div>
                                        <?php } ?>


                                    <?php endif;
                                endwhile;
                            endif;
                            ?>
                        </div>

                    </div>
                </div>
            </section>

            <section
                    class="section-scrollable divided-colour-block-section second-divided-section <?php echo get_sub_field('background_colour_selection_2'); ?>-background">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="content-col col col-12 col-md-7 order-2 order-md- thick-pad">
                            <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                            <div class="subcontent"><?php echo get_sub_field('sub_title'); ?></div>


                            <?php

                            $bg_col = get_sub_field('background_colour_selection_2');
                            if (have_rows('second_section_links')): ?>
                                <div class="row link-row-container">
                                    <?php while (have_rows('second_section_links')) : the_row();
                                        if (get_sub_field('link_type') !== "no") { ?>
                                            <?php // LINKS
                                            $target_blank = false;
                                            if (!isset($bg_col) || $bg_col == "dark") {
                                                $color_class = "white-btn dark-text-hover";
                                            } else {
                                                $color_class = "dark-btn";
                                            }

                                            if (!isset($display_class) || empty($display_class)) {
                                                $display_class = "d-inline-block";
                                            };
                                            $link_type = get_sub_field('link_type');
                                            $link_text = get_sub_field('link_text');

                                            if ($link_type === "lt") {
                                                $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                                $scroll_down_class = "let-s-talk-link";
                                            } else {
                                                $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                                if ($link_type === "in") {
                                                    if (get_post_type($link) === "attachment") {
                                                        $link = wp_get_attachment_url($link);
                                                        $target_blank = true;
                                                    } else {
                                                        if (get_post_type($link) !== false) {
                                                            $link = get_the_permalink($link);
                                                        }
                                                    }
                                                }

                                                $scroll_down_class = "";
                                            }

                                            include('components/link-btn.php');
                                            ?>
                                        <?php } ?>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>

                        <div class="image-col col col-12 col-md-5 order-1 order-md-2 thick-pad-l">
                            <div class="image-container <?php echo get_sub_field('image_flag'); ?>"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('image_second_section')); ?>');"></div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
        elseif (get_row_layout() == 'full_width_block'):?>

            <section class="section-scrollable divided-colour-block-section full-width-section">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="image-col col col-12 col-md-10 col-lg-9 common-pad-l">
                            <div class="image-container" style="background-image:url('<?php echo get_image_url(get_sub_field('top_image')); ?>')"></div>
                        </div>
                        <div class="content-col col col-12 col-md-7 offset-md-4 col-lg-6 offset-lg-5">
                            <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                            <div class="subcontent"><?php echo get_sub_field('sub_title'); ?></div>
                            <?php if (have_rows('links')): ?>
                                <div class="row link-row-container">
                                    <?php
                                    // LINKS
                                    while (have_rows('links')) : the_row();
                                        $target_blank = false;
                                        $bg_col = get_sub_field('background_colour_selection_2');
                                        if (!isset($bg_col) || $bg_col == "dark") {
                                            $color_class = "white-btn dark-text-hover";
                                        } else {
                                            $color_class = "dark-btn";
                                        }

                                        if (!isset($display_class) || empty($display_class)) {
                                            $display_class = "d-inline-block";
                                        };
                                        $link_type = get_sub_field('link_type');
                                        $link_text = get_sub_field('link_text');
                                        if ($link_type === "lt") {
                                            $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                            $scroll_down_class = "let-s-talk-link";
                                        } else {
                                            $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                            if ($link_type === "in") {
                                                if (get_post_type($link) === "attachment") {
                                                    $link = wp_get_attachment_url($link);
                                                    $target_blank = true;
                                                } else {
                                                    if (get_post_type($link) !== false) {
                                                        $link = get_the_permalink($link);
                                                    }
                                                }
                                            }
                                            $scroll_down_class = "";
                                        }
                                        include('components/link-btn.php');
                                    endwhile; ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </section>

            <?php
        endif;
    endwhile;
endif;