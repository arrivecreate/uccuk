
  <style>
  .section-eko-pod {
    /*background: linear-gradient(to bottom, #f6f7f2 42%, #efefe7 42%, #efefe7);*/
    margin-bottom: 6rem;
  }

  @media screen and (max-width: 400px){
    .section-eko-pod {
      margin-bottom: 1rem;
    }
  }

  .section-eko-pod .image > div {
    padding-bottom: 41.66667%;
    background-image: url('<?= get_template_directory_uri().'/eko-pod/main-banner.jpg'?>');
    background-size: cover;
  }
</style>

<!--<a href="https://www.ucc-coffee.co.uk/private-label/" class="section-scrollable section-eko-pod">-->
<!--  <div class="container-fluid container-fluid-cap">-->
<!--    <div class="row">-->
<!--      <div class="col-12">-->
<!--        <div class="image"><div></div></div>-->
<!--      </div>-->
<!--    </div>-->
<!--  </div>-->
<!--</a>-->

<div class="container-fluid container-fluid-cap section-scrollable section-eko-pod">
  <?php $slider = get_field('slider_revolution'); ?>
 <?php if(isset($slider)) { 
    add_revslider($slider);
  // echo do_shortcode('[rev_slider alias="slider-1"][/rev_slider]');
   } ?>
</div>


<!-- <style>
  .section-eko-pod {
    background: linear-gradient(to bottom, #f6f7f2 42%, #efefe7 42%, #efefe7);
    margin-bottom: 6rem;
  }

  .section-eko-pod .image > div {
    padding-bottom: 41.66667%;
    background-image: url('<?= get_template_directory_uri().'/eko-pod/main-banner.jpg'?>');
    background-size: cover;
  }
</style>

<a href="https://www.ucc-coffee.co.uk/private-label/" class="section-scrollable section-eko-pod">
  <div class="container-fluid container-fluid-cap">
    <div class="row">
      <div class="col-12">
        <div class="image"><div></div></div>
      </div>
    </div>
  </div>
</a>

-->