<?php
//Loop through the listing information group
if (have_rows('listing_information')):
    while (have_rows('listing_information')) : the_row();

        if ($left_right_switch === "left") {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-1 thick-pad-l align-self-center";
            $content_col_class = "col col-12 col-md-7 order-1 order-md-2 common-pad-l thick-pad-r align-self-center";
        } else {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-2 thick-pad-r align-self-center";
            $content_col_class = "col col-12 col-md-7 order-1 order-md-1 thick-pad right-pad-for-menu align-self-center";
        }
        ?>
        <section class="section-scrollable coffee-case-study-listing-element case-study-listing-element case-study-listing-element <?php echo $left_right_switch ?>-aligned-section">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col <?php echo $image_col_class ?>">
                        <?php
                        $text_alignment = $left_right_switch;
                        $image = get_sub_field('image');
                        if (is_array($image)) { //$image might be an array or an image, we check for that
                            $image = $image['url'];
                        }
                        $quote = get_sub_field('quote');
                        $vertical_alignment = "top";
                        $box_color = "dark-lines";
                        include('components/mirror-quote.php');
                        ?>
                    </div>
                    <div class="content-col <?php echo $content_col_class ?>">

                        <?php
                        $logo = get_image_url(get_sub_field('logo'));
                        if (isset($logo)) { ?>
                            <div class="case-study-logo-container">
                                <img class='case-study-logo img-fluid' src='<?php echo $logo; ?>'/>
                            </div>
                            <div class="line-position-allocator">
                                <div class="line-container thick <?php if ($left_right_switch == "left") {
                                    echo "docked-right";
                                } else {
                                    echo "docked-left";
                                } ?>">
                                    <span class="fill-line red-line"></span>
                                </div>
                            </div>
                        <?php } ?>

                        <?php if (!isset($logo) || empty($logo)): ?>
                            <div class="line-position-allocator">
                                <div class="line-container thick <?php if ($left_right_switch == "left") {
                                    echo "docked-right";
                                } else {
                                    echo "docked-left";
                                } ?>">
                                    <span class="fill-line red-line"></span>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                        <?php
                        $rows_loop_name = "links";
                        $color_class = "dark-btn";
                        include('components/link-row-container.php');
                        ?>

                    </div>
                </div>
            </div>
        </section>

        <?php
    endwhile;
endif;