<?php

/**
 * This partial needs to be called within The Loop
 */


if (have_rows('classic_image_hero')):
    while (have_rows('classic_image_hero')) : the_row();
        $logo = get_image_url(get_sub_field('logo')); ?>

        <a class="brand" href="<?= esc_url(home_url('/')); ?>">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
        </a>

        <div class="current-page-container">
            <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
        </div>

        <div class="hero-classic-container hero-image-classic hero-image-classic-coffee-works">
            <div class="hero-classic-img"
                 style="background-image: url('<?php echo get_image_url(get_sub_field('background_image')); ?>')"></div>

            <div class="hero-classic-content">
                <div class="row">

                    <?php if (isset($logo) && !empty($logo)):
                        $title_col_class = "col-12 col-sm-8 order-sm-1";
                        $logo_col_class = "col-8 col-sm-4 order-sm-2";
                    else:
                        $title_col_class = "col-12";
                        $logo_col_class = "d-none";
                    endif; ?>

                    <div class="logo-col col <?php echo $logo_col_class; ?> align-self-center">
                        <img class="img-fluid hero-logo" src="<?php echo $logo; ?>" alt="Hero Logo">
                    </div>
                    <div class="title-col col <?php echo $title_col_class; ?> thick-pad-l common-pad-r">
                        <h1 class="title"><?php echo get_sub_field('title'); ?></h1>

                        <?php if (!empty(get_sub_field("sub_title"))) : ?>
                            <div class="line-container thick full-width">
                                <div class="fill-line white-line"></div>
                            </div>
                            <div class="subtitle"><?php echo get_sub_field('sub_title'); ?></div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile;
endif;