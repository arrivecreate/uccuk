<?php
if (have_rows('classic_image_hero')):
    while (have_rows('classic_image_hero')) : the_row();
        $background_image = get_image_url(get_sub_field("background_image"));
    endwhile;
endif;


//Loop through the listing information group
if (have_rows('listing_information')):
    while (have_rows('listing_information')) : the_row();

        if ($left_right_switch === "left") {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-1 thick-pad-l align-self-center";
            $content_col_class = "col col-12 col-md-7 order-1 order-md-2 common-pad-l thick-pad align-self-center";
        } else {
            $image_col_class = "col col-12 col-md-5 order-1 order-md-2 thick-pad-r align-self-center";
            $content_col_class = "col col-12 col-md-7 order-1 order-md-1 thick-pad right-pad-for-menu align-self-center";
        }
        ?>
        <section class="section-scrollable case-study-listing-element <?php echo $left_right_switch ?>-aligned-section">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col <?php echo $image_col_class ?>">
                        <?php
                        $text_alignment = $left_right_switch;
                        if (isset($background_image) && !empty($background_image)) {
                            $image = $background_image;
                        } else {
                            $image = "";
                        }
                        $quote = get_sub_field('quote');
                        $vertical_alignment = "top";
                        $box_color = "dark-lines";
                        include('components/mirror-quote.php');
                        ?>
                    </div>
                    <div class="content-col <?php echo $content_col_class ?>">

                        <?php
                        $logo = get_image_url(get_sub_field('logo'));
                        if (isset($logo) && !empty($logo)) { ?>
                            <img class='case-study-logo img-fluid' src='<?php echo $logo; ?>'/>
                            <div class="line-position-allocator">
                                <div class="line-container thick <?php if ($left_right_switch == "left") {
                                    echo "docked-right";
                                } else {
                                    echo "docked-left";
                                } ?>">
                                    <span class="fill-line red-line"></span>
                                </div>
                            </div>
                        <?php } ?>

                        <?php
                        //Flex Content - content
                        if (have_rows('content')):
                            while (have_rows('content')) : the_row();

                                if (get_row_layout() == 'standard_sub_block'):

                                    echo "<h3 class='title'>" . get_sub_field('title') . "</h3>";

                                elseif (get_row_layout() == 'number_sub_block'):

                                    $micro_title = "";
                                    $number = get_sub_field('number');
                                    $unit_of_measure = get_sub_field('number_unit');;
                                    $number_text = get_sub_field('number_text');
                                    include('components/numbers.php');

                                endif;

                            endwhile;
                        endif;
                        ?>

                        <?php if (!isset($logo) || empty($logo)): ?>
                            <div class="line-position-allocator">
                                <div class="line-container thick <?php if ($left_right_switch == "left") {
                                    echo "docked-right";
                                } else {
                                    echo "docked-left";
                                } ?>">
                                    <span class="fill-line red-line"></span>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                        <?php
                        $rows_loop_name = "links";
                        $color_class = "dark-btn";
                        include('components/link-row-container.php');
                        ?>

                    </div>
                </div>
            </div>
        </section>

        <?php
    endwhile;
endif;