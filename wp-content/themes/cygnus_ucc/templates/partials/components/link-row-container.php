<?php
if(!(isset($rows_loop_name) && !empty($rows_loop_name))){
    $rows_loop_name = 'links';
}

if (have_rows($rows_loop_name)):?>
    <div class="row link-row-container flex-row <?php if(isset($additional_classes_links) && $additional_classes_links) { echo $additional_classes_links; }?>">
        <?php while (have_rows($rows_loop_name)) : the_row();

            $target_blank = false;
            if(!isset($color_class) || empty($color_class)){ $color_class = "dark-btn"; };
            if(!isset($display_class) || empty($display_class)){ $display_class = "d-inline-block"; };
            $link_type = get_sub_field('link_type');
            $link_text = get_sub_field('link_text');
            if($link_type === "lt"){
                $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                $scroll_down_class = "let-s-talk-link";
            }else{

                if($link_type === "video"){
                    $link = "#video-modal-" . get_sub_field("video_link");
                    $scroll_down_class = "video-modal-link"; ?>
                    <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                            <div class='background-overlay'></div>
                            <div class="video-container">
                                <div class='close-btn'><i class='fa fa-times'></i></div>
                                    <video id="<?php echo get_sub_field("video_link"); ?>" class="video-js" controls preload="auto" width="640" height="264" data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=<?php echo get_sub_field("video_link"); ?>"}] }'>
                                    
                                    <p class="vjs-no-js">
                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                    </p>
                                </video>
                            </div>
                         </div>
                <?php } else {
                    $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                    $target_blank = true;
                    if ($link_type === "in") {
                        if (get_post_type($link) === "attachment") {
                            $link = wp_get_attachment_url($link);
                            $target_blank = true;
                        } else {
                            if(get_post_type($link) !== false){
                                $link = get_the_permalink($link);
                            }
                        }
                    }

                    $scroll_down_class = "";
                }
            }

            include('link-btn.php');

        endwhile; ?>
    </div>
<?php endif; ?>