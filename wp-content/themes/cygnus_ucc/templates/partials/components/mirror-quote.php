<?php
//Text alignment - Left or Right
$additional_classes = "text-" . $text_alignment;

//Vertical alignment
if(isset($vertical_alignment) && $vertical_alignment === "bottom"){
    $additional_classes .= " bottom-quote ";
}else{
    $additional_classes .= " top-quote ";
}

if(isset($image_offset_plus) && !empty($image_offset_plus)){
    $additional_classes .= " image-offset-plus ";
}

//No image class
if(!(isset($image) && !empty($image)) ) {
    $additional_classes .= "no-image ";
}

if(!isset($data_hook)){
    $data_hook = "";
}

if(!isset($data_offset)){
    $data_offset = "0";
}
?>

<?php if(isset($vertical_alignment) && $vertical_alignment === "bottom"):?>

    <?php /***  VERTICAL ALIGN BOTTOM  ***/?>
    <div class="mirror-quote-container <?php echo $additional_classes; ?>"
        <?php if(!empty($data_hook)){ echo "data-hook='" . $data_hook . "'"; } ?>
        <?php if(!empty($data_offset)){ echo "data-offset='" . $data_offset . "'"; } ?>
    >
        <div class="mirror-box <?php if(isset($box_color) && !empty($box_color)){ echo $box_color; } ?><?php if( isset($fixed_mirror_box) ){ echo " fixed-height-mirror-box"; } ?>">
            <div class="mirror-box-line top"></div>
            <div class="mirror-box-line right"></div>
            <div class="mirror-box-line bottom"></div>
            <div class="mirror-box-line left"></div>
        </div>
        <?php if(isset($image) && !empty($image)) { ?>
            <div class="mirror-image-container">
                <div class="mirror-image" style="background-image: url('<?php echo $image; ?>');"></div>
            </div>
        <?php } ?>
        <p class="quote"><?php if(isset($quote) && !empty($quote)) { echo $quote; }?></p>
    </div>

<?php else: ?>

    <?php /***  VERTICAL ALIGN TOP  ***/?>
    <div class="mirror-quote-container <?php echo $additional_classes; ?>">
        <div class="mirror-box <?php if(isset($box_color) && !empty($box_color)){ echo $box_color; } ?>">
            <div class="mirror-box-line top"></div>
            <div class="mirror-box-line right"></div>
            <div class="mirror-box-line bottom"></div>
            <div class="mirror-box-line left"></div>
        </div>
        <p class="quote"><?php if(isset($quote) && !empty($quote)) { echo $quote; }?></p>
        <?php if(isset($image) && !empty($image)) { ?>
            <div class="mirror-image-container">
                <div class="mirror-image" style="background-image: url('<?php echo $image; ?>');"></div>
            </div>
        <?php } ?>
    </div>

<?php endif; ?>