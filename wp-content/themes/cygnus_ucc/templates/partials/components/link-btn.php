<?php
if (isset($link) && !empty($link) && is_string($link) && !empty($link_text)) { ?>

<div class="col-float">
    <a href="<?php echo $link; ?>" aria-label="<?php echo $link_text; ?>" class="link-btn <?php echo $color_class; ?> <?php echo $display_class; ?> <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
        <span class="link-text"><?php echo $link_text; ?></span>
    </a>
</div>

<?php }