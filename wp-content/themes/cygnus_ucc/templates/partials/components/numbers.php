<?php

/**
 * This partial needs to be called within The Loop
 */

//if (have_rows('promotions')):
//    while (have_rows('promotions')) : the_row();
?>

    <div class="number-component">
        <div class="row no-gutters">
            <div class="col col-12">
                <p class="micro-title"><?php if(isset($micro_title)){ echo $micro_title; }?></p>
            </div>
            <div class="col col-12">
                <div class="row flex-nowrap no-gutters">
                    <div class="col number-col">
                        <div class="number-container">
                          <?php if(isset($number) && $number !== ''){ ?>
                            <div class="cypher number-placeholder" data-direction="increase"><?php echo $number; ?></div>
                          <?php } ?>
                        </div>
                    </div>
                    <div class="col">
                        <div class="label-container">
                            <p class="uom"><?php if(isset($unit_of_measure)){ echo $unit_of_measure; }?></p>
                            <p class="text"><?php if(isset($number_text)){ echo $number_text; }?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//    endwhile;
//endif;
