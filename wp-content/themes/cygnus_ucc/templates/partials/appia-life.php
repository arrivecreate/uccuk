<style>
    @import url('https://fonts.googleapis.com/css?family=Athiti:500&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Play:700&display=swap');

    @font-face {
        font-family : "Big River Script";
        font-style  : normal;
        font-weight : 700;
        src         : url("/wp-content/themes/cygnus_ucc/dist/fonts/big_river_script.ttf") format("truetype");
    }

    .section-appia-life-sub .subcontent {
      font-family: "Athiti", sans-serif;
      font-weight: 500;
      font-size: 1.8rem;
      line-height: 2rem;
      margin-bottom: 1rem;
    }

    .section-appia-life .appia-life-logo {
      margin-top: 20px;
      margin-bottom: 20px;
    }

    .section-appia-life h3.title, .section-appia-life-sub h3.title {
      font-family: "Big River Script", sans-serif;
      font-size: 2.1rem;
      line-height: 1.34;
      text-transform: none;
    }
</style>

<section class="section-scrollable section-appia-life">
  <div class="container-fluid container-fluid-cap">
    <div class="row">
      <div class="content-col col col-12 col-sm-12 col-md-4 common-pad-l align-self-center text-content">
        <div class="text-content-inner">
          <img src="<?= get_template_directory_uri() . '/appia-life/appia-logo@2x.png' ?>" alt="appia-life-logo"
               class="appia-life-logo">
          <h3 class="title">Coffee machines for everyday</h3>
          <a href="/wp-content/uploads/2019/11/Caffe-Culture-Brochure_Digital_3-compressed.pdf" class="link-btn white-btn red-text-hover d-inline-block">
            <span class="link-text">Download Brochure</span>
          </a>
        </div>
      </div>
      <div class="image-col col col-12 col-sm-12 col-md-8 common-pad-l right-pad-for-menu">
        <img src="<?= get_template_directory_uri() . '/appia-life/AppiaLife_2grAlti_bianco_profilo-MS@2x.png' ?>"
             alt="appia-life" class="machine-image slide-in-img slide-from-bottom-img">
      </div>
    </div>
  </div>
</section>

<section class="section-scrollable section-appia-life-sub pt-5 pb-5">
  <div class="container-fluid container-fluid-cap pt-5 pb-5">
    <div class="row">
      <div class="content-col col col-12 col-md-5 offset-md-1 common-pad-l">
      <div class="subcontent red-font">
        Appia Life guarantees world-class performance and consistently great tasting espresso.
      </div>
        <img src="<?= get_template_directory_uri() . '/appia-life/ns-logo.png' ?>" alt="Nuova Simonelli Logo"
             class="equipment-logo img-fluid">
      </div>
      <div class="content-col col col-12 col-md-5 common-pad-l">
        <p>
          The next generation of Appia: the coffee machine loved by thousands of baristas worldwide. Appia Life takes on
          the same proven technology for exceptional coffee quality but with new, enhanced features, energy saving and
          barista-friendly design. To make great coffee even simpler.
          <br/>
          <br/>
          For more information <a href="/our-equipment/appia-life/" class="red-font">click here</a>.
        </p>
      </div>
    </div>
  </div>
</section>

<style>
  .appia-life-logo {
    display : block
  }

  .section-appia-life {
    background-image    : url("<?= get_template_directory_uri().'/appia-life/bg@2x.png'?>");
    background-color    : #BC001E;
    background-repeat   : no-repeat;
    background-position : center;
    background-size     : cover;
    color               : #fff;
  }

  .section-appia-life .text-content {
    align-items     : flex-end;
    display         : flex;
    justify-content : flex-end;
    min-height      : 800px;
  }

  .section-appia-life .text-content .text-content-inner {
    padding-bottom : 80px;
  }

  .section-appia-life .text-content .text-content-inner p {
    margin : 1.8rem 0;
  }


  .section-appia-life .machine-image {
    position          : absolute;
    bottom            : -150px;
    max-width         : 810px;
    height            : auto;
    left              : 50%;
    transform         : translateX(-50%);
  }

  .section-appia-life-sub .red-font {
    color : #BC001E;
  }

  .equipment-logo {
    margin-bottom:20px;
  }

  @media screen and (max-width : 1400px) {
    .section-appia-life .machine-image {
      max-width: 600px;
      bottom: -120px;
      transform: translateX(30%) matrix(1, 0, 0, 1, -405, 0) !important;
    }
    .section-appia-life .text-content {
      min-height: 573px;
    }
  }

  @media screen and (max-width : 992px) {
    .section-appia-life .text-content {
      min-height      : auto;
      justify-content : flex-start;
    }

    .section-appia-life .text-content .text-content-inner {
      padding-top    : 160px;
      padding-bottom : 10px;
    }

    .section-appia-life .machine-image {
      bottom: -55px;
      max-width: 60%;
      height: auto;
      right: 0;
      left: 100%;
      transform: translateX(-130%) !important;
    }
  }
  @media screen and (max-width : 982px) {
    .section-appia-life .text-content .text-content-inner {
      padding-top: 120px;
    }
  }

  @media screen and (max-width : 894px) {
    .section-appia-life .text-content .text-content-inner {
      padding-top: 90px;
    }
  }

  @media screen and (max-width : 768px) {
    .section-appia-life .text-content .text-content-inner {
      padding-top : 80px;
    }

    .section-appia-life .machine-image {
      display:none;
    }
  }

  @media screen and (max-width : 767px) {
    .section-appia-life .text-content .text-content-inner {
      padding-top : 100px;
    }
  }

  @media screen and (max-width : 576px) {
    .section-appia-life .link-btn.white-btn {
      font-size: 0.8em;
    }
    .section-appia-life .text-content {
      font-size : 1em;
    }

    .section-appia-life .text-content {
      justify-content : flex-start;
    }

    .section-appia-life .text-content .appia-life-logo {
      width : 40%;
    }

    .section-appia-life .text-content .text-content-inner {
      padding-top: 60px;
      padding-bottom : 10px;
    }

    .section-appia-life .machine-image {
      bottom    : -35px;
      max-width : 35%;
      height    : auto;
      right     : 0;
      left      : 100%;
      transform: translateX(-130%)!important
    }

  }

  @media screen and (max-width : 576px) {
    .section-appia-life .text-content .text-content-inner {
      padding-top: 20px;
      padding-bottom : 10px;
    }
  }

</style>
