<?php

/**
 * This partial needs to be called within The Loop
 */


if (have_rows('classic_image_hero')):
    while (have_rows('classic_image_hero')) : the_row(); ?>

        <div class="custom-cpt-hero">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="hero-classic-img"
                         style="background-image: url('<?php echo get_image_url(get_sub_field('background_image')); ?>')"></div>

                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
                    </a>

                    <div class="current-page-container">
                        <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                    </div>

                    <div class="line-col col col-12 col-md-3 order-1 order-md-1">
                        <div class="line-container vertical-line hero-line-container thick">
                            <div class="fill-line red-line"></div>
                        </div>
                    </div>

                    <div class="content-col col col-12 col-md-6 order-2 order-md-2 align-self-center">
                        <div class="hero-classic-content">
                            <?php if (!empty(get_image_url(get_sub_field("logo")))) { ?>
                                <img src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="<?php echo get_sub_field('title'); ?> Logo"
                                     class="equipment-logo img-fluid">
                            <?php } else { ?>
                                <h1 class="title"><?php echo get_sub_field('title'); ?></h1>
                            <?php } ?>

                            <div class="line-container thick">
                                <div class="fill-line dark-line"></div>
                            </div>

                            <div class="subcontent"><?php echo get_sub_field('sub_title'); ?></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile;
endif;