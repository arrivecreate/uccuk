<?php

/**
 * This partial needs to be called within The Loop
 */


if (have_rows('image_hero')):
    while (have_rows('image_hero')) : the_row(); ?>

        <div class="image-hero-container white-label-hero">
            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>

            <div class="landing-container">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 main-image-container">
                            <div class="main-image" style="background-image: url('<?php echo get_image_url(get_sub_field('main_image')); ?>');"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="hero-content">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 line-col">
                            <div class="line-container thick vertical-line" data-hook="1">
                                <div class="fill-line red-line"></div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col col-md-8 thick-pad-l">
                            <?php if(get_sub_field('micro_title')): ?>
                                <h1 class="micro-title black-title"><?php echo get_sub_field('micro_title'); ?></h1>
                            <?php endif; ?>
                            <?php if(get_sub_field('micro_title')): ?>
                                <h2 class="title red-title"><?php echo get_sub_field('title'); ?></h2>
                            <?php else: ?>
                                <h1 class="title red-title"><?php echo get_sub_field('title'); ?></h1>
                            <?php endif; ?>
                        </div>
                        <div class="col horizontal-line-col align-self-center">
                            <div class="line-container thick">
                                <div class="fill-line dark-line"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-11 thick-pad-l">
                            <p class="subtitle"><?php echo get_sub_field('sub_title'); ?></p>
                            <p class="subcontent"><?php echo get_sub_field('sub_content'); ?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <?php endwhile;
endif;

