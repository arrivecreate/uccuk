<?php

/**
 * This partial needs to be called within The Loop
 */

if (have_rows('promotions')): ?>

    <div class="promotion-container">
        <?php while (have_rows('promotions')) : the_row(); ?>
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="col col-12 thick-pad-l">
                        <h3 class="title red"><?php echo get_sub_field('title'); ?></h3>
                    </div>
                    <div class="col col-12 col-md-7 image-col">
                        <img class="img-fluid slide-in-img slide-from-left-img" data-movement="20" data-delay="+=0.35" data-opacity="0" src="<?php echo get_image_url(get_sub_field('image')); ?>" alt="Promo Img">
                    </div>
                    <div class="col col-12 col-md-5 content-col">
                        <h4 class="sub-title right-pad-for-menu"><?php echo get_sub_field('sub_title'); ?></h4>
                        <div class="extended-line-container">
                            <div class="line-container thick">
                                <span class="fill-line red-line"></span>
                            </div>
                        </div>
                        <div class="subcontent right-pad-for-menu"><?php echo get_sub_field('sub_content'); ?></div>
                        <?php
                        $rows_loop_name = "links";
                        $color_class = "dark-btn";
                        include('components/link-row-container.php');
                        ?>
                    </div>
                </div>
            </div>
            <?php
        endwhile; ?>
    </div>

<?php endif;