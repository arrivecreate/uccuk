<?php

/**
 * This partial needs to be called within The Loop
 */

if (have_rows('image_hero') && get_sub_field("version_with_side_image")) :

    if (have_rows('image_hero')):
        while (have_rows('image_hero')) : the_row(); ?>

            <div class="image-hero-container">
                <div class="container-fluid container-fluid-cap">
                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
                    </a>

                    <div class="current-page-container">
                        <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                    </div>
                </div>

                <div class="landing-container">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="side-image-container common-pad-r">
                                <div class="side-image" style="background-image: url('<?php echo get_image_url(get_sub_field('side_image')); ?>')"></div>
                            </div>
                            <div class="col col-sm-10 main-image-container">
                                <div class="main-image" style="background-image: url('<?php echo get_image_url(get_sub_field('main_image')); ?>');"></div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="hero-content">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="col col-sm-10 line-col">
                                <div class="inner-padding">
                                    <div class="line-container thick vertical-line" data-hook="1">
                                        <div class="fill-line red-line"></div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="titles-col col col-sm-8 col-xl-7 thick-pad-l">
                                <?php if (get_sub_field('micro_title')): ?>
                                    <h1 class="micro-title black-title"><?php echo get_sub_field('micro_title'); ?></h1>
                                <?php endif; ?>
                                <?php if (get_sub_field('micro_title')): ?>
                                    <h2 class="title red-title"><?php echo get_sub_field('title'); ?></h2>
                                <?php else: ?>
                                    <h1 class="title red-title"><?php echo get_sub_field('title'); ?></h1>
                                <?php endif; ?>
                            </div>
                            <div class="horizontal-line-col col align-self-center">
                                <div class="line-container thick">
                                    <div class="fill-line dark-line"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col col-11 col-md-10 col-xl-9 thick-pad-l">
                                <?php if (get_sub_field('sub_title')) { ?><p class="subtitle"><?php echo get_sub_field('sub_title'); ?></p> <?php } ?>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        endwhile;
    endif;

else:
    $custom_classic_hero_group_name = "image_hero";

    //these two conditions wrap the banner into a container if the template requires it
    if(isset($outer_container_required) && !empty($outer_container_required) ){
        echo "<section class=\"section-scrollable\">
                <div class=\"container-fluid container-fluid-cap\">";
    }

    include('classic-hero.php');

    if(isset($outer_container_required) && !empty($outer_container_required)){
        echo "</div></section>";
    }
endif;

