<?php

/**
 * This partial needs to be called within The Loop
 */


if (have_rows('classic_image_hero')):
    while (have_rows('classic_image_hero')) : the_row();
        $logo = get_image_url(get_sub_field('logo')); ?>

        <div class="hero-classic-container hero-image-classic">
            <div class="hero-classic-img"
                 style="background-image: url('<?php echo get_image_url(get_sub_field('background_image')); ?>')"></div>

            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>

            <div class="hero-classic-content">
                <h1 class="title red-title"><?php echo get_sub_field('title'); ?></h1>

                <?php if (isset($logo) && !empty($logo)): ?>
                    <div class="line-container thick full-width">
                        <div class="fill-line red-line"></div>
                    </div>
                <?php else: ?>
                    <p class="subtitle"><?php echo get_sub_field('sub_title'); ?></p>
                    <div class="line-container thick">
                        <div class="fill-line red-line"></div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
        <?php if (isset($logo) && !empty($logo)): ?>
            <div class="hero-image-classic-logo-row align-items-center">
                <div class="row no-gutters">
                    <div class="col col-12 col-sm-5">
                        <img class="img-fluid hero-logo" src="<?php echo $logo; ?>" alt="Hero Logo">
                    </div>
                    <div class="col col-12 col-sm-7">
                        <p class="subcontent"><?php echo get_sub_field('sub_content'); ?></p>
                    </div>
                </div>
            </div>
        <?php endif; ?>

    <?php endwhile;
endif;