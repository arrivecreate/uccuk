<?php

/**
 * This partial needs to be called within The Loop
 */

$classic_hero_group_name = 'classic_hero';
$classic_hero_image_name = 'background_image';
$classic_hero_subtitle_name = 'sub_title';

/*
With this small condition we can assign a different group name to loop into
This is used for instance byt the Image hero. If the tickbox is not clicked, it relies on the classic hero
(but the name of the fields are different, as this is a change happened 26/05/2018)
We have default values above and if the group name is different, we use different value names for the elements
*/

if (isset($custom_classic_hero_group_name) && !empty($custom_classic_hero_group_name)) {
    $classic_hero_group_name = $custom_classic_hero_group_name;
    $classic_hero_image_name = "main_image";
    $classic_hero_subtitle_name = "sub_content";
}

if (have_rows($classic_hero_group_name)):
    while (have_rows($classic_hero_group_name)) : the_row();
        ?>

        <div class="hero-classic-container">
            <div class="hero-classic-img"
                 style="background-image: url('<?php echo get_image_url(get_sub_field($classic_hero_image_name)); ?>')"></div>

            <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg" alt="<?php bloginfo('name'); ?>">
            </a>

            <div class="current-page-container">
                <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
            </div>

            <div class="hero-classic-content">
                <h1 class="title red-title"><?php echo get_sub_field('title'); ?></h1>
                <div class="subtitle"><?php echo get_sub_field($classic_hero_subtitle_name); ?></div>
                <div class="line-container thick" data-hook="1">
                    <div class="fill-line red-line"></div>
                </div>
            </div>
        </div>

        <?php
    endwhile;
endif;