<header class="main-header">

    <div class="container-fluid container-fluid-cap">
        <div class="row">

            <?php /* <div class="col col-header-mobile-background"></div> UNUSED */?>
            <div class="col">
                <nav id="side-nav">
                    <div class="search-trigger-box">
                        <button id="search-trigger" style="border:0px;" class="search" aria-label="Search">
                            <img style="width:22px;" src="<?php echo get_template_directory_uri(); ?>/assets/images/search.svg" alt="Search">
                        </button>
                        <form action="/" method="GET">
                            <input name="s" placeholder="Search" type="text">
                        </form>
                    </div>
                    <button id="menu-trigger" class="burger" aria-label="Main Menu">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line shorter-line"></span>
                    </button>
                    <ul class="dots hidden">
                    </ul>
                </nav>
            </div>
        </div>
    </div>

    <div id="main-menu">
        <div id="menu-bg-animator"></div>
        <div class="container-fluid container-fluid-cap full-screen">
            <div class="row full-screen align-items-center">
                <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo-white.svg" alt="<?php bloginfo('name'); ?>">
                </a>
                <div class="col text-right main-menu-col-pad">
                    <div class="close-btn"><i class="fas fa-times"></i></div>
                    <?php
                    if (has_nav_menu('primary_navigation')) :
                        wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'main-nav']);
                    endif;
                    ?>
                </div>
            </div>
        </div>.

    </div>
</header>
