<?php
global $post;

if($post){
    $show_footer_options = get_field('show_footer_options', $post->ID);
    $contact_options = get_field('contact_us_override', $post->ID);
}else{
    $show_footer_options = get_field("show_footer_options", "options");
    $show_contact_us = get_field("contact_us_display", "options");
}

$footer_options = 'footer_options_group';
$options = '';

if ($show_footer_options == 'no' || $show_footer_options == '') {
    $options = 'options';
}

if($contact_options == true) {
  $display = get_field('contact_us_display', $post->ID);
} elseif ($contact_options == NULL) {
  $display = get_field("contact_us_display", "options");
} else {
  $display = $show_contact_us;
}

?>

<footer class="content-info">

    <div class="invisible-trigger xs d-block d-sm-none"></div>
    <div class="invisible-trigger sm d-none d-sm-block d-md-none"></div>
    <div class="invisible-trigger md d-none d-md-block d-lg-none"></div>
    <div class="invisible-trigger lg d-none d-lg-block d-xl-none"></div>
    <div class="invisible-trigger xl d-none d-xl-block"></div>
    <div class="invisible-trigger fs d-none"></div>
    <div class="invisible-trigger navigation-xxl-trigger d-none"></div>

    <section class="section-scrollable phone-number-section">
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <?php
                if (have_rows($footer_options, $options)) :
                    while (have_rows($footer_options, $options)) : the_row();

                        $title_and_number_order = $phased_image_order = $text_alignment = $pad = '';
                        $section_alignment = get_sub_field('alignment');

                        if ($section_alignment == 'left') {
                            $text_alignment = "right";
                            $title_and_number_order = 'order-2 order-md-1';
                            $phased_image_order = 'col-sm-5 order-1 order-sm-2 offset-sm-1 common-pad-r';
                            $pad = "thick-pad-l";
                        }else{
                            $text_alignment = "left";
                            $title_and_number_order = 'order-2 offset-sm-1';
                            $phased_image_order = 'col-sm-5 order-1 common-pad-l';
                            $pad = "thick-pad-r";
                        }
                        ?>
                        <div class="title-and-number-col col col-12 col-sm-6 <?php echo $pad; ?> <?php echo $title_and_number_order ?>">
                            <h3 class="title"> <?php echo get_sub_field('title'); ?></h3>
                            <h4 class="phone_number">
                                <a href="tel:<?php echo get_sub_field('phone_number'); ?>"><?php echo get_sub_field('phone_number'); ?>
                                    <div class="line-container phone-number-underline thick">
                                        <span class="fill-line dark-line"></span>
                                    </div>
                                </a>
                            </h4>

                        </div>


                        <div class="col col-12 phased-img-col <?php echo $phased_image_order ?>">
                            <?php
                                $quote = get_sub_field('quote');
                                $image_offset_plus = true;
                                if (!isset($quote) || empty($quote)) {
                                    $quote = "It's all about<br>the experience";
                                }
                                $image = get_image_url(get_sub_field('image'));
                                // if ($image) {  //necessary if we are in a custom post type
                                //     $image = $image['url'];
                                // }
                                include('partials/components/mirror-quote.php');
                            ?>
                        </div>
                    <?php endwhile;
                endif;
                ?>
            </div>
        </div>
    </section>
    <div class="footer-red">
        <section class="form-section">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                     <div class="form-col col col-sm-6 <?php if ((isset($section_alignment) && $section_alignment == 'right')) { echo "offset-sm-6 thick-pad-r"; } else { echo "thick-pad-l"; } ?>">
                        <div class="pre-form-line-container">
                             <div id="pre-form-line" class="line-container vertical-line thick <?php if (isset($section_alignment) && $section_alignment == 'right') { echo "right-aligned"; } ?>" data-delay="0.6">
                                 <span class="fill-line red-line"></span>
                             </div>
                        </div>

                        <div id="footer_form_lbl">
                            <?php echo do_shortcode('[contact-form-7 id="1368" title="Footer Contact Form"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="bottom-section">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="col <?php if(isset($section_alignment) && $section_alignment == 'left'){ echo "col-sm-7 thick-pad-l common-pad-r"; }else{ echo "col-sm-6 common-pad-l thick-pad-r"; }?>">
                        <div class="total-coffee-solution">
                            <span class="tcs">The total <br>coffee solution<br></span>
                            <ul class="social-icons">
                                <li>
                                    <a href="https://www.youtube.com/channel/UCpUQOyd7nDNhMZFZD_vheZQ">
                                        <img src="/wp-content/themes/cygnus_ucc/dist/images/youtube.svg" alt="Youtube">
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/UCCCoffeeUK">
                                        <img src="/wp-content/themes/cygnus_ucc/dist/images/twitter.svg" alt="Twitter">
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/1971362?trk=tyah&trkInfo=tarId:1406029210138,tas:ucc%20coffee,idx:2-1-2">
                                        <img src="/wp-content/themes/cygnus_ucc/dist/images/linked-in.svg" alt="LinkedIn">
                                    </a>
                                </li>
                                <li>
                                    <a href="https://www.instagram.com/ucccoffeeuk/?hl=en">
                                        <img src="/wp-content/themes/cygnus_ucc/dist/images/instagram.svg" alt="Twitter">
                                    </a>
                                </li>
                            </ul>

                            <div class="company-information">
                                <?php
                                $company_info = get_field("footer_company_information", "options");

                                if ($company_info && $company_info['telephone_number']) {
                                    echo "<p><a href='tel:" . $company_info['telephone_number'] . "'></a>T: " . $company_info['telephone_number'] . "</p>";
                                }

                                if ($company_info && $company_info['address']) {
                                    echo "<p>" . $company_info['address'] . "</p>";
                                }
                                ?>

                                <div class='row'>
                                    <div class='col vat-and-registrations'>
                                        <?php
                                        if ($company_info && $company_info['vat']) {
                                            echo "<p>VAT: " . $company_info['vat'] . "</p>";
                                        }

                                        if ($company_info && $company_info['weee_registration_number']) {
                                        echo "<p>WEEE Registration No: " . $company_info['weee_registration_number'] . "</p>";
                                        }

                                        if ($company_info && $company_info['company_registration']) {
                                            echo "<p>Registration Number: " . $company_info['company_registration'] . "</p>";
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>


                            <div class="legal-links">
                                <a class="privacy" href="/privacy/">Privacy &amp; Cookie Policy</a>
                                <a class="legal-docs" href="/legal-docs/">Legal Docs</a>
                                <a class="sitemap" href="/sitemap/">Sitemap</a>
                                <?php /* <a class="terms-and-conditions" href="/terms-and-conditions">Terms And Conditions</a> */ ?>
                            </div>
                        </div>
                    </div>
                    <div class="col logo-col">
                        <div class="logo-container">
                            <img class="img-fluid" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo-white.svg" alt="UCC Logo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<?php if($display) : ?>
<div id="fixed-contact-us-btn-area" class="show"><button id="fixed-contact-us-btn" class="link-btn white-btn let-s-talk-link"><span class="icon"><i class="fas fa-envelope"></i></span><span class="link-text">Contact Us</span></button></div>
<?php endif; ?>

<script>
        jQuery('#search-trigger').on('click', function(){
            jQuery('#search-trigger').toggleClass('active');
        }) 
    </script>