<article <?php post_class(); ?> style="margin-bottom:20px;">
  <header>
    <p class="quote" style="margin-bottom:0px;font-size:36px;line-height:42px;"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
    <?php if (get_post_type() === 'post') { get_template_part('templates/entry-meta'); } ?>
  </header>
  <div class="entry-summary">
    <?php the_excerpt(); ?>
  </div>
</article>
