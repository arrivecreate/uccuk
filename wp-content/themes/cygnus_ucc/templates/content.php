<article <?php post_class(); ?>>
    <h4 class="title entry-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

    <?php

    //and we check if we have also a secondary image, that we will prefer to create the mirror quote
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];
    $secondary_image = get_field('secondary_image');
    if(!empty($secondary_image)){
        $image = $secondary_image;
    }else{
        if(has_post_thumbnail()){
            $image = $thumb_url;
        }else{
            $image = "/wp-content/themes/cygnus_ucc/dist/images/ucc_post_thumbnail_fallback.jpg";
        }
    }

    ?>
    <a href="<?php echo get_permalink($post->ID); ?>">
        <div class="image-container" style="background-image:url('<?php echo $image; ?>')"></div>
    </a>

    <div class="subcontent entry-summary" data-mh="subcontent">
        <a href="<?php echo get_permalink($post->ID); ?>">
            <?php the_excerpt(); ?>
        </a>
    </div>

    <div class="moretag">
        <a class="link-btn dark-btn d-inline-block" href="<?php echo get_permalink($post->ID); ?>"><span class="link-text">More</span></a>
    </div> 
</article>
