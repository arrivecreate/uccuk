<?php
/**
 * Template Name: White Label Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Two Column Vertical - White Label stuff here!
     */
    $outer_container_required = true;
    include('templates/partials/image-hero.php');

    $two_column_regular = true;
    include('templates/partials/white-label.php');

endwhile; ?>
