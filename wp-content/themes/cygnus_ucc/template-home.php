<?php
/**
 * Template Name: Home Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
  <style>
    .hero-classic-container {
      margin-bottom:0px;
    }
    .section-tcs-2 {
        color:white;
        background-color:#C32C25;
    }
    .section-tcs-1 {
        padding-bottom:0px !important;
    }
    <?php if(get_the_ID() == "5818") { ?>
    @media(max-width:768px) {
        .row.mobile-reverse {
            flex-direction: column-reverse;
        }
        body, html {
            overflow-x:hidden;
        }
        .page-template-template-home .section-3 .box-image-col .box-image-container {
            top:0px !important;
            margin-bottom:0px !important;
        }
        .section-tcs-2 .title-and-subcontent-container {
            margin-top:20px;
        }
        .section-tcs-2 {
            padding-bottom:5% !important;
        }
    }
    @media(screen and (min-width: 768px) and (max-width: 1200px)) {
        .page-template-template-home .section-3 {
            padding-bottom:25%;
        }
    }
    <?php } ?>
    @media screen and (max-width: 1919px) {
        .main, div[role=document], footer {
            overflow-x: visible 
        }
    }
</style>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>

<?php if(get_the_ID() != "5818") { ?>
  <section class="section-scrollable">
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="col col-12 col-md-11 offset-md-1 col-lg-11 offset-lg-1 pb-5 pt-1 align-self-center">
          <p><strong style="color: #d51216;"><?= get_field('corvid_content','options'); ?></strong></p>
          <?php if(get_field('file_or_url','options') == 'file') {  ?>
            <a href="<?= get_field('corvid_file','options'); ?>" target="_blank" aria-label="Our work" class="link-btn dark-btn d-inline-block ">
              <span class="link-text">View Updates</span>
            </a>
          <?php } ?>
          <?php if(get_field('file_or_url','options') == 'url') {  ?>
            <a href="<?= get_field('url','options'); ?>" target="_blank" aria-label="Our work" class="link-btn dark-btn d-inline-block ">
              <span class="link-text">View Updates</span>
            </a>
          <?php } ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>

  <!-- appia life -->
  <?php /* get_template_part('templates/partials/appia-life', 'classic-hero-blog'); */ ?>
  <!-- appia life -->

  <!-- eco capsule -->
  <?php if(get_the_ID() == "5818") { ?>
    <div class="new-home-rev-slider">
  <?php } ?>

        <?php get_template_part('templates/partials/eko-pod'); ?>

  <?php if(get_the_ID() == "5818") { ?>
    </div>
  <?php } ?>
  <!-- eco capsule -->

    <?php if(get_the_ID() != "5818") { ?>
    <?php //SECTION ONE
    if (have_rows('section_one')):
        while (have_rows('section_one')) : the_row(); ?>

            <div class="section-1-overlapping-img">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-3 pl-0">
                            <div id="home-section-1-img" class="slide-in-img slide-from-bottom-img"
                                 style="background-image: url('<?php echo get_image_url(get_image_url(get_sub_field('image'))); ?>');" data-hook="0.58"></div>
                        </div>
                    </div>
                </div>
            </div>

            <section class="section-scrollable section-1">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-6 offset-md-5 col-lg-5 offset-lg-5">
                            <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "dark-btn";
                            include('templates/partials/components/link-row-container.php');
                            ?>

                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;
    }

    //SECTION TWO
    if (have_rows('section_two')):
        while (have_rows('section_two')) : the_row(); ?>

            <section class="section-scrollable section-2"  <?php if(get_the_ID() == "5818") { ?> style="overflow:hidden" <?php } ?>>
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="image-col col col-12 col-md-5 order-1 order-md-2 align-self-start">
                            <?php
                            $text_alignment = "right";
                            $image = get_image_url(get_sub_field('image'));
                            $quote = get_sub_field('quote');
                            $vertical_alignment = "bottom";
                            $box_color = "white-lines";
                            $data_offset = "-150";
                            $data_hook = "0.82";
                            include('templates/partials/components/mirror-quote.php');
                            ?>
                        </div>
                        <div class="content-col col col-12 col-md-6 order-2 order-md-1 thick-pad-l align-self-center">
                            <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "white-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>

                        </div>

                        <div class="col col-12 order-3 bottom-line-col thick-pad">
                            <div class="bottom-line-container">
                                <div id="section-2-bottom-line" class="line-container vertical-line thick">
                                    <span class="fill-line white-line"></span>
                                </div>
                                <div class="pre-item-line-container">
                                    <div id="pre-item-line" class="line-container vertical-line thick">
                                        <span class="fill-line red-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION THREE
    if (have_rows('section_three')):
        while (have_rows('section_three')) : the_row(); ?>

            <section class="section-scrollable section-3">
                <div class="container-fluid container-fluid-cap">
                    <div class="row align-items-top">
                        <div class="box-image-col col col-12 col-md-6 pl-0 order-1">
                            <div class="box-image-container slide-in-img slide-from-bottom-img" data-movement="30"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('box_image')); ?>')"></div>

                            <div class="thick-pad-l case-study">
                                <?php

                                while (have_rows('info_title')) : the_row();

                                    if (get_row_layout() == 'standard_sub_block'): ?>
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <?php elseif (get_row_layout() == 'number_sub_block'):
                                        $micro_title = get_sub_field('micro_title');
                                        $number = get_sub_field('number');
                                        $unit_of_measure = get_sub_field('number_unit');;
                                        $number_text = get_sub_field('number_text');
                                        include('templates/partials/components/numbers.php');
                                    endif;

                                endwhile;

                                ?>
                                <?php if (get_sub_field('logo')) { ?>
                                    <img class="img-fluid home-case-study-logo" src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Case Study Logo">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="right-col col col-12 col-md-6 order-2 common-pad-l right-pad-for-menu">
                            <div class="title-and-subcontent-container">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "dark-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>

                                <div class="line-container-multi-line">
                                    <div class="fill-line top docked-right" data-order="1"></div>
                                    <div class="fill-line vertical-line left docked-top" data-order="2"></div>
                                    <!-- <div class="fill-line bottom docked-left" data-order="4"></div>
                                         <div class="fill-line right docked-top" data-order="3"></div> -->
                                </div>
                            </div>
                        </div>


                        <?php
                        if (get_sub_field("percentage_block_or_logo_slider_flag") === "logo_slider") {
                            if (have_rows('logo_slider_block')):
                                while (have_rows('logo_slider_block')) : the_row(); ?>
                                    <div class="logo-slider-col col col-12 order-3 bottom-line-col thick-pad">
                                        <?php $logo_slider_title = get_sub_field("logo_slider_title"); ?>

                                        <?php if (have_rows('logos_repeater')): ?>
                                            <div class="row">
                                                <div class="col col-md-6">
                                                    <?php if ($logo_slider_title) { ?>
                                                        <span class="title"><?php echo $logo_slider_title; ?></span>
                                                    <?php } ?>
                                                    <div class="row logo-list">
                                                        <?php
                                                        $i = 0;
                                                        while (have_rows('logos_repeater')) : the_row(); ?>
                                                            <div class="logo-col col col-md-6 col-xl-4 <?php echo ($i == 0) ? "active" : ""; ?>" data-logo="<?php echo $i; ?>">
                                                                <div class="logo" style="background-image: url('<?php echo get_image_url(get_sub_field("logo")); ?>');"></div>
                                                            </div>
                                                            <?php $i++;
                                                        endwhile; ?>
                                                    </div>
                                                    <?php
                                                    $rows_loop_name = "links";
                                                    $color_class = "dark-btn";
                                                    $additional_classes_links = "d-none d-md-block";
                                                    include('templates/partials/components/link-row-container.php');
                                                    ?>
                                                </div>
                                                <div class="col col-md-6">
                                                    <div class="mirror-quote-container text-right top-quote logo-mqc">
                                                        <div class="mirror-box red-lines">
                                                            <div class="mirror-box-line top"></div>
                                                            <div class="mirror-box-line right"></div>
                                                            <div class="mirror-box-line bottom"></div>
                                                            <div class="mirror-box-line left"></div>
                                                        </div>
                                                        <?php
                                                        $i = 0;
                                                        while (have_rows('logos_repeater')) : the_row(); ?>
                                                            <div class="couple-quote-container cqc-logo-<?php echo $i; ?>">
                                                                <p class="quote quote-logo-<?php echo $i; ?>"><?php echo get_sub_field("logo_quote"); ?></p>
                                                                <div class="mirror-image-container mic-logo-<?php echo $i; ?>">
                                                                    <div class="mirror-image"
                                                                         style="background-image: url('<?php echo get_image_url(get_sub_field("logo_quote_related_image"), 'large'); ?>');"></div>
                                                                </div>
                                                            </div>
                                                            <?php $i++;
                                                        endwhile; ?>
                                                    </div>

                                                    
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                <?php
                                endwhile;
                            endif;
                        } else { ?>
                            <div class="col col-12 order-3 bottom-line-col thick-pad">
                                <div class="bottom-line-container">
                                    <div id="section-2-bottom-line" class="line-container vertical-line thick">
                                        <span class="fill-line white-line"></span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION FOUR
    if (have_rows('section_four')): ?>

    <?php if(get_the_ID() == '5818') { ?> 
        <section class="section-scrollable section-4">
            <div class="container-fluid container-fluid-cap">
                <?php while (have_rows('section_four')) : the_row(); ?>
                    <div class="row">
                        <div class="mirror-quote-col col col-12 col-md-6 common-pad new-home-quote">
                            <?php
                            $text_alignment = "left";
                            $image = get_image_url(get_sub_field('image'));
                            $quote = get_sub_field('quote');
                            $vertical_alignment = "top";
                            $box_color = "red-lines";
                            include('templates/partials/components/mirror-quote.php');
                            ?>
                        </div>
                        <div class="what-we-do-col col col-12 col-md-6 thick-pad-r common-pad-l" style="padding-top:11%; padding-bottom:19%;">
                            <?php if (get_sub_field("what_we_do_title")) { ?>
                                <h3 class="what-we-do-title"><?php echo get_sub_field("what_we_do_title") ?></h3>
                            <?php }

                            if (get_sub_field("what_we_do_paragraph")) { ?>
                                <div class="subcontent our-partners-paragraph"><?php echo get_sub_field("what_we_do_paragraph") ?></div>
                            <?php }

                            $rows_loop_name = "what_we_do_links";
                            $color_class = "white-btn";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                    </div>
                    <div class="row mobile-reverse">
                        <div class="logo-list-col col col-12 col-md-6 thick-pad-l common-pad-r new-home-machine-paragraph">
                            <?php if (get_sub_field("our_partners_title")) { ?><h3
                                    class="title"><?php echo get_sub_field("our_partners_title") ?></h3><?php } ?>
                            <?php if (get_sub_field("our_partners_paragraph")) { ?>
                                <div class="subcontent our-partners-paragraph"><?php echo get_sub_field("our_partners_paragraph") ?></div><?php } ?>
                            <?php
                            if (have_rows('partners')): ?>
                                <div class="row">
                                    <?php while (have_rows('partners')) : the_row(); ?>
                                        <div class="logo-col col col-6 col-md-6 col-xl-4">
                                            <div class="logo" style="background-image:url('<?php echo get_image_url(get_sub_field("image")); ?>');"></div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif;

                            $rows_loop_name = "our_partners_links";
                            $color_class = "white-btn";
                            $additional_classes_links = "";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                        <div class="box-image-col col col-12 col-md-6 pr-0 new-home-machine">
                            <div class="box-image-container slide-in-img slide-from-bottom-img"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('machine_image')); ?>');"></div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </section>
        <?php } else { ?>
        <section class="section-scrollable section-4">
            <div class="container-fluid container-fluid-cap">
                <?php while (have_rows('section_four')) : the_row(); ?>
                    <div class="row">
                        <div class="box-image-col col col-12 col-md-6 order-1 pr-0">
                            <div class="box-image-container slide-in-img slide-from-bottom-img"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('machine_image')); ?>');"></div>
                        </div>
                        <div class="logo-list-col col col-12 col-md-6 common-pad-l right-pad-for-menu order-2">
                            <?php if (get_sub_field("our_partners_title")) { ?><span
                                    class="our-partners-title"><?php echo get_sub_field("our_partners_title") ?></span><?php } ?>
                            <?php if (get_sub_field("our_partners_paragraph")) { ?>
                                <div class="subcontent our-partners-paragraph"><?php echo get_sub_field("our_partners_paragraph") ?></div><?php } ?>
                            <?php
                            if (have_rows('partners')): ?>
                                <div class="row">
                                    <?php while (have_rows('partners')) : the_row(); ?>
                                        <div class="logo-col col col-6 col-md-6 col-xl-4">
                                            <div class="logo" style="background-image:url('<?php echo get_image_url(get_sub_field("image")); ?>');"></div>
                                        </div>
                                    <?php endwhile; ?>
                                </div>
                            <?php endif;

                            $rows_loop_name = "our_partners_links";
                            $color_class = "white-btn";
                            $additional_classes_links = "";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="what-we-do-col col col-12 col-md-6 thick-pad-l">
                            <?php if (get_sub_field("what_we_do_title")) { ?>
                                <h3 class="what-we-do-title"><?php echo get_sub_field("what_we_do_title") ?></h3>
                            <?php }

                            if (get_sub_field("what_we_do_paragraph")) { ?>
                                <div class="subcontent our-partners-paragraph"><?php echo get_sub_field("what_we_do_paragraph") ?></div>
                            <?php }

                            $rows_loop_name = "what_we_do_links";
                            $color_class = "white-btn";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                        <div class="mirror-quote-col col col-12 col-md-6 order-1 common-pad">
                            <?php
                            $text_alignment = "right";
                            $image = get_image_url(get_sub_field('image'));
                            $quote = get_sub_field('quote');
                            $vertical_alignment = "top";
                            $box_color = "red-lines";
                            include('templates/partials/components/mirror-quote.php');
                            ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </section>
        <?php } ?>
    <?php
    endif;

    if(get_the_ID() != "5818") { 
    //SECTION FIVE
    if (have_rows('section_five')):
        while (have_rows('section_five')) : the_row(); ?>

            <section class="section-scrollable section-5">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="content-col col col-12 col-md-6 order-2 thick-pad-l align-self-center">
                            <div class="inner-padding">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "red-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                            </div>
                        </div>

                        <div class="col col-12 order-3 bottom-line-col thick-pad">
                            <div class="bottom-line-container">
                                <div id="section-2-bottom-line" class="line-container vertical-line thick">
                                    <span class="fill-line white-line"></span>
                                </div>
                                <div class="pre-item-line-container">
                                    <div id="pre-item-line" class="line-container vertical-line thick">
                                        <span class="fill-line red-line"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        <?php
        endwhile;
    endif;
    }
    if(get_the_ID() == "5818") { 
    //SECTION TCS ONE
    if (have_rows('section_tcs_one')):
        while (have_rows('section_tcs_one')) : the_row(); ?>

            <section class="section-scrollable section-3 section-tcs-1">
                <div class="container-fluid container-fluid-cap">
                    <div class="row align-items-top">
                        <div class="box-image-col col col-12 col-md-6 pl-0 order-1">
                            <div class="box-image-container slide-in-img slide-from-bottom-img" data-movement="30"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('box_image')); ?>')"></div>

                            <div class="thick-pad-l case-study">
                                <?php

                                while (have_rows('info_title')) : the_row();

                                    if (get_row_layout() == 'standard_sub_block'): ?>
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <?php elseif (get_row_layout() == 'number_sub_block'):
                                        $micro_title = get_sub_field('micro_title');
                                        $number = get_sub_field('number');
                                        $unit_of_measure = get_sub_field('number_unit');;
                                        $number_text = get_sub_field('number_text');
                                        include('templates/partials/components/numbers.php');
                                    endif;

                                endwhile;

                                ?>
                                <?php if (get_sub_field('logo')) { ?>
                                    <img class="img-fluid home-case-study-logo" src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Case Study Logo">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="right-col col col-12 col-md-6 order-2 common-pad-l right-pad-for-menu">
                            <div class="title-and-subcontent-container">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "dark-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>

                                
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION TCS TWO
    if (have_rows('section_tcs_two')):
        while (have_rows('section_tcs_two')) : the_row(); ?>

            <section class="section-scrollable section-tcs-2 section-3">
                <div class="container-fluid container-fluid-cap">
                    <div class="row align-items-top">
                        <div class="image-col col col-12 col-md-5 order-1 order-md-2 align-self-start">
                            <!-- <div class="box-image-container slide-in-img slide-from-bottom-img" data-movement="30"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('box_image')); ?>')"></div>

                            <div class="thick-pad-l case-study">
                                <?php

                                while (have_rows('info_title')) : the_row();

                                    if (get_row_layout() == 'standard_sub_block'): ?>
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <?php elseif (get_row_layout() == 'number_sub_block'):
                                        $micro_title = get_sub_field('micro_title');
                                        $number = get_sub_field('number');
                                        $unit_of_measure = get_sub_field('number_unit');;
                                        $number_text = get_sub_field('number_text');
                                        include('templates/partials/components/numbers.php');
                                    endif;

                                endwhile;

                                ?>
                                <?php if (get_sub_field('logo')) { ?>
                                    <img class="img-fluid home-case-study-logo" src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Case Study Logo">
                                <?php } ?>
                            </div> -->
                            
                                <?php
                                $text_alignment = "right";
                                $image = get_image_url(get_sub_field('box_image'));
                                $quote = get_sub_field('quote');
                                $vertical_alignment = "bottom";
                                $box_color = "white-lines";
                                $data_offset = "-150";
                                $data_hook = "0.82";
                                include('templates/partials/components/mirror-quote.php');
                                ?>
                        </div>
                        <div class="left-col col col-12 col-md-6 order-1 thick-pad-l">
                            <div class="title-and-subcontent-container">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "white-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    
    //SECTION TCS Three
    if (have_rows('section_tcs_three')):
        while (have_rows('section_tcs_three')) : the_row(); ?>

            <section class="section-scrollable section-3">
                <div class="container-fluid container-fluid-cap">
                    <div class="row align-items-top">
                        <div class="box-image-col col col-12 col-md-6 pl-0 order-1">
                            <div class="box-image-container slide-in-img slide-from-bottom-img" data-movement="30"
                                 style="background-image: url('<?php echo get_image_url(get_sub_field('box_image')); ?>')"></div>

                            <div class="thick-pad-l case-study">
                                <?php

                                while (have_rows('info_title')) : the_row();

                                    if (get_row_layout() == 'standard_sub_block'): ?>
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <?php elseif (get_row_layout() == 'number_sub_block'):
                                        $micro_title = get_sub_field('micro_title');
                                        $number = get_sub_field('number');
                                        $unit_of_measure = get_sub_field('number_unit');;
                                        $number_text = get_sub_field('number_text');
                                        include('templates/partials/components/numbers.php');
                                    endif;

                                endwhile;

                                ?>
                                <?php if (get_sub_field('logo')) { ?>
                                    <img class="img-fluid home-case-study-logo" src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Case Study Logo">
                                <?php } ?>
                            </div>
                        </div>
                        <div class="right-col col col-12 col-md-6 order-2 common-pad-l right-pad-for-menu">
                            <div class="title-and-subcontent-container">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "dark-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    }

    if (get_field('show_promotions') == 'yes') {
        get_template_part('templates/partials/promotions', 'promotions');
    }


endwhile; ?>
