<?php
/**
 * Template Name: Plain Content
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>


    <section class="section-scrollable main-section">
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <div class="col col-10 col-lg-9 thick-pad-l">
                    <?php echo get_field("main_content"); ?>
                </div>
            </div>
        </div>
    </section>

<?php endwhile; ?>