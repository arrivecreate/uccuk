<?php
/**
 * Template Name: Our Work Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Call the Hero and Sub Hero
     */?>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>

    <?php
    $left_right_switch = "left";
    include('templates/partials/sub-hero.php');

    /**
     * Loop through the custom post type and display listing information
     */
    $loop = new WP_Query(
                array(
                    'post_type' => 'case_studies',
                    'post_status' => 'publish',
                    'posts_per_page' => -1
                )
            );

    $left_right_switch = "right";

    if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
            include("templates/partials/case-study-listing-element.php");
            if ($left_right_switch == "left") {
                $left_right_switch = "right";
            } else {
                $left_right_switch = "left";
            }
        endwhile;
    endif;
    wp_reset_postdata();

    /**
     * Call the divided colour block - single ACFs
     */
    get_template_part('templates/partials/divided-colour-block', 'divided-colour-block');

endwhile; ?>
