<?php
/**
 * Template Name: Beyond Coffee Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Divided Colour Block stuff here!
     */?>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>
<?php
    get_template_part('templates/partials/divided-colour-block', 'divided-colour-block');
endwhile; ?>
