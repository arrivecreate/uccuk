<?php
/**
 * Template Name: What We Do Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>

    <?php
    /**
     * Do Divided Colour Block stuff here!
     */
    get_template_part('templates/partials/divided-colour-block', 'divided-colour-block');

endwhile; ?>
