<?php
/**
 * Template Name: To Go Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
<style>
    .hero-classic-container {
      margin-bottom:0px;
    }
    .top-link--box {
        display:flex;
    }
    .section-img img {
        width: 100%;
        height: auto;
    }
    .section-five-left {
        height: 200px;
        width: auto;
        position: absolute;
        left: -200px;
        top: 200px;
        z-index: -1;
    }
    .section-five-right {
        height: 200px;
        width: auto;
        position: absolute;
        right: -200px;
        bottom: 200px;
        z-index: -1;
    }
    h3, h2 {
        font-size: 2.6rem;
        line-height: 2.9rem;
    }

    @media(min-width:768px) {
        .col-float + .col-float {
            margin-top:1rem;
        }
        body, html {
            overflow-x:hidden;
        }
    }
    @media(max-width:1000px) {
        .section-five-left, .section-five-right {
            display: none;
        }
        div.top-link--box {
            display: block;
            text-align: center;
        }
        a.top-link--a, a.top-link--a:active, a.top-link--a:visited {
            display: inline-block;
            width: calc(25% - 25px);
            height: 72px;
            vertical-align: middle;
            margin-bottom: 15px;
        }

        [class*=" section-"] {
            padding:10% 0px !important;
        }
    }

    @media(max-width:768px) {
        .row.mobile-reverse {
            flex-direction: column-reverse;
        }
        .video-image-link {
            margin-top: 2rem;
            display: block;
        }
        a.top-link--a, a.top-link--a:active, a.top-link--a:visited {
            width: calc(50% - 23px) !important;
        }
    }

    .video-image-link {
        position:relative;
    }
    .video-image-link:hover:after {
        opacity:1;
    }
    .video-image-link:after {
        width: 0;
        height: 0;
        border: 46px solid transparent;
        border-left: 80px solid #fff;
        -webkit-filter: drop-shadow(0px 0px 2px rgba(0, 0, 0, 0.25));
        opacity: 0.8;
    }

    .video-image-link:after, .video-link:before {
        transform: translate(calc(-50% + 28px), calc(-50% + 10px));
        content: "";
        position: absolute;
        top: 50%;
        left: 50%;
    }

    .top-link--a, .top-link--a:active, .top-link--a:visited {
        transition: all .33s ease !important;
        padding: 10px 5px;
        text-align: center;
        color: #d51216;
        background-color:white;
        border: 2px solid #fff;
        letter-spacing: 2px;
        text-transform: uppercase;
        flex: 1;
        font-weight: bold;
        font-size: 14px;
        justify-content: space-between;
        display: flex;
        flex-shrink: 1;
        margin: 0px 10px;
        justify-content: center;
        align-items: center;
    }

    .top-link--box {
        margin-top: 30px;
        margin-bottom: 0px !important;
        padding-bottom: 0px !important;
    }

    .top-link--a:hover {
        border-color:white;
        background-color:transparent;
        color:white !important;
    }
    [class*=" section-"] {
        padding:5% 0px;
    }
    .section-1, .section-4 {
        color: #fff;
        background-color: #d51216;
    }
    .section-2 {
        color: #fff;
        background-color: #2C353E;
    }

    /*New Styles*/
    .links-red {
        padding-bottom: 0 !important;
        padding-top: 3rem !important;
    }
    .links-red .top-link--a {
        color: #fff !important;
        border: 2px solid #fff !important;
        transition: all .33s ease !important;
    }
    .links-red .top-link--a:hover {
        background-color: #F4C4C5 !important;
        color: #d51216 !important;
    }
    .section-2 .col-float {
        width: 100%;
    }
    .section-5 {
        text-align: center;
    }
    .section-5 .link-row-container {
        justify-content: center;
    }
    .section-5 .link-row-container .black-btn {
        border: 3px solid #000;
        color: #000;
    }
    .section-5 .link-row-container .black-btn:hover {
        border: 3px solid #d51216;
        color: #d51216;
    }
    .phone-number-section .phone_number {
        display:none !important;
    }
  </style>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>


      <!-- <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
          <div class="row">
          <div class="col col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 pb-1 pt-1 align-self-center">
                <div class="text-center">
                    <h3 class="title red-title" style="color:#d51216;"><?php echo get_field('tcs_header') ?></h3>
                </div>
            </div>
            <div class="col col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 pb-5 pt-1 align-self-center top-link--box">
                <?php $links = get_field('top_links'); ?>
                <?php foreach($links as $l) { ?>
                    <a class="top-link--a" target="<?php echo $l['link']['target']; ?>" href="<?php echo $l['link']['url']; ?>"><?php echo $l['link']['title']; ?></a>
                <?php } ?>
            </div>
          </div>
        </div>
      </section> -->


  <?php //SECTION ONE
    if (have_rows('section_one')):
        while (have_rows('section_one')) : the_row(); ?>
            <section class="section-scrollable section-1">
                <div class="container-fluid container-fluid-cap">
                    <div class="row mobile-reverse">
                        <div class="col col-12 col-md-5 col-lg-5 offset-lg-1 offset-md-1">
                            <?php if(get_sub_field("video_link") != "") { ?>
                            <?php
                                $link = "#video-modal-" . get_sub_field("video_link");
                                $scroll_down_class = "video-modal-link";
                            ?>
                                <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                                        <div class='background-overlay'></div>
                                        <div class="video-container">
                                            <div class='close-btn'><i class='fa fa-times'></i></div>
                                            <?php echo do_shortcode("[lyte id='" . get_sub_field("video_link") . "' /]"); ?>

                                        </div>
                                    </div>

                            <?php $image = get_sub_field("video_thumbnail"); ?>
                            <a href="<?php echo $link; ?>" class="video-image-link video-modal-link">
                                <img style="max-width:100%;" src="<?php echo $image['url']; ?>">
                            </a>
                            <!-- <div class="col-float">
                                <a href="<?php echo $link; ?>" aria-label="Play Video" class="mt-4 link-btn white-btn red-text-hover d-inline-block link-btn <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
                                    <span class="link-text">Play Video</span>
                                </a>
                            </div> -->
                            <?php } ?>
                        </div>
                        <div class="col col-12 col-md-5 col-lg-5">
                            <h2 class="title"><?php echo get_sub_field('title'); ?></h2>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "white-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col col-12 col-md-10 offset-md-1 col-lg-10 offset-lg-1 pb-5 pt-1 align-self-center top-link--box">
                            <?php $links = get_field('top_links'); ?>
                            <?php foreach($links as $l) { ?>
                                <a class="top-link--a" target="<?php echo $l['link']['target']; ?>" href="<?php echo $l['link']['url']; ?>"><?php echo $l['link']['title']; ?></a>
                            <?php } ?>
                        </div>
                    </div> -->
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION TWO
    if (have_rows('section_two')):
        while (have_rows('section_two')) : the_row(); ?>

            <section class="section-scrollable section-2">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-5 col-lg-5 offset-lg-1 offset-md-1">
                            <h2 class="title"><?php echo get_sub_field('title'); ?></h2>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "white-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>

                    <div class="col col-12 col-md-5 col-lg-5">
                            <?php if(get_sub_field("video_link") != "") { ?>
                            <?php
                                $link = "#video-modal-" . get_sub_field("video_link");
                                $scroll_down_class = "video-modal-link";
                            ?>
                                <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                                        <div class='background-overlay'></div>
                                        <div class="video-container">
                                            <div class='close-btn'><i class='fa fa-times'></i></div>
                                            <?php echo do_shortcode("[lyte id='" . get_sub_field("video_link") . "' /]"); ?>

                                        </div>
                                    </div>

                            <?php $image = get_sub_field("video_thumbnail"); ?>
                            <a href="<?php echo $link; ?>" class="video-image-link video-modal-link">
                                <img style="max-width:100%;" src="<?php echo $image['url']; ?>">
                            </a>
                            <!-- <div class="col-float">
                                <a href="<?php echo $link; ?>" aria-label="Play Video" class="mt-4 white-btn red-text-hover d-inline-block link-btn <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
                                    <span class="link-text">Play Video</span>
                                </a>
                            </div> -->
                            <?php } else { ?>
                                <?php
                                    $image = get_sub_field("image");
                                    $size = 'full';
                                ?>
                                <div class="section-img"><?php echo wp_get_attachment_image( $image, $size ); ?></div>
                            <?php } ?>
                        </div>


                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION THREE
    if (have_rows('section_three')):
        while (have_rows('section_three')) : the_row(); ?>

            <section class="section-scrollable section-3">
                <div class="container-fluid container-fluid-cap">
                    <div class="row mobile-reverse">
                    <div class="col col-12 col-md-5 col-lg-5 offset-lg-1 offset-md-1">
                            <?php if(get_sub_field("video_link") != "") { ?>
                            <?php
                                $link = "#video-modal-" . get_sub_field("video_link");
                                $scroll_down_class = "video-modal-link";
                            ?>
                                <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                                        <div class='background-overlay'></div>
                                        <div class="video-container">
                                            <div class='close-btn'><i class='fa fa-times'></i></div>
                                            <?php echo do_shortcode("[lyte id='" . get_sub_field("video_link") . "' /]"); ?>

                                        </div>
                                    </div>

                            <?php $image = get_sub_field("video_thumbnail"); ?>
                            <a href="<?php echo $link; ?>" class="video-image-link video-modal-link">
                                <img style="max-width:100%;" src="<?php echo $image['url']; ?>">
                            </a>
                            <!-- <div class="col-float">
                                <a href="<?php echo $link; ?>" aria-label="Play Video" class="mt-4 dark-btn red-text-hover d-inline-block link-btn <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
                                    <span class="link-text">Play Video</span>
                                </a>
                            </div> -->
                            <?php } else { ?>
                                <?php
                                    $image = get_sub_field("image");
                                    $size = 'full';
                                ?>
                                <div class="section-img"><?php echo wp_get_attachment_image( $image, $size ); ?></div>
                            <?php } ?>
                        </div>
                        <div class="col col-12 col-md-5 col-lg-5">
                            <h2 class="title"><?php echo get_sub_field('title'); ?></h2>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "dark-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>



                    </div>
                </div>
            </section>




        <?php endwhile;
    endif;


    //SECTION Four
    if (have_rows('section_four')):
        while (have_rows('section_four')) : the_row(); ?>

            <section class="section-scrollable section-4">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-5 col-lg-5 offset-lg-1 offset-md-1">
                            <h2 class="title"><?php echo get_sub_field('title'); ?></h2>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                            <?php
                            $rows_loop_name = "links";
                            $color_class = "white-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>

                    <div class="col col-12 col-md-5 col-lg-5">
                            <?php if(get_sub_field("video_link") != "") { ?>
                            <?php
                                $link = "#video-modal-" . get_sub_field("video_link");
                                $scroll_down_class = "video-modal-link";
                            ?>
                                <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                                        <div class='background-overlay'></div>
                                        <div class="video-container">
                                            <div class='close-btn'><i class='fa fa-times'></i></div>
                                            <?php echo do_shortcode("[lyte id='" . get_sub_field("video_link") . "' /]"); ?>

                                        </div>
                                    </div>

                            <?php $image = get_sub_field("video_thumbnail"); ?>
                            <a href="<?php echo $link; ?>" class="video-image-link video-modal-link">
                                <img style="max-width:100%;" src="<?php echo $image['url']; ?>">
                            </a>
                            <!-- <div class="col-float">
                                <a href="<?php echo $link; ?>" aria-label="Play Video" class="mt-4 white-btn red-text-hover d-inline-block link-btn <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
                                    <span class="link-text">Play Video</span>
                                </a>
                            </div> -->
                            <?php } else { ?>
                                <?php
                                    $image = get_sub_field("image");
                                    $size = 'full';
                                ?>
                                <div class="section-img"><?php echo wp_get_attachment_image( $image, $size ); ?></div>
                            <?php } ?>
                        </div>


                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

    //SECTION Five
    if (have_rows('section_five')):
        while (have_rows('section_five')) : the_row(); ?>

            <section class="section-scrollable section-5">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-10 col-lg-10 offset-lg-1 offset-md-1">
                            <h2 class="title"><?php echo get_sub_field('title'); ?></h2>
                            <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>
                            <img class="section-five-right" src="https://www.ucc-coffee.co.uk/wp-content/uploads/2021/09/section-five-right.jpg">
                            <img class="section-five-left" src="https://www.ucc-coffee.co.uk/wp-content/uploads/2021/09/section-5-left.jpg">
                            <br>
                            <br>
                            <?php
                            $rows_loop_name = "links";
                            $color_class = "black-btn red-text-hover";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>

                    <div class="col col-md-11 col-lg-11">
                            <?php if(get_sub_field("video_link") != "") { ?>
                            <?php
                                $link = "#video-modal-" . get_sub_field("video_link");
                                $scroll_down_class = "video-modal-link";
                            ?>
                                <div class='modal-video-container' id='video-modal-<?php echo get_sub_field("video_link"); ?>' data-video-id='<?php echo get_sub_field("video_link"); ?>'>
                                        <div class='background-overlay'></div>
                                        <div class="video-container">
                                            <div class='close-btn'><i class='fa fa-times'></i></div>
                                            <?php echo do_shortcode("[lyte id='" . get_sub_field("video_link") . "' /]"); ?>

                                        </div>
                                    </div>

                            <?php $image = get_sub_field("video_thumbnail"); ?>
                            <a href="<?php echo $link; ?>" class="video-image-link video-modal-link">
                                <img style="max-width:100%;" src="<?php echo $image['url']; ?>">
                            </a>
                            <!-- <div class="col-float">
                                <a href="<?php echo $link; ?>" aria-label="Play Video" class="mt-4 white-btn red-text-hover d-inline-block link-btn <?php if(isset($scroll_down_class)){ echo $scroll_down_class; }?>" <?php if(isset($target_blank) && $target_blank){ echo " target='_blank'"; }?>>
                                    <span class="link-text">Play Video</span>
                                </a>
                            </div> -->
                            <?php } ?>
                        </div>


                    </div>
                </div>
            </section>

        <?php endwhile;
    endif;

endwhile; ?>