<?php
/**
 * Home allows us to make a custom template without editing the index.php directly.
 *
 * This is the 'template' for the News (blog) listing. this is based on the WordPress
 * Template Hierarchy, as the correct file to use to define a custom blog posts index
 * is the home.php template file, and not a custom 'page' template.
 *
 * Without this file WordPress will default to the index.php file.
 */

$actual_link = "http://$_SERVER[HTTP_HOST]/";

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

$pagination = [];

$pagination['found_posts'] = $wp_query->found_posts;
$pagination['posts_per_page'] = get_option('posts_per_page');
$pagination['page_number'] = $paged;

$has_more = true;

if ($pagination['posts_per_page'] * $pagination['page_number'] >= $pagination['found_posts']) :
    $has_more = false;
endif; ?>


<section class="section-scrollable">
    <div class="container-fluid container-fluid-cap">
        <?php get_template_part('templates/partials/classic-hero-blog', 'classic-hero-blog');
        ?>
    </div>
</section>


<section id="news-posts" class="news-posts" data-label-navigation="Posts">
    <div class="container-fluid container-fluid-cap">
        <div class="row">

            <div class="col col-12">
                <?php if (!have_posts()) : ?>
                    <div class="alert alert-warning">
                        <?php _e('Sorry, no results were found.', 'sage'); ?>
                    </div>
                    <?php get_search_form(); ?>
                <?php endif; ?>
            </div>

            <div class="col col-12 results">
                <div class="posts-load">
                    <?php
                    $count = 1;

                    if(have_posts()):
                        while (have_posts()) : the_post();

                            // FIRST POSTS
                            if ($paged == 1 && $count == 1) {?>
                                <section class="section-scrollable first-post">
                                    <div class="container-fluid container-fluid-cap">
                                        <div class="row">
                                            <div class="image-col col col-12 col-md-5 col-xl-6 thick-pad-l">
                                                <?php
                                                //we retrieve the featured image link
                                                $thumb_id = get_post_thumbnail_id();
                                                $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
                                                $thumb_url = $thumb_url_array[0];
                                                //and we check if we have also a secondary image, that we will prefer to create the mirror quote
                                                $secondary_image = get_field('secondary_image');
                                                if(!empty($secondary_image)){
                                                    $image = $secondary_image;
                                                }else{
                                                    $image = $thumb_url;
                                                }
                                                $text_alignment = "left";
                                                $quote = get_field('quote');
                                                $vertical_alignment = "bottom";
                                                $box_color = "dark-lines";
                                                include('templates/partials/components/mirror-quote.php');
                                                ?>
                                            </div>
                                            <div class="content-col col col-12 col-md-7 col-xl-6">
                                                <p class="date right-pad-for-menu"><?php the_date("j F Y"); ?></p>
                                                <div class="line-container thick">
                                                    <span class="fill-line red-line"></span>
                                                </div>
                                                <h2 class="title right-pad-for-menu"><?php the_title(); ?></h2>
                                                <div class="subcontent right-pad-for-menu">
                                                    <?php if (get_field('primary_content')) :
                                                        echo get_field('primary_content');
                                                        if(get_field('secondary_content') || get_the_content()):
                                                            echo "<a href='" . get_post_permalink() . "' class='main-post-read-more link-btn dark-btn d-inline-block' aria-label='Read More'><span class='link-text'>Read more...</span></a>";
                                                        endif;
                                                    else:
                                                        if (get_field('secondary_content')) :
                                                            echo get_field('secondary_content');
                                                        else:
                                                            if (get_the_content()) :
                                                                the_content();
                                                            endif;
                                                        endif;
                                                    endif; ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section class="section-scrollable tweets">
                                    <div class="container-fluid container-fluid-cap">
                                        <div class="row">
                                            <div class="image-col col col-3 thick-pad-l">
                                                <img src="/wp-content/themes/cygnus_ucc/dist/images/twitter_bird-512.png" alt="Twitter Icon" class="img-fluid">
                                            </div>
                                            <div class="content-col col col-12 col-sm-9 common-pad-l right-pad-for-menu">
                                                <h2 class="title red-title">Latest From Twitter</h2>
                                                <?php echo do_shortcode('[custom-twitter-feeds datecustom="j M Y"]'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </section>

                                <?php
                                /*
                                 * We open the more news section here and we close it at the end of the loop.
                                 * This can be possible as the loop would never start if there was not the first post.
                                 * At the end of the first post (here) we open the section, then at the end of the loop,
                                 * indipendently from how many posts we have, we close it.
                                 */
                                ?>
                                    <section class="section-scrollable more-news">
                                        <div class="container-fluid container-fluid-cap">
                                            <div class="row">
                                                <div class="main-more-news-col col col-12 thick-pad right-pad-for-menu">
                                                    <h3 class="title">More News</h3>
                                                    <div class="line-container thick">
                                                        <span class="fill-line red-line"></span>
                                                    </div>
                                                    <div class="row posts-row align-items-end">
                            <?php }else{
                                echo '<div class="col col-12 col-md-6">';
                                get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format());
                                echo '</div>';
                            }

                            $count++;
                        endwhile;?>
                        <?php //we close here the section opened in the first post, that will host all the "more news" posts ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php endif; ?>

                    <?php if ($has_more) : ?>
                        <div class="post-wrapper col-xs-12 thick-pad right-pad-for-menu">
                            <div class="blog-load-more grey">
                                <?php
                                if (get_query_var('cat')) {
                                    $cat = get_category(get_query_var('cat'));
                                    $cat_slug = $cat->slug;
                                    echo '<a class="link-btn dark-btn" href="' . $actual_link . 'category/' . $cat->slug . '/page/' . ($pagination['page_number'] + 1) . '"><span class="link-text">View more</span></a>';
                                } else {
                                    echo get_next_posts_link('View more');
                                }
                                ?>
                                <div class="loading-spinner" style="display: none">
                                    <img src="/wp-includes/js/tinymce/skins/lightgray/img/loader.gif"/></div>
                            </div>
                        </div>
                    <?php endif; ?>

                </div>
            </div>

        </div>
    </div>
</section>

