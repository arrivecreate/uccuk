<?php
/**
 * Template Name: Coffee Works Template (v2)
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <div class="container-fluid container-fluid-cap">
        <?php get_template_part('templates/partials/classic-image-hero-coffee-works', 'classic-image-hero'); ?>
    </div>

    <?php
    //Loop the Group
    if (have_rows('first_section')):
        while (have_rows('first_section')) : the_row();

            if (!empty(get_sub_field('description')) || !empty(get_sub_field('image') || !empty(get_sub_field('video_id')))) :

                $overlapping = "";
                if ((get_sub_field('display_video_or_image') == "image" && !empty(get_sub_field('image')))
                    || (get_sub_field('display_video_or_image') == "video" && !empty(get_sub_field('video_id')))) {
                    $overlapping = "overlapping-element";
                }

                ?>
                <section class="section-scrollable gray-background-section top-gray video-section <?php echo $overlapping; ?>">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="image-video-col col-12 col-md-9 thick-pad-l">
                                <div class="inner-padding">
                                    <?php
                                    if (!empty(get_sub_field('description'))) {
                                        echo "<div class=\"prevideo-desc subcontent\">" . get_sub_field('description') . "</div>";
                                    }

                                    if (!empty(get_sub_field('video_call_to_action'))) {
                                    ?>
                                      <h3 class="title"><?php echo get_sub_field('video_call_to_action'); ?></h3>
                                    <?php
                                    }

                                    if (!empty(get_sub_field('display_video_or_image')) && get_sub_field('display_video_or_image') == "image") {
                                        if (!empty(get_sub_field('image'))) {
                                            echo "<div class=\"image-fallback\">" .
                                                "<div class=\"image-container\" style=\"background-image: url('" . get_image_url(get_sub_field('image')) . "')\"></div>" .
                                                "</div>";
                                        }
                                    } else {
                                        if (!empty(get_sub_field('video_id'))) { ?>
                                            <style>
                                                .video-section .vjs-youtube-mobile .vjs-poster {
                                                    background-image : url("https://img.youtube.com/vi/<?php echo get_sub_field('video_id'); ?>/maxresdefault.jpg");
                                                    display          : inline-block !important;
                                                    z-index          : 35;
                                                }
                                            </style>
                                            <div class="video-container">
                                                <video id="coffee-works-intro-video" class="video-js" controls preload="auto" width="640" height="264"
                                                       data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://www.youtube.com/watch?v=<?php echo get_sub_field('video_id'); ?>"}], "customControlsOnMobile": false }'>
                                                    <p class="vjs-no-js">
                                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                    </p>
                                                </video>
                                            </div>
                                        <?php }
                                    } ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            <?php endif; ?>

            <section class="section-scrollable gray-background-section top-gray">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="image-col col col-12 col-md-4 slide-in-img slide-from-bottom-img">
                            <?php if (!empty(get_sub_field('background_image_top'))) { ?>
                                <div class="image-container" style="background-image: url('<?php echo get_image_url(get_sub_field('background_image_top')) ?>');"></div>
                            <?php } ?>
                        </div>
                        <div class="content-col col col-12 col-md-8">
                            <h2 class="title section-title red-bg-text">
                                <span><?php echo get_sub_field('section_title'); ?></span>
                            </h2>
                            <div class="row">
                                <div class="col col-12 col-md-8 offset-md-1 col-xl-7 offset-xl-2">
                                    <?php
                                    while (have_rows('info_title')) : the_row();
                                        if (get_row_layout() == 'standard_sub_block'):
                                            echo "<h3 class='title'>" . get_sub_field('title') . "</h3>";
                                        elseif (get_row_layout() == 'number_sub_block'):
                                            $micro_title = get_sub_field('micro_title');
                                            $number = get_sub_field('number');
                                            $unit_of_measure = get_sub_field('number_unit');;
                                            $number_text = get_sub_field('number_text');
                                            include('templates/partials/components/numbers.php');
                                        endif;
                                    endwhile;

                                    if (get_sub_field("content_block_one_text_or_styled_number_list") == "styled_number_list") { ?>
                                        <div class="row">
                                            <?php
                                            $subcontent = get_sub_field('content_block_one_styled_number_list_paragraph');
                                            if (!empty($subcontent)):
                                                echo "<div class='styled-number-list-subcontent-col col col-12'><div class='subcontent'>" . $subcontent . "</div></div>";
                                            endif;
                                            ?>
                                        </div>

                                        <div class="row styled-number-list">
                                            <?php
                                            if (have_rows("content_block_one_styled_number_list")):
                                                while (have_rows("content_block_one_styled_number_list")) : the_row(); ?>
                                                    <div class="col col-6 col-sm-4 col-md-6 col-lg-4">
                                                        <div class="number-container">
                                                            <div class="number-image"
                                                                 style="background-image: url('<?php echo get_image_url(get_sub_field("bg_image")) ?>')"></div>
                                                            <div class="number-label"><?php echo get_sub_field("list_element_text"); ?></div>
                                                        </div>
                                                    </div>
                                                <?php endwhile;
                                            endif;
                                            ?>
                                        </div>
                                    <?php } else {
                                        $subcontent = get_sub_field('content_block_one');
                                        if (!empty($subcontent)):
                                            echo "<div class='subcontent'>" . $subcontent . "</div>";
                                        endif;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="section-scrollable step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>1</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Agree critical foundation brand standards</h2>
                    <div class="description subcontent">
                      <p>We work with you to create coffee standards for your business – <strong>the three golden rules</strong>. This forms the foundation of our partnership and coffee excellence across your business.</p>
                    </div>
                    <h3 class="subtitle">CoffeeWorks three golden rules</h3>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="row headed-boxes rules">
                  <div class="col col-12 col-sm-6 col-md-4 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>1</span></div>
                      <div class="box-content">Keep your coffee fresh</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>2</span></div>
                      <div class="box-content">Keep your milk cold and fresh</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 offset-sm-3 offset-md-0 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>3</span></div>
                      <div class="box-content">Keep your machine clean</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col col-12 col-md-5">
                    <p>Difficulties with coffee machines, coffee quality and issues with inconsistency are almost always linked to one of the three golden rules.</p>
                    <p>Watch our video on how these affect coffee availability and keep you serving exceptional coffee.</p>
                  </div>
                  <div class="col col-12 col-md-7">
                  <style>
                                                .step .vjs-youtube-mobile .vjs-poster {
                                                    background-image : url("https://img.youtube.com/vi/WVDkhj6T8kg/maxresdefault.jpg");
                                                    display          : inline-block !important;
                                                    z-index          : 35;
                                                }
                                            </style>
                                            <div class="video-container">
                                                <video id="coffee-works-golden-rules-video" class="video-js" controls preload="auto" width="640" height="264"
                                                       data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "https://www.youtube.com/watch?v=WVDkhj6T8kg&rel=0"}], "customControlsOnMobile": false }'>
                                                    <p class="vjs-no-js">
                                                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                    </p>
                                                </video>
                                            </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>2</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Manager critique</h2>
                    <div class="description subcontent">
                      <p>Coffee delivery is complex, but it doesn’t need to be. We make it accessible by splitting it into easy to understand areas.</p>
                      <p>With our Manager Critique we teach managers what to expect from their baristas, not how to make coffee. All with a commercial, efficiency and quality-based mindset. Making it easy to eliminate bad habits and improve consistency.</p>
                    </div>
                    <h3 class="subtitle">Areas covered include:</h3>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="row headed-boxes areas">
                  <div class="col col-12 col-sm-6 col-md-4 col-lg-5ths headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#1</span></div>
                      <div class="box-content">Site <br />set-up</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 col-lg-5ths headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#2</span></div>
                      <div class="box-content">Great espresso</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 col-lg-5ths headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#3</span></div>
                      <div class="box-content">Perfect milk</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 col-lg-5ths offset-md-2 offset-lg-0 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#4</span></div>
                      <div class="box-content">Routines and efficiencies</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-4 col-lg-5ths offset-sm-3 offset-md-0 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#5</span></div>
                      <div class="box-content">Cleaning your equipment</div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>3</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Training on-site &amp; off-site</h2>
                    <div class="description subcontent">
                      <p>We deliver a range of objective, practical courses around you either at your site, through e-learning, or at our Specialty Coffee Association (SCA) accredited COFFEEWORKS Training Suite, all designed with a commercial mindset.</p>
                      <div class="row">
                        <div class="col col-12">
                          <p>We lead the industry with our facilities – from the most technical and scientific to an ideal environment to roll the sleeves up and truly master the craft of a barista.</p>
                        </div>
                        <div class="col col-12">
                          <p><br/><img src="/wp-content/uploads/2020/01/sca-member-2019.png">&nbsp;&nbsp;&nbsp;<img src="/wp-content/uploads/2018/07/sca-premier.jpg"></p>
                        </div>
                      </div>
                    </div>
                    <h3 class="subtitle">Our SCA Accredited Premier Campus in Milton Keynes includes:</h3>
                    <div class="description subcontent">
                      <ul>
                        <li>Practical training area</li>
                        <li>Sensory, roasting and traditional equipment</li>
                        <li>Classroom and study area</li>
                        <li>Live video and Skype training</li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container img-gallery-container">
                <div class="img-box red-lines">
                  <div class="img-box-line top"></div>
                  <div class="img-box-line right"></div>
                  <div class="img-box-line bottom"></div>
                  <div class="img-box-line left"></div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="img-gallery row">
                      <div class="col col-8 col-lg-8"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step3-1.jpg');"></div></div>
                      <div class="col col-4 col-lg-4 d-flex flex-column">
                        <div class="flex-fill"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step3-2.jpg');"></div></div>
                        <div class="flex-fill"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step3-3.jpg');"></div></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>4</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Reinforcement tools</h2>
                    <div class="description subcontent">
                      <p>We recognise that not all baristas learn at the same pace or in the same way. So, we offer a unique suite of learning tools to suit everyone in your team.</p>
                      <p>We provide quick and easy access to resources including our face-to face training, cleaning and troubleshooting e-guides, and video series.</p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="container img-gallery-container">
                <div class="img-box red-lines">
                  <div class="img-box-line top"></div>
                  <div class="img-box-line right"></div>
                  <div class="img-box-line bottom"></div>
                  <div class="img-box-line left"></div>
                </div>
                <div class="row">
                  <div class="col">
                    <div class="img-gallery row">
                      <div class="col col-4 col-lg-2"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step4-1.jpg');"></div></div>
                      <div class="col col-8 col-lg-4 d-flex flex-column">
                        <div class="flex-fill"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step4-2.jpg');"></div></div>
                        <div class="flex-fill"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step4-3.jpg');"></div></div>
                      </div>
                      <div class="col col-12 col-lg-6"><div class="gallery-img" style="background-image: url('<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step4-4.jpg');"></div></div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>5</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">The coffee excellence check (CEC)</h2>
                    <div class="description subcontent">
                      <p>Our Coffee Excellence check is an efficient and effective means of capturing coffee performance at site on every visit. Using our COFFEEWORKS mobile app, our team capture information on the five key areas of coffee excellence, without disrupting your service.</p>
                      <p>This generates a true reflection of coffee performance and identifies areas for us to improve together.</p>
                    </div>
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step5-cec.jpg" alt="" class="img-fluid">
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>6</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Coffee profit calculator</h2>
                    <div class="description subcontent">
                      <p>Our Coffee Profit Calculator helps you to recognise your full commercial potential.</p>
                      <p>By tracking efficiencies, service areas, storage logics and the positives of a consistent method of coffee preparation across the team, you can increase profit from coffee.</p>
                      <p>Our calculator uses diagnostics to calculate the overall sales opportunity linked to workflow efficiency and speed of service. We work with you to implement these changes in your coffee service.</p>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step step-7">
              <div class="container-fluid container-fluid-cap">
                <div class="img-box red-lines">
                  <div class="img-box-line top"></div>
                  <div class="img-box-line right"></div>
                  <div class="img-box-line bottom"></div>
                  <div class="img-box-line left"></div>
                </div>
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>7</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-8 content-col">
                    <div class="row">
                      <div class="col col-12 col-sm-6 col-md-12 col-lg-4">
                        <h2 class="title">Spot training</h2>
                        <div class="description subcontent">
                          <p>Using data from our assessments and coffee excellence checks, we identify the key areas of impact and deliver short, sharp training on the spot.</p>
                          <p>Spot training complements our range of longer practical training, held in our Milton Keynes training suite.</p>
                        </div>
                      </div>
                      <div class="col col-12 col-sm-6 col-md-12 col-lg-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step7.jpg" alt="" class="img-fluid">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="step">
              <div class="container-fluid container-fluid-cap">
                <div class="row step-content">
                  <div class="col col-12 col-md-4 col-lg-3 title-col">
                    <h2 class="title section-title red-bg-text">
                      <span>8</span>
                    </h2>
                  </div>
                  <div class="col col-12 col-md-6 content-col">
                    <h2 class="title">Dashboard analysis</h2>
                    <img src="<?php echo get_template_directory_uri(); ?>/dist/images/coffee-works/step8-dashboard.gif" alt="" class="img-fluid">
                    <div class="description subcontent">
                      <p>We give you full visibility of coffee performance in your individual sites and across your entire estate. We strategically tailor our support and training logics to reflect your true needs for continuing to improve and grow successfully.</p>
                      <p>Our dashboard analysis gives you access to data collected by our Coffee Excellence Checks. We present this back to you and work to either maintain or improve together.</p>
                    </div>
                    <h3 class="subtitle">Analysis includes:</h3>
                  </div>
                </div>
              </div>
              <div class="container">
                <div class="row headed-boxes markers">
                  <div class="col col-12 col-sm-6 col-md-6 col-lg-3 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#1</span></div>
                      <div class="box-content">Month by month reporting</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-6 col-lg-3 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#2</span></div>
                      <div class="box-content">Top and bottom performance</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-6 col-lg-3 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#3</span></div>
                      <div class="box-content">Site and full estate&nbsp;data</div>
                    </div>
                  </div>
                  <div class="col col-12 col-sm-6 col-md-6 col-lg-3 headed-box-col">
                    <div class="border"></div>
                    <div class="headed-box">
                      <div class="box-title"><span>#4</span></div>
                      <div class="box-content">Link to service statistics for coffee availability</div>
                    </div>
                  </div>
                </div>
              </div>
            </section>

            <section class="works-intro gray-background-section top-gray">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12 col-md-7 title-col">
                            <?php if (get_sub_field('display_title_or_link') == "title" && !empty(get_sub_field('box_title'))) { ?>
                                <h2 class="title section-title red-bg-text">
                                    <span><?php echo get_sub_field('box_title'); ?></span>
                                </h2>
                            <?php } else {
                                if (have_rows('links')):
                                    while (have_rows('links')) : the_row();
                                        $link_type = get_sub_field('link_type');
                                        $link_text = get_sub_field('link_text');
                                        $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                        if ($link_type === "lt") {
                                            $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                            $scroll_down_class = "let-s-talk-link";
                                        } else {
                                            $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                            if ($link_type === "in") {
                                                if (get_post_type($link) === "attachment") {
                                                    $link = wp_get_attachment_url($link);
                                                    $target_blank = true;
                                                } else {
                                                    if (get_post_type($link) !== false) {
                                                        $link = get_the_permalink($link);
                                                    }
                                                }
                                            }
                                        }
                                        if (isset($link) && !empty($link) && !empty($link_text)) { ?>
                                            <a href="<?php echo $link; ?>" class="title section-title red-bg-text">
                                                <span><?php echo $link_text; ?></span>
                                            </a>
                                        <?php }
                                    endwhile;
                                endif;
                            } ?>
                        </div>
                        <div class="col col-12 col-md-7 subtitle-col">
                            <?php
                            $subtitle = get_sub_field('sub_title');
                            if (!empty($subtitle)) {
                                echo "<h3 class='subtitle'>" . $subtitle . "</h3>";
                            }
                            ?>
                        </div>
                        <div class="col col-12 col-md-8 subcontent-col">
                            <?php
                            $subcontent = get_sub_field('sub_content');
                            if (!empty($subcontent)) {
                                echo "<div class='subcontent'>" . $subcontent . "</div>";
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </section>

        <?php
        endwhile;
    endif; ?>


    <?php
    //Loop the repeater
    if (have_rows('main_content')):
        //we reset the index and the alignment
        $i = 0;
        $left_right_switch = "left";

        while (have_rows('main_content')) : the_row();
            $image_placement = get_sub_field('image_placement');

            $additional_classes = "";
            $additional_classes .= "section-aligned-" . $left_right_switch . " ";
            $additional_classes .= "image-" . $image_placement;

            if ($left_right_switch == "left") {
                $image_col_class = "col col-12 col-md-6 order-1 order-md-1";
                $content_col_class = "col col-12 col-md-6 order-2 order-md-2";
            } else {
                $image_col_class = "col col-12 col-md-5 order-1 order-md-2";
                $content_col_class = "col col-12 col-md-7 order-1 order-md-1";
            }
            ?>

            <section class="section-scrollable gray-background-section coffee-works-vertical-section <?php echo $additional_classes; ?>">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-12">
                            <div class="main-col-container">
                                <div class="row">
                                    <div class="image-col <?php echo $image_col_class; ?>">
                                        <div class="image-container" style="background-image: url('<?php echo get_image_url(get_sub_field('main_image')); ?>');"></div>
                                    </div>
                                    <div class="content-col <?php echo $content_col_class; ?>">
                                        <div class="content-inner-container">
                                            <?php
                                            $title = get_sub_field('title');
                                            if (!empty($title)):?>
                                                <h2 class="title"><?php echo $title; ?></h2>
                                            <?php endif; ?>

                                            <?php //Loop repeater
                                            if (have_rows('information')):
                                                echo "<div class='row no-gutters information-graphs'>";
                                                while (have_rows('information')) : the_row();

                                                    if (get_sub_field('percentage') == "yes") {
                                                        ?>
                                                        <div class='col statistic percentage-true'>
                                                            <div class="canvas-container">
                                                                <canvas class="graph" width="100" height="100"
                                                                        data-active-value="<?php echo get_sub_field('value') ?>"></canvas>
                                                            </div>

                                                            <?php
                                                            $micro_title = "";
                                                            $number = get_sub_field('value');
                                                            $unit_of_measure = "%";
                                                            $number_text = "";
                                                            include('templates/partials/components/numbers.php');
                                                            ?>
                                                            <div class="label-underneath"><?php echo get_sub_field('label'); ?></div>
                                                        </div>

                                                    <?php } else {
                                                        echo "<div class='col statistic percentage-false'>";
                                                        $micro_title = "";
                                                        $number = get_sub_field('value');
                                                        $unit_of_measure = "";
                                                        $number_text = "";
                                                        include('templates/partials/components/numbers.php');
                                                        echo "<div class='label-underneath'>" . get_sub_field('label') . "</div>";
                                                        echo "</div>";
                                                    }

                                                endwhile;
                                                echo "</div>";
                                            endif; ?>


                                            <?php
                                            if ($image_placement == "back") {
                                                $description = get_sub_field('description');
                                                if (!empty($description)) {
                                                    echo "<div class='description subcontent'>" . $description . "</div>";
                                                }

                                                // LINKS
                                                $rows_loop_name = "links";
                                                $color_class = "dark-btn";
                                                include('templates/partials/components/link-row-container.php');
                                            }
                                            ?>
                                        </div>
                                    </div>
                                </div>

                                <?php if ($image_placement == "fore") { ?>
                                    <div class="row image-fore-sibling-row">
                                        <div class="placeholder-col <?php echo $image_col_class ?>"></div>
                                        <div class="content-col <?php echo $content_col_class; ?>">
                                            <?php $description = get_sub_field('description');
                                            if (!empty($description)) {
                                                echo "<div class='description subcontent'>" . $description . "</div>";
                                            }

                                            // LINKS
                                            $rows_loop_name = "links";
                                            $color_class = "dark-btn";
                                            include('templates/partials/components/link-row-container.php'); ?>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
            //cycle related operations
            if ($left_right_switch == "left") {
                $left_right_switch = "right";
            } else {
                $left_right_switch = "left";
            }

            $i++;
        endwhile;
    endif; ?>

<section class="section-scrollable tv-series">
    <div class="container-fluid container-fluid-cap">
      <div class="row">
          <div class="col col-12 col-md-7 title-col">
              <h2 class="title section-title red-bg-text">
                  <span>CoffeeWorks TV Series</span>
              </h2>
              <div class="row">
                  <div class="col col-12 col-md-8 offset-md-2 subcontent-col">
                      <div class='description subcontent'>
                        <p>We have a range of videos available to help you deliver coffee excellence. Ranging from how to pick the right machine for your business to dispelling common coffee machine myths.</p>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="row video-row white-bg-row">
        <div class="video-col col col-12 col-sm-11 col-md-10 col-lg-9 offset-lg-1 col-xl-8 offset-xl-1">
          <div class="owl-carousel video-carousel">
            <div>
              <div class="video-container" data-src="gXOo82mzvow">
                <video class="video-js video-js-custom" controls preload="auto">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
              </div>
              <div class="video-title"><h3><span>Common coffee machine myths, dispelled</span></h3></div>
            </div>
            <div>
              <div class="video-container" data-src="7rItmPpo7CA">
                <video class="video-js video-js-custom" controls preload="auto">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
              </div>
              <div class="video-title"><h3><span>Coffee is about your customers, not the barista</span></h3></div>
            </div>
            <div>
              <div class="video-container" data-src="kRw0ASHLOcE">
                <video class="video-js video-js-custom" controls preload="auto">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
              </div>
              <div class="video-title"><h3><span>Choosing the right machine for your business</span></h3></div>
            </div>
            <div>
              <div class="video-container" data-src="ggCHRSGWRLw">
                <video class="video-js video-js-custom" controls preload="auto">
                    <p class="vjs-no-js">
                        To view this video please enable JavaScript, and consider upgrading to a web browser that
                        <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
              </div>
              <div class="video-title"><h3><span>How to spot a bad coffee</span></h3></div>
            </div>
          </div>
          <div class="video-nav-container"><span class="slider-counter">1</span></div>
        </div>
      </div>
    </div>
</section>

<?php endwhile; ?>
