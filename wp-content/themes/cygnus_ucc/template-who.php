<?php
/**
 * Template Name: Who We Are Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Two Column Vertical stuff here!
     */

    /* this banner defaults to the classic banner so we flag that we need the container */
    $outer_container_required = true;
    include('templates/partials/image-hero.php');
    get_template_part('templates/partials/two-column-vertical', 'two-column-vertical');

endwhile; ?>
