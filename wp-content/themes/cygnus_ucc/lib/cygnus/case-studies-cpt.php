<?php

/**
 * Case Studies Custom Post Type declaration.
 */


/**
 * Registers a new custom post type for the Case Studies.
 */
function create_post_type_case_studies()
{

    register_post_type('case_studies',
        array(
            'labels' => array(
                'name' => __('Case Studies'),
                'singular_name' => __('Case Study')
            ),
            'public' => true,
            'has_archive' => false,
            'menu-icon' => 'dashicons-admin-comments',
            'rewrite' => array('slug' => 'our-work')
        )
    );

}

add_action('init', 'create_post_type_case_studies');