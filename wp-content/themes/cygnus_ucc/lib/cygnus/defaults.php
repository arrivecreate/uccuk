<?php

/**
 * Registers the header menu for the website
 */
function register_header_menu() {
    register_nav_menu('header-menu',__( 'Header Menu' ));
}
add_action( 'init', 'register_header_menu' );


/**
 * Admin Options Page declarations
 */
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title' => 'Footer Settings',
        'menu_title' => 'Theme Settings',
        'menu_slug' => 'theme-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

}


/**
 * Cygnus Custom Scripts Enqueuing
 */
function cyg_assets()
{
    wp_enqueue_script('gsap', get_stylesheet_directory_uri() . '/bower_components/gsap/src/uncompressed/TweenMax.js', array('jquery'), '', true);
    wp_enqueue_script('gsapScrollPlugin', get_stylesheet_directory_uri() . '/bower_components/gsap/src/minified/plugins/ScrollToPlugin.min.js', array('gsap'), '', true);
    wp_enqueue_script('scrollMagic', get_stylesheet_directory_uri() . '/bower_components/scrollmagic/scrollmagic/uncompressed/ScrollMagic.js', array('jquery', 'gsap'), '', true);
    //wp_enqueue_script('scrollMagicAnim', get_stylesheet_directory_uri() . '/bower_components/scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js', array('gsap', 'scrollMagic'), '', true);
    //wp_enqueue_script('scrollMagicDebug', get_stylesheet_directory_uri() . '/bower_components/scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js', array('gsap', 'scrollMagic'), '', true);
    //wp_enqueue_script('odometer', get_stylesheet_directory_uri() . '/bower_components/odometer/odometer.js', array('jquery', 'gsap', 'scrollMagic'), '', true);
    wp_enqueue_style('videocss', get_stylesheet_directory_uri() . '/bower_components/videojs/dist/video-js.min.css', array(), '', "all");
    wp_enqueue_script('videojs', get_stylesheet_directory_uri() . '/bower_components/videojs/dist/video.min.js', array('jquery'), '', true);
    wp_enqueue_script('videojsYoutube', get_stylesheet_directory_uri() . '/bower_components/videojs-youtube/dist/Youtube.min.js', array('jquery', 'videojs'), '', true);
    

    //COFFEE WORKS
    //if (is_page_template('template-coffee-works.php') || is_page_template('single-equipment.php')) {
        //wp_enqueue_script('chartjs', get_stylesheet_directory_uri() . '/bower_components/chart.js/dist/Chart.min.js', array('jquery'), '', true);
    //}
}

 add_action( 'wp_enqueue_scripts', 'cyg_assets' );

/**
 * Declare WooCommerce Support
 */
function cygnus_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
add_action( 'after_setup_theme', 'cygnus_add_woocommerce_support' );



// Replaces the excerpt "Read More" text with nothing and adding it manually in the correspondant templates
function new_excerpt_no_more($more) {
    global $post;
    return '';
}
add_filter('excerpt_more', 'new_excerpt_no_more');


/**
 * * Get all the main taxonomies for Side Products
 *
 * @return array|int|WP_Error
 */
function get_custom_categories_list($taxonomy) {

    $terms = get_terms( array( 'taxonomy' => $taxonomy ) );
    $terms_list = array();

    foreach($terms as $term){
        $terms_list[] = $term->name;
    }

    return $terms_list;

}

/**
 * * Get the individual term for Side Products taxonomy
 *
 * @param $taxonomy
 * @return array|int|WP_Error
 */
function get_custom_categories_info($taxonomy, $category) {

    $terms = get_terms( array( 'taxonomy' => $taxonomy, 'name' => $category ) );

    return $terms;

}

/**
 * * Get all the main categories in a drop-down for selected taxonomy
 *
 * @param $taxonomy
 */
function get_custom_categories_select($taxonomy) {

    $terms = get_terms( $taxonomy );
    $select = '';

    foreach($terms as $term){

        if($select == ''){
            $select = '<select id="'.$term->taxonomy.'" name="'.$term->taxonomy.'" class="select-field">';
            $select .= '<option value="" selected>Select category</option>';
        }
        $select .= '<option value="'.$term->slug.'">'.$term->name.'</option>';

    }

    $select .= '</select>';

    echo $select;

    return;

}


/**
 * * Function to send an email from the form in the contact us page
 *
 * @param $data
 */
function sendEmail(){
    try{
        $data = array();
        parse_str($_POST['data'], $data);

        $cc = 'Cc: info@ucc-coffee.co.uk' . "\r\n";
        $sendto = $_POST['sendTo'];

        if(empty($sendto)){
            $sendto = "info@ucc-coffee.co.uk";
            $cc = "";
        }

        $to = $sendto;
        $subject = 'Contact Form Submission';
        $message = "";

        if(isset($data['your-name'])){
            $message .= "<p><strong>Name : </strong>" . $data['your-name'] . "</p>";
        }

        if(isset($data['contact-page-your-email'])){
            $message .= "<p><strong>Email : </strong>" . $data['contact-page-your-email'] . "</p>";
        }

        // if(isset($data['your-telephone'])){
        //     $message .= "<p><strong>Phone Number : </strong>" . $data['your-telephone'] . "</p>";
        // }

        if(isset($data['business-size'])){
            $message .= "<p><strong>Business Size : </strong>" . $data['business-size'] . "</p>";
        }

        if(isset($data['company'])){
            $message .= "<p><strong>Company : </strong>" . $data['company'] . "</p>";
        }

        if(isset($data['postcode'])){
            $message .= "<p><strong>Postcode : </strong>" . $data['postcode'] . "</p>";
        }

        if(isset($data['consent'])){
            $message .= "<p><strong>Consent Given : </strong> Yes</p>";
        }else{
            /* This should never happen, but we put it to flag it anyway */
            $message .= "<p><strong>Consent Given : -No-</strong></p>";
        }

        if(isset($data['sales-comm'])){
            $message .= "<p><strong>Sales Comm : </strong> Yes</p>";
        }else{
            $message .= "<p><strong>Sales Comm : </strong> No</p>";
        }

        if(isset($data['your-message'])){
            $message .= "<p><strong>Message : </strong>" . $data['your-message'] . "</p>";
        }

        $headers = 'From: info@ucc-coffee.co.uk' . "\r\n" .
            $cc .
            'Reply-To: info@ucc-coffee.co.uk' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        mail($to, $subject, $message, $headers);

        $result = 'Success';
    }catch(exception $e){

    }

    print_r(" - " . var_dump($data)) ;
    wp_die();
}

add_action("wp_ajax_send_email", "sendEmail");
add_action("wp_ajax_nopriv_send_email", "sendEmail");







add_filter('after_setup_theme', 'remove_redundant_shortlink');

function remove_redundant_shortlink() {
    // remove HTML meta tag
    // <link rel='shortlink' href='http://example.com/?p=25' />
    remove_action('wp_head', 'wp_shortlink_wp_head', 10);

    // remove HTTP header
    // Link: <https://example.com/?p=25>; rel=shortlink
    remove_action( 'template_redirect', 'wp_shortlink_header', 11);
}

/* additions - EF */

function get_image_url($id = false, $size = 'xlarge') {
    // debugging
    // return '//placehold.it/1600x1200';
    if( !$id ) {
        return;
    }

    if( is_string($id) ) {
        return $id;
    } else {
        $image = wp_get_attachment_image_src($id, $size);
        if( is_array($image) ) {
            return $image[0];
        }
    }

}




//function to add async attribute
function add_async_attribute($tag, $handle) {	
	$scripts_to_async = array('my-js-handle-async', 'another-handle-async');
	//check if this script is in the array	
	if (in_array($handle, $scripts_to_async)){
		//return with async
		return str_replace(' src', ' async="async" src', $tag);
	}else{
		//return without async
		return $tag;
	}
}

//function to add defer attribute
function add_defer_attribute($tag, $handle) {	
	$scripts_to_defer = array('gsap', 'gsapScrollPlugin', 'videojs', 'videojsYoutube', 'gtm4wp-form-move-tracker', 'revmin');
	//check if this script is in the array
	if (in_array($handle, $scripts_to_defer)){
		//return with defer
		return str_replace(' src', ' defer="defer" src', $tag);		
	}else{
		//return without async
		return $tag;
	}
}


function _changeme_defer_css( $html, $handle ) {

    $deferred_stylesheets = apply_filters( 'changeme_deferred_stylesheets', array() );

    if ( in_array( $handle, $deferred_stylesheets, true ) ) {
        return str_replace( 'media=\'all\'', 'media="print" onload="this.media=\'all\'"', $html );
    } else {
        return $html;
    }

}
add_filter( 'style_loader_tag', '_changeme_defer_css', 10, 2 );

add_filter( 'changeme_deferred_stylesheets', function( $handles ) {
    if ( ! is_admin() ) {
        $handles[] = 'dashicons';
        $handles[] = 'admin-bar';
        $handles[] = 'wp-block-library';
        $handles[] = 'contact-form-7';
        $handles[] = 'ctf_styles';
        $handles[] = 'videocss';
        $handles[] = 'rs-plugin-settings';
        $handles[] = 'cookie-law-info';
        $handles[] = 'cookie-law-info-gdpr';
        $handles[] = 'jetpack_css'; 
    }
    return $handles;
}, 10, 1 );

add_filter('script_loader_tag', 'add_async_attribute', 10, 2);
add_filter('script_loader_tag', 'add_defer_attribute', 10, 2);