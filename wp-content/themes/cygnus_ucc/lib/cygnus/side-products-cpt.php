<?php

/**
 * Side ProductsCustom Post Type declaration.
 */


/**
 * Registers a new custom post type for the Brands, including registering the required category.
 */
function create_post_type_side_products() {

    register_post_type( 'side_products',
        array(
            'labels' => array(
                'name' => __( 'Side Products' ),
                'singular_name' => __( 'Side Product' )
            ),
            'public' => true,
            'has_archive' => false,
            'menu-icon' => 'dashicons-image-filter',
            'rewrite' => array(
                'slug' => 'side-products'
            ),
        )
    );

    $labels = array(
        'name' => _x('Categories', 'taxonomy general name', 'textdomain'),
        'singular_name' => _x('Category', 'taxonomy singular name', 'textdomain'),
        'search_items' => __('Search Categories', 'textdomain'),
        'all_items' => __('All Categories', 'textdomain'),
        'parent_item' => __('Parent Categories', 'textdomain'),
        'parent_item_colon' => __('Parent Categories:', 'textdomain'),
        'edit_item' => __('Edit Categories', 'textdomain'),
        'update_item' => __('Update Categories', 'textdomain'),
        'add_new_item' => __('Add New Categories', 'textdomain'),
        'new_item_name' => __('New Categories Name', 'textdomain'),
        'menu_name' => __('Categories', 'textdomain'),
    );

    $args = array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array('slug' => 'side-products'),
    );

    register_taxonomy('side-products-category', array('side_products'), $args);

}
add_action( 'init', 'create_post_type_side_products' );
