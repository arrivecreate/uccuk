<?php

/**
 * Case Studies Custom Post Type declaration.
 */


/**
 * Registers a new custom post type for the Case Studies.
 */
function create_post_type_coffee()
{

    register_post_type('coffee',
        array(
            'labels' => array(
                'name' => __('Coffee'),
                'singular_name' => __('Coffee')
            ),
            'public' => true,
            'has_archive' => false,
            'menu-icon' => 'dashicons-admin-comments',
            'rewrite' => array('slug' => 'coffee')
        )
    );

}

add_action('init', 'create_post_type_coffee');