<?php

/**
 * This single is specific to the Coffee
 */

while (have_posts()) : the_post();

    //Loop through the main information group
    if (have_rows('main_information')):
        while (have_rows('main_information')) : the_row();

            /* We add here an style tag that will allow us to change the colour of the components if available*/
            if(!empty(get_sub_field("bg-colour"))) { $bg_colour = get_sub_field("bg-colour"); } else { $bg_colour = "#333d47"; }
            if(!empty(get_sub_field("titles-colour"))) { $titles_colour = get_sub_field("titles-colour"); } else { $titles_colour = "#9B9B9B"; }
            if(!empty(get_sub_field("text-colour"))) { $text_colour = get_sub_field("text-colour"); } else { $text_colour = "#FFFFFF"; }
            if(!empty(get_sub_field("component-colour"))) { $component_colour = get_sub_field("component-colour"); } else { $component_colour = "#9B9B9B"; }
            ?>

            <style>
                /* Common text colour and common background*/
                .single-coffee section.parent-sliding-section,
                .single-coffee section.product-type-section {
                    background-color : <?php echo $bg_colour; ?>;
                    color            : <?php echo $text_colour; ?>;
                }

                /* Banner title and first section title colour */
                .custom-cpt-hero .hero-classic-content .title,
                .single-coffee .first-coffee-case-section .content-col .title {
                    color : <?php echo $titles_colour; ?>;
                }

                /* Component colours */
                .custom-cpt-hero .line-container .fill-line,
                .first-coffee-case-section .mirror-box .mirror-box-line {
                    background-color : <?php echo $component_colour; ?>;
                }

                .single-coffee .first-coffee-case-section .image-col .mirror-quote-container .quote {
                    color : <?php echo $component_colour; ?>;
                }

                /* Hover change of text color on hover */
                .parent-sliding-section .link-btn:hover,
                .product-type-section .link-btn:hover {
                    color : <?php echo $bg_colour; ?>;
                }
            </style>

            <?php
            //Loop through the hero section
            if (have_rows('hero_section')):
                while (have_rows('hero_section')) : the_row(); ?>

                    <div class="custom-cpt-hero">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <div class="image-col col col-12 col-md-3 order-1 order-md-3 common-pad-r">
                                    <div class="hero-classic-img" style="background-image: url('<?php echo get_image_url(get_sub_field('side_image')); ?>')"></div>
                                </div>

                                <div class="current-page-container">
                                    <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                                </div>

                                <div class="container-fluid container-fluid-cap">
                                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg"
                                             alt="<?php bloginfo('name'); ?>">
                                    </a>
                                </div>

                                <div class="line-col col col-12 col-md-3 order-2 order-md-1">
                                    <div class="line-container vertical-line hero-line-container thick">
                                        <div class="fill-line dark-line"></div>
                                    </div>
                                </div>

                                <div class="content-col col col-12 col-md-8 col-lg-6 order-3 order-md-2 align-self-center">
                                    <div class="inner-padding">
                                        <div class="hero-classic-content">
                                            <?php if (!empty(get_sub_field("logo"))) { ?>
                                                <img src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="<?php echo get_sub_field('title'); ?> Logo"
                                                     class="coffee-logo img-fluid">
                                            <?php } ?>
                                            <h1 class="title"><?php echo get_sub_field('title'); ?></h1>
                                            <div class="line-container thick">
                                                <div class="fill-line dark-line"></div>
                                            </div>
                                            <div class="subcontent subtitle"><?php echo get_sub_field('sub_content'); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endwhile;
endif;

//Loop through the first section
if (have_rows('section_one')):
    while (have_rows('section_one')) : the_row(); ?>

        <section class="section-scrollable first-coffee-case-section light-background">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col col col-12 col-md-5 order-1 order-md-1 thick-pad-l">
                        <?php
                        $text_alignment = "left";
                        $image = get_image_url(get_sub_field('image'));
                        $quote = get_sub_field('quote');
                        $vertical_alignment = "bottom";
                        $data_offset = "-170";
                        include('templates/partials/components/mirror-quote.php');
                        ?>
                    </div>
                    <div class="content-col col col-12 col-md-7 order-1 order-md-2 thick-pad-l right-pad-for-menu">

                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                        <?php // LINKS
                        $rows_loop_name = "links";
                        $color_class = "dark-btn";
                        include('templates/partials/components/link-row-container.php');
                        ?>

                        <div class="secondary-image slide-in-img slide-from-bottom-img" style="background-image:url('<?php echo get_image_url(get_sub_field("secondary_image")); ?>');"></div>
                    </div>

                </div>
            </div>
        </section>

    <?php endwhile;
endif;

//we find out if a quote is available for the last rail section, if not, we are going to assign
//the last rail class to the last element in the product list
$third_section_content = "";
if (have_rows('section_three')):
    while (have_rows('section_three')) : the_row();
        $third_section_content = get_sub_field('content');
    endwhile;
endif;

//Loop through the sub sections
if (have_rows('section_two')):
    while (have_rows('section_two')) : the_row();

        $main_image = get_image_url(get_sub_field('main_image'));
        if (is_array($main_image)) { //$main_image might be an array or an image, we check for that
            $main_image = $main_image['url'];
        }

        // Loop the Parent Section Repeater
        if (have_rows('parent_section')):
            while (have_rows('parent_section')) : the_row(); ?>

                <?php
                $parent_image = get_image_url(get_sub_field('background_image'));
                if (is_array($parent_image)) { //$parent_image might be an array or an image, we check for that
                    $parent_image = $parent_image['url'];
                }
                ?>

                <section class="section-scrollable parent-sliding-section">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="image-col col col-12 col-md-6">
                                <div class="inner-padding">
                                    <div id="parent-bg-image-container" class="background-image-container"
                                         style="background-image: url('<?php echo $parent_image; ?>')"></div>
                                    <div id="parent-image-container" class="image-container" style="background-image: url('<?php echo $main_image; ?>')"></div>
                                    <div id="sliding-coffee-element"></div>
                                </div>
                            </div>
                            <div class="content-col col col-12 col-md-6 right-pad-for-menu">
                                <img src="<?php echo get_image_url(get_sub_field('logo')); ?>" alt="Logo" class="coffee-logo img-fluid">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php if(get_sub_field('secondary_title')){ ?><h3 class="title secondary-title"><?php echo get_sub_field('secondary_title'); ?></h3><?php } ?>
                                <?php if(get_sub_field('secondary_sub_content')){?><div class="subcontent secondary-subcontent"><?php echo get_sub_field('secondary_sub_content'); ?></div><?php } ?>

                                <?php
                                $rows_loop_name = "links";
                                $color_class = "white-btn";
                                include('templates/partials/components/link-row-container.php');
                                ?>
                            </div>
                        </div>
                    </div>
                </section>

                <?php
            endwhile;
        endif;


        $last_rail = ($third_section_content) ? "" : "last-rail-section";
        $product_list_count = 0;
        if (have_rows('product_list')):
            while (have_rows('product_list')) : the_row(); $product_list_count++; endwhile;
        endif;

        // Loop the Product List Repeater
        if (have_rows('product_list')): $i = 0;
            while (have_rows('product_list')) : the_row(); ?>

                <section class="section-scrollable product-type-section <?php echo $i; ?> <?php echo ($i == $product_list_count - 1) ? $last_rail : ""; ?>">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="image-col col col-12 col-md-6">
                                <div class="inner-padding">
                                    <div class="bg-image" style="background-image: url('<?php echo get_image_url(get_sub_field('overlay_image')); ?>');">
                                        <?php
                                        $line_bg_color = get_sub_field("colour");
                                        if (!empty($line_bg_color)) {
                                            echo "<style>.coffee-type-line .fill-line[data-colour='" . $line_bg_color . "']{ background-color: #" . $line_bg_color . " }</style>"
                                            ?>
                                            <div class="line-container coffee-type-line">
                                                <div class="fill-line" data-colour="<?php echo $line_bg_color; ?>"></div>
                                            </div>
                                            <?php
                                        } ?>
                                    </div>
                                    <div class="image-container"
                                         style="background-image: url('<?php echo get_image_url(get_sub_field("product_image")); ?>')"></div>
                                </div>
                            </div>
                            <div class="content-col col col-12 col-md-6 right-pad-for-menu">
                                <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                <h4 class="subtitle"><?php echo get_sub_field('sub_title'); ?></h4>
                                <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                <?php
                                if (have_rows('info')):?>
                                    <div class="coffee-details-table">
                                        <div class="table">
                                            <?php while (have_rows('info')) : the_row(); ?>
                                                <div class="trow">
                                                    <div class="td td-label">
                                                        <?php echo get_sub_field('column_one'); ?>
                                                    </div>
                                                    <div class="td td-details">
                                                        <?php echo get_sub_field('column_two'); ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </section>

                <?php
            $i++;
            endwhile;
        endif;

    endwhile;
endif;

if (have_rows('section_three')):
    while (have_rows('section_three')) : the_row(); ?>

        <?php if(get_sub_field('content')): ?>
        <section class="section-scrollable last-rail-section light-background">
            <div class="container-fluid container-fluid-cap">
                <div class="row">
                    <div class="image-col col col-12 col-md-6">
                        <div class="inner-padding">
                            <div class="image-container" style="background-image: url('<?php echo get_image_url(get_sub_field("image")); ?>')"></div>
                        </div>
                    </div>
                    <div class="content-col col col-12 col-md-5 offset-md-1 right-pad-for-menu align-self-center">

                        <div class="subcontent">
                            <div class="quote-content">
                                <span class="quote quote-open fa fa-quote-left"></span>
                                <?php echo get_sub_field('content'); ?>
                                <span class="quote quote-close fa fa-quote-right"></span>
                            </div>
                            <p class="name"><?php echo get_sub_field('name'); ?></p>
                            <p class="job-role"><?php echo get_sub_field('job_title'); ?></p>
                            <?php
                            $rows_loop_name = "links";
                            $color_class = "dark-btn";
                            include('templates/partials/components/link-row-container.php');
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
            endif;
    endwhile;
endif;

endwhile;
endif;

endwhile;