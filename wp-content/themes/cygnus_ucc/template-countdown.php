<?php
/**
 * Template Name: Countdown Landing Template
 */
?>

<!-- <script type="text/javascript" src="https://raw.githubusercontent.com/mckamey/countdownjs/master/countdown.min.js"></script> -->

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.19.1/TweenMax.min.js"></script>
<script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/MorphSVGPlugin.min.js"></script>
<script type="text/javascript" src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/16327/DrawSVGPlugin.min.js"></script>

<script type="text/javascript">
    jQuery(document).ready(function($){
        $.fn.changeElementType = function(newType) {
            var attrs = {};

            $.each(this[0].attributes, function(idx, attr) {
                attrs[attr.nodeName] = attr.nodeValue;
            });

            this.replaceWith(function() {
                return $("<" + newType + "/>", attrs).append($(this).contents());
            });
        }

        // Moving elements around so material design can be applied to the form
        $(".material-theme .wpcf7-form-control-wrap").each(function (i) {
            $(this).parent().find(".bar").insertAfter($(this).find("input"));
            $(this).parent().find("label").insertAfter($(this).find("input"));
        });

        $(".material-theme .wpcf7-checkbox .wpcf7-list-item").each(function (i) {
            $(this).find("input").after("<i class='helper'></i>");
        });

        $(".material-theme .wpcf7-checkbox .wpcf7-list-item").changeElementType("label");

        // Setting all form fields as required
        $(".material-theme .wpcf7-form-control").attr("required", "required");

        // Used to aid with applying material design to the form
        function checkForInput(element) {
            if ($(element).val().length > 0) {
                $(element).addClass("has-value");
            } else {
                $(element).removeClass("has-value");
            }
        }

        $(".material-theme .wpcf7-form-control").on("change keyup", function() {
            checkForInput(this);
        });

        // Set the date we're counting down to
        var countDownDate = new Date("Mar 28, 2019 10:00:00");
        var deadline = countDownDate;
        var preDeadline = new Date("Mar 28, 2019 09:59:59");
        var proDeadline = new Date("Mar 28, 2019 10:00:01");

        // var countDownDate = new Date("Jan 18, 2019 23:02:00");
        // var deadline = countDownDate;
        // var preDeadline = new Date("Jan 18, 2019 23:01:59");
        // var proDeadline = new Date("Jan 18, 2019 23:02:01");

        function updateTimer(deadline) {
            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var time = deadline.getTime() - now;

            // Time calculations for days, hours, minutes and seconds
            /// days
            var days = Math.floor(time/(1000*60*60*24));
            var formattedDays = ("0" + days).slice(-2);
            var stringDays = formattedDays.toString();
            var arrayDays = stringDays.split("");
            //// hours
            var hours = Math.floor((time/(1000*60*60)) % 24);
            var formattedHours = ("0" + hours).slice(-2);
            var stringHours = formattedHours.toString();
            var arrayHours = stringHours.split("");
            //// minutes
            var minutes = Math.floor((time/1000/60) % 60);
            var formattedMinutes = ("0" + minutes).slice(-2);
            var stringMinutes = formattedMinutes.toString();
            var arrayMinutes = stringMinutes.split("");
            //// seconds
            var seconds = Math.floor((time/1000) % 60);
            var formattedSeconds = ("0" + seconds).slice(-2);
            var stringSeconds = formattedSeconds.toString();
            var arraySeconds = stringSeconds.split("");

            return {
                'days' : days,
                'formattedDays' : formattedDays,
                'stringDays' : stringDays,
                'arrayDays' : arrayDays,
                'hours' : hours,
                'formattedHours' : formattedHours,
                'stringHours' : stringHours,
                'arrayHours' : arrayHours,
                'minutes' : minutes,
                'formattedMinutes' : formattedMinutes,
                'stringMinutes' : stringMinutes,
                'arrayMinutes' : arrayMinutes,
                'seconds' : seconds,
                'formattedSeconds' : formattedSeconds,
                'stringSeconds' : stringSeconds,
                'arraySeconds' : arraySeconds,
                'total' : time
            }
        }

        function updateDigits(unit, timerArray, prevDigitTimerArray, nextDigitTimerArray) {
            for (var i = 0, len = timerArray.length; i < len; i++) {
                if(timerArray[i] !== prevDigitTimerArray[i]) {
                    $("#" + unit + "-" + (i+1) + " .draw").css("display", "").removeClass("draw");
                    $("#" + unit + "-" + (i+1) + " .d-" + timerArray[i]).css("display", "inline-block").addClass("draw");
                }
            }
        }

        function setClock() {
            // Get todays date and time
            var now = new Date().getTime();

            // Find the distance between now and the count down date
            var distance = countDownDate.getTime() - now;

            var timer = updateTimer(deadline);
            var prevDigitTimer = updateTimer(proDeadline);
            var nextDigitTimer = updateTimer(preDeadline);

            // colons
            $(".colon").animate({opacity: 1}, 350);

            if (timer.total > 0) {
                // days
                for (var i = 0, len = nextDigitTimer.arrayDays.length; i < len; i++) {
                    $("#days-" + (i+1) + " .d-" + nextDigitTimer.arrayDays[i]).css("display", "inline-block").addClass("draw");
                }

                // hours
                for (var i = 0, len = nextDigitTimer.arrayHours.length; i < len; i++) {
                    $("#hours-" + (i+1) + " .d-" + nextDigitTimer.arrayHours[i]).css("display", "inline-block").addClass("draw");
                }

                // minutes
                for (var i = 0, len = nextDigitTimer.arrayMinutes.length; i < len; i++) {
                    $("#mins-" + (i+1) + " .d-" + nextDigitTimer.arrayMinutes[i]).css("display", "inline-block").addClass("draw");
                }

                // seconds
                for (var i = 0, len = nextDigitTimer.arraySeconds.length; i < len; i++) {
                    $("#secs-" + (i+1) + " .d-" + nextDigitTimer.arraySeconds[i]).css("display", "inline-block").addClass("draw");
                }

                var timerInterval = setInterval(function() {
                    timer = updateTimer(deadline);
                    prevDigitTimer = updateTimer(proDeadline);
                    nextDigitTimer = updateTimer(preDeadline);

                    // Display the result in the element with id="demo"
                    // document.getElementById("demo").innerHTML = timer.formattedDays + "d " + timer.formattedHours + "h " + timer.formattedMinutes + "m " + timer.formattedSeconds + "s ";

                    // console.log("Days: " + timer.days + " | Hours: " + timer.hours + " | Minutes: " + timer.minutes + " | Seconds: " + timer.seconds);

                    // animations
                    //// seconds
                    updateDigits("secs", timer.arraySeconds, prevDigitTimer.arraySeconds, nextDigitTimer.arraySeconds);
                    //// minutes
                    updateDigits("mins", timer.arrayMinutes, prevDigitTimer.arrayMinutes, nextDigitTimer.arrayMinutes);
                    //// hours
                    updateDigits("hours", timer.arrayHours, prevDigitTimer.arrayHours, nextDigitTimer.arrayHours);
                    //// days
                    updateDigits("days", timer.arrayDays, prevDigitTimer.arrayDays, nextDigitTimer.arrayDays);
                    //// colons
                    $(".colon").animate({opacity: 1}, 350).delay(300).animate({opacity: 0}, 350);

                    if(timer.arrayDays[0] === "0" && timer.arrayDays[1] === "0") {
                        $("#days").addClass("zero");

                        if(timer.arrayHours[0] === "0" && timer.arrayHours[1] === "0") {
                            $("#hours").addClass("zero");

                            if(timer.arrayMinutes[0] === "0" && timer.arrayMinutes[1] === "0") {
                                $("#mins").addClass("zero");

                                if(timer.arraySeconds[0] === "0" && timer.arraySeconds[1] === "0") {
                                    $("#secs").addClass("zero");
                                }
                            }
                        }
                    }

                    // check if end of timer
                    if(nextDigitTimer.total < 1){
                        clearInterval(timerInterval);
                        $(".colon").animate({opacity: 1}, 350);
                        $(".unit").addClass("zero");
                        // document.getElementById("demo").innerHTML = "EXPIRED";
                    }
                }, 1000);
            } else {
                $(".d-0").css("display", "inline-block").addClass("draw");
                $(".unit").addClass("zero");
            }
        }

        // setClock();
        setTimeout(function() { setClock(); }, 1000);
    });
</script>

<!-- <video playsinline autoplay loop muted id="bgVideo" class="video-js" preload="auto" data-setup='{ "techOrder": ["html5"] }'>
  <source type="video/mp4" src="/wp-content/themes/cygnus_ucc/dist/videos/countdown/background.mp4">
</video> -->

<video playsinline autoplay loop muted id="bgVideo">
  <source type="video/mp4" src="/wp-content/themes/cygnus_ucc/dist/videos/countdown/background.mp4">
</video>

<section id="countdown-clock">
    <p id="demo"></p>
    <div><img src="/wp-content/themes/cygnus_ucc/dist/images/ucc-logo-white.svg" alt="" class="logo"></div>
    <div id="countdown" class="clock">
        <div id="days" class="unit">
            <div class="unit-container">
                <div class="digits">
                    <ul id="days-1" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                        <li class="d-6"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/6.svg"); ?></li>
                        <li class="d-7"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/7.svg"); ?></li>
                        <li class="d-8"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/8.svg"); ?></li>
                        <li class="d-9"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/9.svg"); ?></li>
                    </ul>
                    <ul id="days-2" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                        <li class="d-6"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/6.svg"); ?></li>
                        <li class="d-7"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/7.svg"); ?></li>
                        <li class="d-8"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/8.svg"); ?></li>
                        <li class="d-9"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/9.svg"); ?></li>
                    </ul>
                </div>
                <div class="label">
                    Days
                </div>
            </div>
        </div>
        <div class="colon"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/colon.svg"); ?></div>
        <div id="hours" class="unit">
            <div class="unit-container">
                <div class="digits">
                    <ul id="hours-1" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                    </ul>
                    <ul id="hours-2" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                        <li class="d-6"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/6.svg"); ?></li>
                        <li class="d-7"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/7.svg"); ?></li>
                        <li class="d-8"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/8.svg"); ?></li>
                        <li class="d-9"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/9.svg"); ?></li>
                    </ul>
                </div>
                <div class="label">
                    Hours
                </div>
            </div>
        </div>
        <div class="colon"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/colon.svg"); ?></div>
        <div id="mins" class="unit">
            <div class="unit-container">
                <div class="digits">
                    <ul id="mins-1" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                    </ul>
                    <ul id="mins-2" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                        <li class="d-6"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/6.svg"); ?></li>
                        <li class="d-7"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/7.svg"); ?></li>
                        <li class="d-8"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/8.svg"); ?></li>
                        <li class="d-9"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/9.svg"); ?></li>
                    </ul>
                </div>
                <div class="label">
                    Mins
                </div>
            </div>
        </div>
        <div class="colon"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/colon.svg"); ?></div>
        <div id="secs" class="unit">
            <div class="unit-container">
                <div class="digits">
                    <ul id="secs-1" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                    </ul>
                    <ul id="secs-2" class="digit">
                        <li class="d-4 placeholder"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-0"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/0.svg"); ?></li>
                        <li class="d-1"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/1.svg"); ?></li>
                        <li class="d-2"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/2.svg"); ?></li>
                        <li class="d-3"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/3.svg"); ?></li>
                        <li class="d-4"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/4.svg"); ?></li>
                        <li class="d-5"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/5.svg"); ?></li>
                        <li class="d-6"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/6.svg"); ?></li>
                        <li class="d-7"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/7.svg"); ?></li>
                        <li class="d-8"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/8.svg"); ?></li>
                        <li class="d-9"><?php echo file_get_contents(__DIR__ . "/dist/images/countdown/9.svg"); ?></li>
                    </ul>
                </div>
                <div class="label">
                    Secs
                </div>
            </div>
        </div>
    </div>
</section>
<section id="countdown-heading-video">
    <?php echo file_get_contents(__DIR__ . "/dist/images/countdown/the-future-of-automation-is-coming.svg"); ?>
    <div class="modal-video-container" id="video-modal-tCQLKTKepmQ" data-video-id="tCQLKTKepmQ">
        <div class="background-overlay"></div>
        <div class="video-container">
            <div class="close-btn"><i class="fa fa-times"></i></div>
            <video id="tCQLKTKepmQ" class="video-js" controls preload="auto" width="640" height="264"
                    data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=tCQLKTKepmQ"}] }'>
                <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                </p>
            </video>
        </div>
    </div>

    <a href="#video-modal-tCQLKTKepmQ" aria-label="Play" class="video-modal-link">
        <?php echo file_get_contents(__DIR__ . "/dist/images/countdown/play-icon.svg"); ?>
    </a>
</section>
<section id="countdown-form">
    <h2>Launching March&nbsp;2019 at London Coffee Festival</h2>
    <h2>Register your interest and receive access to exclusive previews</h2>

    <?php echo do_shortcode('[contact-form-7 id="4757" title="Event Contact Form"]'); ?>
</section>
<footer class="footer-event">
    <div class="copyright">&copy;2019 UCC Coffee UK Ltd</div>
    <div class="legal-links">
        <a href="https://www.ucc-coffee.co.uk/privacy/" target="_blank">Privacy Policy</a>
        <a href="https://www.ucc-coffee.co.uk/legal-docs/" target="_blank">Terms of Use</a>
    </div>
</footer>