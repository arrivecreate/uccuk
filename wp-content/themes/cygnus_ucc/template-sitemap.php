<?php
/**
 * Template Name: Sitemap Template
 */
?>
<?php while (have_posts()) : the_post();

    /**
     * Call the Hero and Sub Hero
     */
    ?>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>


    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <div class="col col-12 thick-pad">
                    <?php wp_list_pages();?>
                </div>
            </div>
        </div>
    </section>

<?php
endwhile; ?>
