<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/page', 'header'); ?>
    <?php get_template_part('templates/content', 'page'); ?>

    <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
    <?php get_template_part('templates/partials/classic-image-hero', 'classic-image-hero'); ?>
    <?php get_template_part('templates/partials/image-hero', 'image-hero'); ?>
<?php endwhile; ?>

