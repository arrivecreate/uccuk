<?php

/**
 * This single is specific to the Equipment CPT
 */

while (have_posts()) : the_post();

//Loop through the main information group
    if (have_rows('main_information')):
        while (have_rows('main_information')) : the_row();

            //Loop through the hero section
            if (have_rows('hero_section')):
                while (have_rows('hero_section')) : the_row(); ?>

                    <div class="custom-cpt-hero">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <div class="image-col col col-12 col-md-3 order-1 order-md-3 common-pad-r">
                                    <div class="hero-classic-img" style="background-image: url('<?php echo get_image_url(get_sub_field('side_image')); ?>')"></div>
                                </div>

                                <div class="container-fluid container-fluid-cap">
                                    <a class="brand" href="<?= esc_url(home_url('/')); ?>">
                                        <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg"
                                             alt="<?php bloginfo('name'); ?>">
                                    </a>

                                    <div class="current-page-container">
                                        <span class="page-breadcrumb"><?php echo single_post_title(); ?></span>
                                    </div>
                                </div>

                                <div class="line-col col col-12 col-md-3 order-2 order-md-1">
                                    <div class="line-container vertical-line hero-line-container thick">
                                        <div class="fill-line dark-line"></div>
                                    </div>
                                </div>

                                <div class="content-col col col-12 col-md-8 col-lg-6 order-3 order-md-2 align-self-center">
                                    <div class="inner-padding">
                                        <div class="hero-classic-content">
                                            <?php if (get_image_url(get_sub_field("logo"))) { ?>
                                                <img src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="<?php echo get_sub_field('title'); ?> Logo"
                                                     class="equipment-logo img-fluid">
                                            <?php } ?>
                                            <h1 class="title"><?php echo get_sub_field('title'); ?></h1>
                                            <div class="line-container thick">
                                                <div class="fill-line dark-line"></div>
                                            </div>
                                            <div class="subcontent subtitle"><?php echo get_sub_field('sub_content'); ?></div>

                                            <div class="col-float mt-3" style="width:98.6%;">
                                                <a href="#" id="enquire-now" data-post-title="<?php echo get_sub_field('title'); ?>" class="link-btn red-btn d-inline-block">
                                                    <span class="link-text">Enquire Now</span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php endwhile;
            endif;

            //Loop through the first section
            if (have_rows('section_one')):
                while (have_rows('section_one')) : the_row(); ?>

                    <section class="section-scrollable first-equipment-case-section">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <div class="image-col col col-12 col-sm-4 col-md-5 order-1 order-md-1 common-pad-l">
                                    <div class="image-container" style="background-image: url('<?php echo get_image_url(get_sub_field('image')); ?>')"></div>
                                </div>
                                <div class="content-col col col-12 col-sm-8 col-md-7 order-1 order-md-2 common-pad-l right-pad-for-menu align-self-center">
                                    <h3 class="title"><?php echo get_sub_field('top_title'); ?></h3>
                                    <div class="line-container thick">
                                        <span class="fill-line red-line"></span>
                                    </div>
                                    <h4 class="subtitle"><?php echo get_sub_field('main_title'); ?></h4>
                                    <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                    <?php
                                    if (have_rows('additional_titles')): ?>
                                        <div class="row">
                                            <?php while (have_rows('additional_titles')) : the_row(); ?>
                                                <div class="col col-12">
                                                    <h3 class="title additional-title"><?php echo get_sub_field("title") ?></h3>
                                                    <div class="subcontent"><?php echo get_sub_field("subcontent") ?></div>

                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    <?php endif; ?>

                                    <?php // LINKS
                                    $rows_loop_name = "links";
                                    $color_class = "dark-btn";
                                    include('templates/partials/components/link-row-container.php');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>

                <?php endwhile;
            endif;


            //Loop through the sub sections
            if (have_rows('section_two')):
                while (have_rows('section_two')) : the_row();

                    $main_image = get_image_url(get_sub_field('image'));
                    if (is_array($main_image)) { //$main_image might be an array or an image, we check for that
                        $main_image = $main_image['url'];
                    } ?>

                    <?php if (!empty(get_sub_field("title")) || !empty(get_sub_field("image"))): ?>
                        <section class="section-scrollable section-two">
                            <div class="container-fluid container-fluid-cap">
                                <div class="row">
                                    <div class="image-col col col-12 col-md-5 offset-md-1 order-1 order-md-2">
                                        <div id="parent-image-container" class="image-container"
                                             style="background-image: url('<?php echo $main_image; ?>')"></div>
                                        <?php
                                        $text_alignment = "left";
                                        $image = get_image_url(get_sub_field('image'));
                                        $quote = "";
                                        $vertical_alignment = "bottom";
                                        $box_color = "silver-lines";
                                        include('templates/partials/components/mirror-quote.php');
                                        ?>
                                    </div>
                                    <div class="content-col col col-12 col-md-6 thick-pad order-2 order-md-1">
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                        <div class="subcontent"><?php echo get_sub_field('content'); ?></div>
                                        <h3 class="title"><?php echo get_sub_field('secondary_title'); ?></h3>
                                        <div class="subcontent"><?php echo get_sub_field('secondary_content'); ?></div>

                                        <?php // LINKS
                                        $rows_loop_name = "links";
                                        $color_class = "dark-btn";
                                        include('templates/partials/components/link-row-container.php');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php
                    endif;
                endwhile;
            endif;

            //Loop through the first section
            if (have_rows('section_three')):
                while (have_rows('section_three')) : the_row(); ?>

                    <?php
                    $image_positioning = get_sub_field("image_positioning");
                    if (isset($image_positioning) && !empty($image_positioning) && $image_positioning === "contained") {
                        $image_positioning = "contained";
                    } else {
                        $image_positioning = "exceeding";
                    }
                    ?>

                    <?php if (!empty(get_sub_field("title")) || !empty(get_sub_field("image"))): ?>
                        <section class="section-scrollable section-three">
                            <div class="container-fluid container-fluid-cap">
                                <div class="row">
                                    <div class="image-col col col-12 col-sm-4 col-md-6 col-lg-7 order-1 order-md-2 left-aligned">
                                        <div class="image-container <?php echo $image_positioning; ?>"
                                             style="background-image: url('<?php echo get_image_url(get_sub_field("image")); ?>')"></div>
                                    </div>
                                    <div class="content-col col col-12 col-sm-8 col-md-6 col-lg-5 order-2 order-md-1 thick-pad-l <?php echo $image_positioning; ?>">
                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                        <?php // LINKS
                                        $rows_loop_name = "links";
                                        $color_class = "dark-btn";
                                        include('templates/partials/components/link-row-container.php');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php
                    endif;
                endwhile;
            endif;

            if (have_rows('section_four')):
                while (have_rows('section_four')) : the_row(); ?>

                    <?php
                    $image_positioning = get_sub_field("image_positioning");
                    if (isset($image_positioning) && !empty($image_positioning) && $image_positioning === "contained") {
                        $image_positioning = "contained";
                    } else {
                        $image_positioning = "exceeding";
                    }
                    ?>

                    <?php if (!empty(get_sub_field("title")) || !empty(get_sub_field("image"))): ?>
                        <section class="section-scrollable section-four">
                            <div class="container-fluid container-fluid-cap">
                                <div class="row">
                                    <div class="image-col col col-12 col-sm-4 col-md-6 col-xl-7 order-1 order-sm-2 order-md-1 right-aligned">
                                        <div class="image-container <?php echo $image_positioning; ?>"
                                             style="background-image: url('<?php echo get_image_url(get_sub_field("image")); ?>')"></div>
                                    </div>
                                    <div class="content-col col col-12 col-sm-8 col-md-6 col-xl-5 order-2 order-sm-1 order-md-2 thick-pad-r <?php echo $image_positioning; ?>">

                                        <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                        <div class="subcontent"><?php echo get_sub_field('sub_content'); ?></div>

                                        <?php // LINKS
                                        $rows_loop_name = "links";
                                        $color_class = "dark-btn";
                                        include('templates/partials/components/link-row-container.php');
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </section>
                    <?php
                    endif;
                endwhile;
            endif;


            if (have_rows('section_five')):
                while (have_rows('section_five')) : the_row(); ?>

                    <section class="section-scrollable section-five">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row video-row white-bg-row">
                                <div class="video-col col col-12 col-md-7 thick-pad-l">
                                    <div class="video-container">
                                        <div class="owl-carousel video-carousel">
                                            <?php while (have_rows('videos')) : the_row(); ?>
                                                <div class="video-container" data-video="<?php echo get_sub_field('video'); ?>">
                                                <?php echo do_shortcode("[lyte id='" . get_sub_field("video") . "' /]"); ?>
                                                    <!--<video class="video-js video-js-custom" controls preload="auto" width="640" height="264">
                                                        <p class="vjs-no-js">
                                                            To view this video please enable JavaScript, and consider upgrading to a web browser that
                                                            <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                                        </p>
                                                    </video>-->
                                                </div>
                                            <?php endwhile; ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="content-col col col-12 col-md-5 right-pad-for-menu">
                                    <?php
                                    if (have_rows('videos')):?>
                                        <div class="owl-carousel video-carousel-titles">
                                            <?php while (have_rows('videos')) : the_row(); ?>
                                                <div class="video-title"><?php echo get_sub_field('top_title'); ?></div>
                                            <?php endwhile; ?>
                                        </div>
                                        <div class="line-container thick">
                                            <span class="fill-line red-line"></span>
                                        </div>
                                        <div class="video-nav-container"></div>
                                    <?php endif; ?>
                                </div>
                            </div>
                            <div class="row product-data-video-related">
                                <div class="image-col col col-12 col-md-5 thick-pad-l">

                                    <div class="line-container thick">
                                        <span class="fill-line white-line"></span>
                                    </div>
                                    <?php if (get_sub_field("logo")) { ?>
                                        <img class="equipment-logo img-fluid" src="<?php echo get_image_url(get_sub_field("logo")); ?>" alt="Equipment Brand Logo">
                                    <?php } ?>
                                </div>
                                <div class="content-col col col-12 col-md-7 thick-pad-r">
                                    <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <div class="subcontent"><?php echo get_sub_field('content'); ?></div>

                                    <?php // LINKS
                                    $rows_loop_name = "links";
                                    $color_class = "white-btn dark-text-hover";
                                    include('templates/partials/components/link-row-container.php');
                                    ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    <script>
                        jQuery('#enquire-now').click(function(e) {
                            e.preventDefault();
                            jQuery([document.documentElement, document.body]).animate({
                                scrollTop: jQuery(".footer-red .form-section").offset().top
                            }, 2000);
                            var product = jQuery(this).attr('data-post-title');
                            jQuery('.footer-red .form-section #product').val(product);
                        });
                    </script>

                <?php
                endwhile;
            endif;

        endwhile;
    endif;

endwhile;
