<?php
/**
 * Template Name: Equipment Template
 */
?>

<?php while (have_posts()) : the_post();

$additional_heading_1 = get_field('additional_heading_1');
$additional_content_1 = get_field('additional_content_1');
$additional_heading_2 = get_field('additional_heading_2');
$additional_content_2 = get_field('additional_content_2');

    /**
     * Call the Hero and Sub Hero
     */
    ?>

    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>

    <?php
    $left_right_switch = "left";
    include('templates/partials/sub-hero.php');

    $terms = get_custom_categories_list('equipment-category');

    $isolated_category = false;
    if(get_field("isolate_category") && get_field("category_selected") && !empty(get_field("category_selected"))){
        //we retrieve the category selected
        $subCat = get_field("category_selected");
        //we modify the loop so that we actually loop only on an array composed by the single category, we are stripping everything else
        $terms = [$subCat->name];
        $isolated_category = true;
    }

    ?>

    <section class="section-scrollable filter-section <?php echo ($isolated_category) ? "isolated-category-filter" : ""; ?>" <?php echo ($isolated_category) ? "data-isolated-category='" . $subCat->slug . "'" : ""; ?>>
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <div class="filter-col col col-md-6 offset-md-5 thick-pad">
                    <h4 class="filter-label"><?php echo get_field('filter_label'); ?></h4>
                    <?php echo get_custom_categories_select('equipment-category'); ?>
                    <div class="subcontent"><?php echo get_field('filter_sub_content'); ?></div>
                </div>
            </div>
        </div>
    </section>

    <?php
    foreach ($terms as $term) :
        /**
         * Loop through the custom post type and display listing information
         */
        $loop = new WP_Query(
            array(
                'post_type' => 'equipment',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'tax_query' => array(
                    array(
                        'taxonomy' => "equipment-category",
                        'field' => 'slug', // term_id, slug
                        'terms' => $term,
                    ),
                ),
            )
        );

        if ($loop->have_posts()) :
            $cat = get_custom_categories_info('equipment-category', $term)[0];
            ?>
    <div style="display:none">
            <?php print_r($cat); ?>
    </div>
            <section class="section-scrollable equipment-category <?php echo $cat->slug; ?>">
                <div class="container-fluid container-fluid-cap">
                    <div class="row">
                        <div class="col col-11 col-sm-10 col-lg-6 thick-pad-l">
                            <?php if (isset($cat->name)) { ?><h2 data-title="<?php echo $cat->name; ?>" class="title"><?php echo get_field('equipment_name', 'equipment-category_' . $cat->term_taxonomy_id); ?></h2><?php } ?>
                        </div>
                        <div class="col col-12 thick-pad line-col">
                            <div class="line-container thick">
                                <span class="fill-line red-line"></span>
                            </div>
                        </div>
                        <div class="col col-11 col-sm-10 col-md-8 col-lg-6 thick-pad-l">
                            <?php if (isset($cat->description)) { ?>
                                <div class="subcontent"><?php echo get_field('equipment_description',  'equipment-category_' . $cat->term_taxonomy_id); ?></div> <?php } ?>
                        </div>
                        <div class="letstalk-col col col-lg-6 thick-pad-r">
                            <?php
                            if (isset($cat->term_id)) {
                                //Get the custom link field for the category, for use with name & description
                                if (have_rows('link', 'term_' . $cat->term_id)):
                                    while (have_rows('link', 'term_' . $cat->term_id)) : the_row();
                                        $link_type = get_sub_field('link_type');
                                        $link_text = get_sub_field('link_text');
                                        $link = get_sub_field($link_type . 'ternal_link');
                                        if ($link_type === "lt") {
                                            $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                            $scroll_down_class = "let-s-talk-link";
                                        } else {
                                            $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                            if ($link_type === "in") {
                                                if (get_post_type($link) === "attachment") {
                                                    $link = wp_get_attachment_url($link);
                                                    $target_blank = true;
                                                } else {
                                                    if (get_post_type($link) !== false) {
                                                        $link = get_the_permalink($link);
                                                    }
                                                }
                                            }

                                            $scroll_down_class = "";
                                        }

                                        $rows_loop_name = "links";
                                        $color_class = "white-btn dark-text-hover";
                                        $display_class = "d-inline-block";
                                        include('templates/partials/components/link-btn.php');
                                    endwhile;
                                endif;
                            }
                            ?>
                        </div>
                    </div>

                    <div class="row equipment-row">
                        <div class="col col-12">
                            <?php $i = 0;
                            $left_right_switch = "left";
                            while ($loop->have_posts()) : $loop->the_post();

                                if (have_rows('listing_information')):
                                    while (have_rows('listing_information')) : the_row(); ?>
                                        <?php
                                        /*
                                         * We add rows that will contain only couples of columns, this way we can
                                         * make appear content dependant on the resolution.
                                         * On desktop, the row.coupling-row will have the white overlay appearing
                                         * On mobile, the white overlay will belong to the single column
                                         */
                                        if ($i % 2 == 0) {
                                            echo "<div class='row thick-pad coupling-row'><div class='white-bg'></div>";
                                        }
                                        ?>
                                        <div class="image-col col col-12 col-lg-6 <?php echo $left_right_switch ?>-aligned">
                                            <div class="row">
                                                <div class="product-image-subcol col">
                                                    <div class="image-container">
                                                        <img class="img-fluid" src="<?php echo get_image_url(get_sub_field('main_image')); ?>"
                                                             alt="<?php echo get_sub_field('headline'); ?>">
                                                    </div>
                                                </div>
                                                <div class="product-label-subcol col">
                                                    <div class="row d-flex">
                                                        <div class="grow-1"></div>
                                                        <div class="maker-logo-container">
                                                            <img class="img-fluid maker-logo" src="<?php echo get_image_url(get_sub_field('logo_image')); ?>"
                                                                 alt="<?php echo get_sub_field('headline'); ?> - Maker's Logo">
                                                        </div>
                                                        <div class="headline-container">
                                                            <h4 class="headline"><?php echo get_sub_field('headline'); ?></h4>
                                                        </div>
                                                        <div class="plus-container">
                                                            <span class="plus"><i class="fas fa-plus"></i></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="content-col col col-12 col-lg-6 <?php echo $left_right_switch ?>-aligned">
                                            <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                            <div class="subcontent"><?php echo mb_strimwidth(get_sub_field('content'), 0, 170, "..."); ?></div>

                                            <div class="row link-row-container flex-row">
                                                <?php
                                                /* EXPLORE BUTTON */
                                                $link_text = "Explore";
                                                $link = get_post_permalink();

                                                if($post->ID == 5072) {
                                                  $link = '/our-equipment/appia-life/';
                                                }

                                                $target_blank = false;
                                                $rows_loop_name = "links";
                                                $color_class = "dark-btn";
                                                $display_class = "d-inline-block";
                                                $scroll_down_class = "";

                                                $disable_explore = get_field('disable_explore_link', $post->ID);

                                                if( !$disable_explore ) :
                                                    include('templates/partials/components/link-btn.php');
                                                endif;

                                                if (have_rows('links')):
                                                    while (have_rows('links')) : the_row();
                                                        $color_class = "dark-btn";
                                                        $display_class = "d-inline-block";
                                                        $link_type = get_sub_field('link_type');
                                                        $link_text = get_sub_field('link_text');
                                                        if ($link_type === "lt") {
                                                            $link = "#";   //this is a "Let's talk" link, that will work together with js to scroll down to the footer
                                                            $scroll_down_class = "let-s-talk-link";
                                                        } else {
                                                            $link = get_sub_field($link_type . 'ternal_link'); //link_type is either "ex" or "in"
                                                            if ($link_type === "in") {
                                                                if (get_post_type($link) === "attachment") {
                                                                    $link = wp_get_attachment_url($link);
                                                                    $target_blank = true;
                                                                } else {
                                                                    if (get_post_type($link) !== false) {
                                                                        $link = get_the_permalink($link);
                                                                    }
                                                                }
                                                            }

                                                            $scroll_down_class = "";
                                                        }

                                                        include('templates/partials/components/link-btn.php');
                                                    endwhile;
                                                endif;
                                                ?>
                                            </div>
                                        </div>
                                        <?php if ($i % 2 == 1) {
                                            echo "</div>";
                                        }
                                        $i++; ?>
                                    <?php endwhile; ?>
                                <?php endif;

                                if ($left_right_switch == "left") {
                                    $left_right_switch = "right";
                                } else {
                                    $left_right_switch = "left";
                                }
                            endwhile; ?>
                        </div>
                    </div>
                </div>
            </section>

        <?php endif;
        wp_reset_postdata();

    endforeach;?>


<?php if($additional_heading_1 != "") { ?>
                <section class="section-scrollable additional-content-section-1 pt-5 pb-5">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="col col-11 col-sm-10 col-lg-6 thick-pad-l">
                                <h2 data-title="<?php echo $additional_heading_1; ?>" class="title">
                                    <?php echo $additional_heading_1; ?>
                                </h2>
                            </div>
                            <div class="col col-12 thick-pad line-col mt-3 mb-3">
                                <div class="line-container thick">
                                    <span class="fill-line red-line" style="width: 100%;"></span>
                                </div>
                            </div>
                            <div class="col col-11 col-sm-10 col-md-8 col-lg-6 thick-pad-l">
                                <div class="subcontent">
                                    <?php echo $additional_content_1; ?>
                                </div>
                            </div>
                            <div class="letstalk-col col col-lg-6 thick-pad-r"></div>
                        </div>
                    </div>
                </section>
            <?php } ?>

            

            <?php if($additional_heading_2 != "") { ?>
                <section class="section-scrollable additional-content-section-1 pt-5 pb-5">
                    <div class="container-fluid container-fluid-cap">
                        <div class="row">
                            <div class="col col-11 col-sm-10 col-lg-6 thick-pad-l">
                                <h2 data-title="<?php echo $additional_heading_2; ?>" class="title">
                                    <?php echo $additional_heading_2; ?>
                                </h2>
                            </div>
                            <div class="col col-12 thick-pad line-col mt-3 mb-3">
                                <div class="line-container thick">
                                    <span class="fill-line red-line" style="width: 100%;"></span>
                                </div>
                            </div>
                            <div class="col col-11 col-sm-10 col-md-8 col-lg-6 thick-pad-l">
                                <div class="subcontent">
                                    <?php echo $additional_content_2; ?>
                                </div>
                            </div>
                            <div class="letstalk-col col col-lg-6 thick-pad-r"></div>
                        </div>
                    </div>
                </section>
            <?php } ?>
<?php endwhile; 
?>
