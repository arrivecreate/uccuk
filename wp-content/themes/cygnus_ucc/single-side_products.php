<?php

/**
 * This single is specific to the Side Products
 */

while (have_posts()) : the_post(); ?>

    <div class="container-fluid container-fluid-cap">
        <?php get_template_part('templates/partials/classic-image-hero-side-products', 'classic-image-hero-side-products'); ?>
    </div>

<?php
//Loop through the main information group
    if (have_rows('main_information')):
        while (have_rows('main_information')) : the_row();

            //Loop through the first section
            if (have_rows('products')):
                while (have_rows('products')) : the_row(); ?>

                    <section class="section-scrollable single-product-case">
                        <div class="container-fluid container-fluid-cap">
                            <div class="row">
                                <div class="image-col col col-12 col-md-5 order-1 order-md-2 common-pad-r">
                                    <?php
                                    $text_alignment = "left";
                                    $image = get_sub_field('image');
                                    $quote = '';
                                    $vertical_alignment = "top";
                                    $box_color = "silver-lines";
                                    include('templates/partials/components/mirror-quote.php');
                                    ?>
                                </div>

                                <div class="content-col col col-12 col-md-7 order-1 order-md-1 thick-pad-l common-pad-r align-self-center">
                                    <h3 class="title"><?php echo get_sub_field('title'); ?></h3>
                                    <div class="line-container thick">
                                        <span class="fill-line red-line"></span>
                                    </div>
                                    <div class="subcontent"><?php echo get_sub_field('content'); ?></div>

                                    <?php // LINKS
                                    $rows_loop_name = "links";
                                    $color_class = "dark-btn";
                                    include('templates/partials/components/link-row-container.php');
                                    ?>
                                </div>

                            </div>
                        </div>
                    </section>

                <?php endwhile;
            endif;

        endwhile;
    endif;

endwhile;