<?php
/**
 * Template Name: Careers Template
 */
?>

<?php while (have_posts()) : the_post();

    /**
     * Do Careers stuff here!
     */ ?>
    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">
            <?php get_template_part('templates/partials/classic-hero', 'classic-hero'); ?>
        </div>
    </section>

    <section class="section-scrollable careers-sub-hero">
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <div class="content-col col col-12 col-md-10 col-lg-7 thick-pad-l">
                    <h3 class="title"><?php echo get_field('video_title'); ?></h3>
                    <div class="subcontent"><?php echo get_field('video_paragraph'); ?></div>
                </div>

                <?php if (!empty(get_field('video_call_to_action'))) { ?>
                    <div class="col col-12 col-md-9 offset-md-3 right-pad-for-menu">
                      <h3 class="title video-title"><?php echo get_field('video_call_to_action'); ?></h3>
                    </div>
                <?php } ?>

                <div class="video-col col col-12 col-md-9 offset-md-3 right-pad-for-menu">
                    <?php if (!empty(get_field('video_id'))) { ?>
                        <style>
                            .vjs-youtube-mobile .vjs-poster {
                                background-image : url("https://img.youtube.com/vi/<?php echo get_field('video_id'); ?>/maxresdefault.jpg");
                                display          : inline-block !important;
                                z-index          : 35;
                            }
                        </style>
                        <div class="video-container">
                            <video id="careers-video" class="video-js" controls preload="auto" width="640" height="264"
                                   data-setup='{ "techOrder": ["youtube"], "sources": [{ "type": "video/youtube", "src": "http://www.youtube.com/watch?v=<?php echo get_field('video_id'); ?>"}] }'>
                                <p class="vjs-no-js">
                                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                                    <a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                                </p>
                            </video>
                        </div>
                    <?php } elseif (get_field('video_image_fallback')) { ?>
                        <div class="fallback-image" style="background-image:url('<?php echo get_field('video_image_fallback'); ?>')"></div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </section>

    <section class="section-scrollable midcontent-section">
        <div class="container-fluid container-fluid-cap">
            <div class="row">
                <div class="content-col col col-12 col-lg-8 thick-pad">
                    <h3 class="title"><?php echo get_field('midcontent_title'); ?></h3>
                    <div class="subcontent"><?php echo get_field('midcontent_subcontent'); ?></div>
                </div>
            </div>
        </div>
    </section>

    <section class="section-scrollable">
        <!--<iframe id="careersiframe" name="iframe" src="https://cw.na1.hgncloud.com/ucccoffee/index.do" width="100%" height="900px"
                scrolling="auto" frameborder="0" class="wrapper" sandbox="allow-scripts allow-popups allow-same-origin allow-forms">
            This option will not work correctly. Unfortunately, your browser does not support inline frames.
        </iframe>
		-->
		<iframe id="careersiframe" name="iframe" src="https://jobs.jobvite.com/ucc-coffee-co-uk" width="100%" height="900px"
                scrolling="auto" frameborder="0" class="wrapper" sandbox="allow-scripts allow-popups allow-same-origin allow-forms">
            This option will not work correctly. Unfortunately, your browser does not support inline frames.
        </iframe>
    </section>
    <?php
    /**
     * Call the divided colour block - single ACFs
     */
    $left_right_switch = "left";
    include('templates/partials/divided-colour-block.php');
endwhile; ?>
