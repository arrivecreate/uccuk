<?php
/**
 * Template Name: Appia Life Template
 */

while (have_posts()) : the_post(); ?>

  <style>
    @import url('https://fonts.googleapis.com/css?family=Athiti:500&display=swap');
    @import url('https://fonts.googleapis.com/css?family=Play:700&display=swap');

    @font-face {
        font-family : "Big River Script";
        font-style  : normal;
        font-weight : 700;
        src         : url("/wp-content/themes/cygnus_ucc/dist/fonts/big_river_script.ttf") format("truetype");
    }

    body .subcontent, html .subcontent {
      font-family: "Athiti", sans-serif;
      font-weight: 500;
    }

    .custom-cpt-hero .hero-classic-content .title, .section-appia-life h3.title {
      font-family: "Play", sans-serif;
    }

    .custom-cpt-hero .hero-classic-content .title {
      font-size: 3.25rem;
      line-height: 1.01;
      letter-spacing: 0.4rem;
    }

    .section-appia-life h3.title {
      font-size: 2.2rem;
      line-height: 1.06;
      letter-spacing: 0;
    }

    @media screen and (min-width : 768px) {
      .section-appia-life h3.title {
        font-size: 2.4rem;
      }
    }

    @media screen and (min-width : 992px) {
      .section-appia-life h3.title {
        font-size: 2.6rem;
      }
    }

    @media screen and (min-width : 1200px) {
      .section-appia-life h3.title {
        font-size: 2.8rem;
      }
    }

    .hero-appia-life .subtitle {
      padding-top : 1.5rem;
    }

    .hero-appia-life .red-font,
    .section-appia-life .red-font {
      color : #BC001E;
    }

    .first-section.section-appia-life {
      margin-top : -20px;
    }

    .first-section.section-appia-life .appia-life-logo {
      width: 50%;
      max-width: 294px;
      height: auto;
      margin-top: 20px;
      margin-bottom: 20px;
    }

    .first-section.section-appia-life h3.title {
      font-family: "Big River Script", sans-serif;
      font-size: 2.1rem;
      line-height: 1.34;
      text-transform: none;
    }

    .first-section.section-appia-life .image-container {
      position            : absolute;
      top                 : 0;
      left                : 0;
      display             : block;
      width               : 100%;
      height              : 100%;
      background-size     : contain;
      background-repeat   : no-repeat;
      background-position : 100% 0;
    }

    .section-appia-life {
      padding-top    : 4.6rem;
      padding-bottom : 10rem;
    }

    .first-section.section-appia-life {
      background  : #BC001E;
      color       : #fff;
      padding-top : 10rem;
    }

    .second-section.section-appia-life {
      padding-top : 13rem;
    }

    .third-section.section-appia-life {
      background  : #FFD500;
      padding-top : 22rem;
    }

    .fourth-section.section-appia-life {
      background     : #28B6BE;
      color          : #fff;
      padding-top    : 22rem;
      padding-bottom : 2rem;
    }

    .fifth-section.section-appia-life {
      background-color    : #6CB52D;
      color               : #fff;
      padding-top         : 2rem;
    }

    .third-section.section-appia-life .floated-img {
      position  : absolute;
      top       : -360px;
      left      : 100%;
      transform : translateX(-40%);
    }

    .fourth-section.section-appia-life .floated-img {
      position : absolute;
      top      : -360px;
      left     : 25%;
    }

    .first-section.section-appia-life .slide-in-img {
      position            : absolute;
      top                 : 1rem;
      left                : 0;
      width               : 100%;
      height              : 1px;
      padding-bottom      : 350%;
      background-size     : cover;
      background-repeat   : no-repeat;
      background-position : 0;
    }

	.second-section .box-image-col,
	.third-section .box-image-col,
	.fourth-section .box-image-col {
		height:0px;
	}

	.second-section .box-image-col .box-image-container {
		position: relative;
		top: -15rem;
		left: -102%;
		display: block;
		width: 210%;
		height: 1px;
		padding-bottom: 180%;
		background-size: contain;
		background-repeat: no-repeat;
		background-position: 50%;
	}

	.fourth-section .box-image-col .box-image-container {
		position: relative;
		top: -8rem;
		left: -102%;
		display: block;
		width: 218%;
		height: 1px;
		padding-bottom: 180%;
		background-size: contain;
		background-repeat: no-repeat;
		background-position: 50%;
	}

	.third-section .box-image-col .box-image-container {
		position: relative;
		top: -8rem;
		right: 10%;
		display: block;
		width: 218%;
		height: 1px;
		padding-bottom: 180%;
		background-size: contain;
		background-repeat: no-repeat;
		background-position: 50%;
	}

    /*.fourth-section.section-appia-life .slide-in-img {
      position            : absolute;
      left                : 0;
      width               : 100%;
      height              : 1px;
      padding-bottom      : 350%;
      background-size     : cover;
      background-repeat   : no-repeat;
      background-position : 88%;
    }*/

    /*.second-section.section-appia-life .slide-in-img {
      top                 : -20rem;
      background-position : 95%;
    }*/

    /*.fourth-section.section-appia-life .slide-in-img {
      top : -12rem;
    }*/


    /*.third-section.section-appia-life .slide-in-img {
      position            : absolute;
      top                 : -4.5rem;
      right               : 0;
      width               : 100%;
      height              : 1px;
      padding-bottom      : 350%;
      background-size     : cover;
      background-repeat   : no-repeat;
      background-position : 10%;
    }*/

    .bottom-items {
      display         : flex;
      justify-content : flex-start;
      padding         : 1rem 0;
    }

    .bottom-items .bottom-item-inner {
      text-align  : left;
      font-weight : 600;
    }

    .mobile-image {
      display : none !important;
    }

    @media screen and (max-width : 1440px) {

      .first-section.section-appia-life .desktop-image {
        height : 800px !important;
      }

      .desktop-image {
        height : 1300px !important;
      }
    }

    @media screen and (max-width : 1024px) {
      .third-section.section-appia-life .floated-img {
        width : 60%;
      }

      .first-section.section-appia-life .desktop-image {
        height : 460px !important;
        top    : 14rem;
      }

      .desktop-image {
        height : 900px !important;
      }

      .second-section.section-appia-life {
        padding-top: 10rem;
      }
      .second-section.section-appia-life .slide-in-img {
        top : -8rem;
      }

      .third-section.section-appia-life {
        padding-top : 17rem;
      }

      .fourth-section.section-appia-life .slide-in-img {
        top : 0rem;
      }

      .fourth-section.section-appia-life .floated-img {
        position : absolute;
        top      : -150px;
        left     : 8%;
        width    : 90%;
      }

      .fourth-section.section-appia-life {
        padding-top : 14rem;
      }

    }

    @media screen and (max-width : 768px) {
      .floated-img {
        display : none;
      }

      .first-section.section-appia-life .desktop-image {
        height : 80% !important;
        top    : 14rem;
      }

      .first-section.section-appia-life {
        padding-top : 6rem;
      }

      .third-section.section-appia-life {
        padding-top : 12rem;
      }

      .fourth-section.section-appia-life {
        padding-top : 10rem;
      }

      .second-section.section-appia-life .slide-in-img {
        top : -3rem;
      }

      .fourth-section.section-appia-life .slide-in-img {
        top : 5rem;
      }

      .desktop-image {
        height : 700px !important;
      }
    }

    @media screen and (max-width : 767px) {

      .custom-cpt-hero.hero-appia-life .inner-padding {
        padding-top : 8rem;
      }

      .bottom-items {
        justify-content : flex-start;
      }

      .bottom-items .bottom-item-inner {
        text-align  : left;
        font-weight : 600;
        padding     : 1.5rem;
      }

      .mobile-image {
        padding   : 4rem;
        width     : 80%;
        position  : absolute;
        left      : 50%;
        transform : translateX(-50%);
      }

      .mobile-image {
        display : block !important;
      }

      .desktop-image,
      .floated-img {
        display : none !important;
      }

      .first-section.section-appia-life {
        padding-bottom : 20rem;
        margin-top     : 50px;
      }

      .second-section.section-appia-life {
        padding-top    : 20rem;
        padding-bottom : 14rem;
      }

      .third-section.section-appia-life,
      .fourth-section.section-appia-life {
        padding-top    : 14rem;
        padding-bottom : 14rem;
      }

      .fifth-section.section-appia-life {
        padding-top    : 14rem;
        padding-bottom : 14rem;
      }

      .fourth-section.section-appia-life .mobile-image,
      .third-section.section-appia-life .mobile-image,
      .second-section.section-appia-life .mobile-image {
        padding : 0 4rem;
      }
    }

    @media screen and (max-width : 567px) {

      .first-section.section-appia-life {
        padding-bottom : 20rem;
      }

      .second-section.section-appia-life {
        padding-top : 20rem;
      }

      .second-section.section-appia-life .mobile-image,
      .third-section.section-appia-life .mobile-image,
      .fourth-section.section-appia-life .mobile-image {
        padding : 1rem;
        width   : 90%;
      }

      .first-section.section-appia-life {
        padding-bottom : 10rem;
        padding-top    : 0;
      }

      .third-section.section-appia-life,
      .fourth-section.section-appia-life,
      .fifth-section.section-appia-life {
        padding-top : 10rem;
      }

      .fourth-section.section-appia-life {
        padding-bottom : 10rem;
      }
    }

    @media screen and (max-width : 414px) {
      .fourth-section.section-appia-life,
      .fifth-section.section-appia-life {
        padding-top : 12rem;
      }
    }

    @media screen and (max-width : 375px) {
      .first-section.section-appia-life {
        padding-bottom : 10rem;
        padding-top    : 0;
        margin-top     : 50px;
      }

      .second-section.section-appia-life {
        padding-top : 10rem;
        padding-bottom : 10rem;
      }

      .third-section.section-appia-life,
      .fourth-section.section-appia-life,
      .fifth-section.section-appia-life {
        padding-top : 10rem;
      }

      .fourth-section.section-appia-life {
        padding-bottom : 10rem;
      }
    }

    @media screen and (max-width : 320px) {

      .second-section.section-appia-life {
        padding-top : 8rem;
        padding-bottom : 8rem;
      }

      .third-section.section-appia-life,
      .fourth-section.section-appia-life {
        padding-top    : 8rem;
        padding-bottom : 10rem;
      }

      .fifth-section.section-appia-life {
        padding-top    : 8rem;
        padding-bottom : 10rem;
      }
    }
  </style>

  <div class="custom-cpt-hero hero-appia-life">
    <div class="container-fluid container-fluid-cap">
      <div class="row">

        <div class="container-fluid container-fluid-cap">
          <a class="brand" href="<?= esc_url(home_url('/')); ?>">
            <img class="logo" src="<?php echo get_template_directory_uri(); ?>/dist/images/ucc-logo.svg"
                 alt="<?php bloginfo('name'); ?>">
          </a>

          <div class="current-page-container">
            <span class="page-breadcrumb">Appia Life</span>
          </div>
        </div>

        <div class="line-col col col-12 col-md-3 order-2 order-md-1">
          <div class="line-container vertical-line hero-line-container thick">
            <div class="fill-line dark-line" style="max-height:280px;"></div>
          </div>
        </div>

        <div class="content-col col col-12 col-md-8 col-lg-8 order-3 order-md-2 align-self-center">
          <div class="inner-padding">
            <div class="hero-classic-content">
              <h1 class="title"><img src="<?= get_template_directory_uri() . '/appia-life/ns-logo@2x.png' ?>" alt="Nuova Simonelli's" class="equipment-logo img-fluid"> <br />Appia Life <br /><small>Coffee Machines</small><br/></h1>
              <div class="line-container thick">
                <div class="fill-line dark-line"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <section class="section-scrollable first-section section-appia-life">
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="content-col col col-12 col-sm-12 col-md-6 thick-pad-l align-self-center">
          <img src="<?= get_template_directory_uri() . '/appia-life/appia-logo@2x.png' ?>" alt="appia-life-logo"
               class="appia-life-logo">
          <h3 class="title">Coffee machines for everyday</h3>
          <div class="subcontent">The next generation of Appia: the coffee machine loved by thousands of baristas
            worldwide. Appia Life takes on the same proven technology for exceptional coffee quality but with new,
            enhanced features, energy saving and
            barista-friendly design. To make great coffee even simpler.
          </div>
          <div class="row link-row-container flex-row ">

            <div class="col-float">
              <a href="/wp-content/uploads/2019/10/Caffe-Culture-Brochure_Digital_3-compressed.pdf" aria-label="brochure"
                 class="link-btn white-btn d-inline-block " target="_blank">
                <span class="link-text">brochure</span>
              </a>
            </div>
            <div class="col-float">
              <a href="/contact-page/" aria-label="Back to range" class="link-btn white-btn d-inline-block ">
                <span class="link-text">Contact Us</span>
              </a>
            </div>

          </div>
        </div>
        <div class="box-image-col col col-12 col-sm-12 col-md-6 common-pad-l right-pad-for-menu">
          <div class="desktop-image slide-in-img slide-from-bottom-img"
               style="background-image: url('<?= get_template_directory_uri() . '/appia-life/AppiaLife_2grAlti_bianco_profilo-MS@2x.png' ?>'); visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); padding-bottom: 0; height: 840px;"
               data-hook="0.58"></div>
          <div class="mobile-image">
            <img src="<?= get_template_directory_uri() . '/appia-life/AppiaLife_2grAlti_bianco_profilo-MS@2x.png' ?>"
                 class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-scrollable second-section section-appia-life" style="position:relative">
  	<div class="random-background" style="position: absolute; right: 0; bottom: 0; z-index: 0; transform: translateY(50%);">
		<img src="<?= get_template_directory_uri() . '/appia-life/big grey life.png' ?>" style="max-width: 800px; width: 100%; opacity: 0.8;">
	</div>
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="box-image-col col col-12 col-sm-12 col-md-5">
          <div id="home-section-1-img" class="box-image-container desktop-image slide-in-img slide-from-bottom-img"
               style="background-image: url('<?= get_template_directory_uri() . '/appia-life/machine-02@2x_large.png' ?>'); visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"
               data-hook="0.58"></div>
        </div>
        <div class="content-col col col-12 col-sm-12 col-md-7 common-pad-l align-self-center">
          <h3 class="title">Proven Performance, New&nbsp;Generation</h3>
          <div class="subcontent">Designed for consistent, exceptional coffee. Appia Life features the
            same proven technology as previous Appia models with new, improved
            features. Includes built-in volumetric dosing, push & pull levers, soft-
            touch portafilter, adjustable steel steam wand, wide-work surface,
            water level indicator and dual scale pressure gauge.
          </div>
          <div class="mobile-image">
            <img src="<?= get_template_directory_uri() . '/appia-life/machine-02@2x_large.png' ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>

  </section>

  <section class="section-scrollable third-section section-appia-life">
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="content-col col col-12 col-sm-12 col-md-7 thick-pad-l align-self-center">
          <h3 class="title">RELIABILITY<br/> PERFORMANCE,<br/> MAXIMUM QUALITY</h3>
          <div class="subcontent">Dependable and consistent quality coffee. Solid build quality and reliable
            performance. Features include Soft Infusion System (SIS), which gently pre-wets the coffee cake for a
            gentle, even extraction resulting in maximum flavour. Made from durable and sustainable materials.
          </div>
          <div class="row link-row-container flex-row ">

            <div class="col-float">
              <a href="/wp-content/uploads/2019/10/Caffe-Culture-Brochure_Digital_3-compressed.pdf" aria-label="brochure"
                 class="link-btn dark-btn d-inline-block " target="_blank">
                <span class="link-text">brochure</span>
              </a>
            </div>
            <div class="col-float">
              <a href="/contact-page/" aria-label="Back to range" class="link-btn dark-btn d-inline-block ">
                <span class="link-text">Contact Us</span>
              </a>
            </div>

          </div>
        </div>
        <div class="box-image-col col col-12 col-sm-12 col-md-5 pr-0" style="z-index: 2;">
          <div id="home-section-1-img" class="box-image-container desktop-image slide-in-img slide-from-bottom-img"
               style="background-image: url('<?= get_template_directory_uri() . '/appia-life/machine - 03@2x.png' ?>'); visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"
               data-hook="0.58"></div>
          <div class="mobile-image" style="z-index: 2;">
            <img src="<?= get_template_directory_uri() . '/appia-life/machine - 03@2x.png' ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-scrollable fourth-section section-appia-life" style="position:relative">
	<div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; overflow: hidden;">
		<div class="random-background" style="position: absolute;right: 0;bottom: 0;z-index: 0;transform: translateY(50%);">
			<img src="/wp-content/themes/cygnus_ucc/appia-life/bigBlueLife.png" style="width: 150%;height: auto; opacity:0.5">
		</div>
	</div>
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="box-image-col col col-12 col-sm-12 col-md-5" style="z-index:2">
          <div id="home-section-1-img" class="box-image-container desktop-image slide-in-img slide-from-bottom-img"
               style="background-image: url('<?= get_template_directory_uri() . '/appia-life/machine - 04@2x_large.png' ?>'); visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);"
               data-hook="0.58"></div>
        </div>
        <div class="content-col col col-12 col-sm-12 col-md-7 common-pad-l align-self-center right-pad-for-menu">
          <h3 class="title">LESS MACHINE,<br/> MORE ENGAGEMENT</h3>
          <div class="subcontent">Appia Life features a 30mm lower profile than its previous model for increased
            customer interaction. Its cup warmer is aligned to the top of machine for easier
            reach and speed. <br/><br/>
            <h3 class="title">New barista-friendly design</h3>
            Designed for simplicity of use with new ergonomic features to improve day to day
            usability for baristas. Includes a reverse mirror, which allows the barista to check
            the group head for cleanliness and dripping from a comfortable position, user-
            friendly push button panel which is more responsive to the barista. All in a sleek
            look with chrome panels.
          </div>
          <div class="mobile-image" style="z-index: 2;">
            <img src="<?= get_template_directory_uri() . '/appia-life/machine - 04@2x_large.png' ?>" class="img-fluid">
          </div>
        </div>
      </div>
    </div>
  </section>

  <section class="section-scrollable fifth-section section-appia-life" style="position:relative">
	<div style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; overflow: hidden;">
		<div class="random-background" style="position: absolute;right: 0;top: 0;z-index: 0;transform: translateY(-50%);">
			<img src="/wp-content/themes/cygnus_ucc/appia-life/bigBlueLife.png" style="width: 150%;height: auto;opacity:0.5">
		</div>
	</div>
    <div class="container-fluid container-fluid-cap">
      <div class="row">
        <div class="image-col col col-12 col-sm-12 col-md-5 common-pad-l right-pad-for-menu">
        </div>
        <div class="content-col col col-12 col-sm-12 col-md-7 common-pad-l align-self-center right-pad-for-menu">
          <h3 class="title">SMART, SIMPLE AND ENERGY EFFICIENT</h3>
          <div class="subcontent">Unique features keep things simple. Automatic cleaning function, adjustable steam
            arm, easy maintenance with adjustable nozzle, new side opening system and
            redesigned groups to ensure minimal disruption. Additional optional features are
            also available.<br /><br />
            Improved insulation thanks to Drytex® Thermical technology wrapped around the
            boiler. 13% less energy consumption and 20% overall reduction in energy
            consumption and environmental impact than previous model*.
            <br/><br/>
            * Appia II vs Appia Life Lifecycle Asessment
          </div>
          <div class="row link-row-container flex-row bottom-items">
            <div class="col-float">
              <a href="/wp-content/uploads/2019/10/Caffe-Culture-Brochure_Digital_3-compressed.pdf" aria-label="brochure"
                 class="link-btn white-btn dark-text-hover d-inline-block" target="_blank">
                <span class="link-text">brochure</span>
              </a>
            </div>
            <div class="col-float">
              <a href="/contact-page/" aria-label="Back to range"
                 class="link-btn white-btn dark-text-hover d-inline-block">
                <span class="link-text">Contact Us</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


<?php endwhile; ?>
