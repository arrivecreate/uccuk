<?php
/**
 * Template Name: Contacts Page Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

    <section class="section-scrollable">
        <div class="container-fluid container-fluid-cap">

        <div class="hero-classic-container js-positioning" style="margin-bottom:40px !important;">
            <div class="hero-classic-img" style="background-image: url('<?php get_site_url(); ?>/wp-content/uploads/2021/10/MicrosoftTeams-image-1.jpeg')"></div>

            <a class="brand" href="<?php get_site_url(); ?>">
                <img alt="UCC Coffee STAGE" data-src="<?php get_site_url(); ?>/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg" class="logo lazyloaded" src="<?php get_site_url(); ?>/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg"><noscript><img class="logo" src="https://uccuk.uccstage.co.uk/wp-content/themes/cygnus_ucc/dist/images/ucc-logo.svg" alt="UCC Coffee STAGE"></noscript>
            </a>
        </div>


            <div class="content-row row">
                <div class="form-col-intro col col-12 col-md-12 thick-pad-l thick-pad-r">
                    <h2 style="display: inline-block;border-bottom: 2px solid;padding-bottom: 30px;margin-bottom: 30px;color: #d51316;" class="title"><?php echo (get_field("intro_title")) ? get_field("intro_title") : "How can we help?"; ?></h2>
                    <h2 style="color: #d51316;" class="title"><?php echo (get_field("faq_title")) ? get_field("faq_title") : "How can we help?"; ?></h2>
                    <p class="subcontent"><?php echo get_field("faq_intro"); ?></p>
                </div>

                <?php $faqs = get_field("faqs"); ?>
                <?php $total = count($faqs); ?>
                <?php $half = ceil($total/2); ?>

                <div class="faq-col-1 form-col-intro col col-12 col-md-6 thick-pad-l">

                    <?php $i = 0; foreach($faqs as $faq) { ?>
                        <?php if($i < $half) { ?>
                            <div class="faq_question" data-count="<?php echo $i; ?>">
                                <h1 class="title"><?php echo $faqs[$i]['question']; ?></h1>
                                <div class="subcontent"><?php echo $faqs[$i]['answer']; ?></div>
                                <?php $i++; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                    
                </div>
                <div class="faq-col-2 form-col-intro col col-12 col-md-6 thick-pad-r">
                    <?php foreach($faqs as $faq) { ?>
                        <?php if($i >= $half) { ?>
                            <div class="faq_question" data-count="<?php echo $i; ?>">
                                <h1 class="title"><?php echo $faqs[$i]['question']; ?></h1>
                                <div class="subcontent"><?php echo $faqs[$i]['answer']; ?></div>
                                <?php $i++; ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>

                <div class="form-col-intro col col-12 col-md-7 thick-pad-l">
                    <h3 class="form_title">Drop us a line</h3>
                    <p class="subcontent"><?php echo (get_field("intro_paragraph")) ? get_field("intro_paragraph") : "Can't quite find what you're looking for, want to chat to one of our team or give us some feedback... Get in touch. We
                        look forward to hearing from you."; ?>
                    </p>
                </div>
                <div class="form-col col col-12 col-lg-11 thick-pad-l right-pad-for-menu">
                    <div id="footer_form_lbl">

                        <?php echo do_shortcode('[contact-form-7 id="6337"]'); ?>

                        <!-- <script>
                            jQuery(document).ready(function ($) {
                                $("footer .section-scrollable").remove();

                                var formIntro = $(".form-col-intro"),
                                    formContainer = $("#footer_form_lbl"),
                                    contactUsForm = $("#contact-us-form"),
                                    formSpinner = contactUsForm.find(".ajax-loader-container"),
                                    contactUsFormTl = new TimelineMax(),
                                    formResponse = $("#contact-us-form-response"),
                                    ajaxError = contactUsForm.find(".error-box"),
                                    restartBtn = formResponse.find(".restart-form-btn"),
                                    mailRegex = /(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@[*[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+]*/,
                                    telephoneRegex = /^[\d\s\S]+/;

                                contactUsForm.on("click", ".error-msg", function () {
                                    $(this).fadeOut();
                                });

                                restartBtn.click(function (e) {
                                    e.preventDefault();

                                    contactUsFormTl
                                        .staggerTo(formResponse.children(), 1, {
                                            x: 20,
                                            y: 0,
                                            autoAlpha: 0,
                                            onComplete: function () {
                                                formResponse.slideUp();
                                                formIntro.slideDown();
                                                formContainer.slideDown(function () {
                                                    contactUsFormTl
                                                        .to(contactUsForm, 0.35, {
                                                            scaleX: 1,
                                                            scaleY: 1,
                                                            autoAlpha: 1
                                                        });
                                                });
                                            }
                                        }, 0.1);

                                });

                                function assignError(elem, p) {
                                    if (elem.children(".error-msg").length == 0) {
                                        elem.prepend("<div class='error-msg'></div>");
                                    }
                                    elem.addClass("error").find(".error-msg").html("<p>" + p + "</p>").fadeIn();
                                }

                                function removeError(elem) {
                                    elem.removeClass("error").find(".error-msg").fadeOut();
                                }

                                contactUsForm.find(".submit-btn").click(function (e) {
                                    e.preventDefault();

                                    var data = contactUsForm.serialize(),
                                        mailCanBeSent = true;

                                    ajaxError.slideUp();
                                    formSpinner.addClass("active");

                                    contactUsForm.find("input, textarea").each(function (index, elem) {
                                        var $elem = $(elem);

                                        if (!(($elem).hasClass("required") && ($elem.attr("type") !== "checkbox") && !$elem.val())
                                            && !(($elem).hasClass("email") && !mailRegex.test($elem.val()))
                                            && !(($elem).hasClass("telephone") && !telephoneRegex.test($elem.val()))
                                            && !(($elem).hasClass("required") && ($elem.attr("type") === "checkbox" && !$elem.prop("checked")))) {
                                            removeError($elem.closest(".input-wrapper"));
                                        }

                                        if (($elem).hasClass("required") && ($elem.attr("type") !== "checkbox") && !$elem.val()) {
                                            mailCanBeSent = false;
                                            assignError($elem.closest(".input-wrapper"), "This field is required.");
                                        }

                                        if (($elem).hasClass("required") && ($elem.attr("type") === "checkbox" && !$elem.prop("checked"))) {
                                            mailCanBeSent = false;
                                            assignError($elem.siblings(".input-wrapper"), "This field is required.");
                                        }

                                        if (($elem).hasClass("email") && !mailRegex.test($elem.val())) {
                                            mailCanBeSent = false;
                                            assignError($elem.closest(".input-wrapper"), "The e-mail address entered is invalid.");
                                        }

                                        if (($elem).hasClass("telephone") && ($elem.val() && !telephoneRegex.test($elem.val()))) {
                                            mailCanBeSent = false;
                                            assignError($elem.closest(".input-wrapper"), "The telephone number is invalid.");
                                        }
                                    });

                                    if (mailCanBeSent) {
                                        $.ajax({
                                            url: ajax_object['ajax_url'],
                                            type: "POST",
                                            data: {"data": data, "sendTo": "<?php echo get_field("email_for_contact_form"); ?>", "action": "send_email"}
                                        }).done(function (response) {
                                            contactUsFormTl
                                                .to(contactUsForm, 0.28, {
                                                    autoAlpha: 0,
                                                    scaleX: 0.95,
                                                    scaleY: 0.95,
                                                    onComplete: function () {
                                                        formContainer.slideUp();
                                                        formIntro.slideUp();
                                                        formResponse.slideDown(function () {
                                                            contactUsFormTl
                                                                .staggerFromTo(formResponse.children(), 1, {
                                                                    x: 20,
                                                                    y: 0,
                                                                    autoAlpha: 0
                                                                }, {
                                                                    x: 0,
                                                                    y: 0,
                                                                    autoAlpha: 1
                                                                }, 0.1);
                                                        });
                                                    }
                                                });
                                        }).fail(function (jqHXRm, textStatus) {
                                            ajaxError.slideDown();
                                        }).always(function (data) {
                                            formSpinner.removeClass("active");
                                        });
                                    } else {
                                        formSpinner.removeClass("active");
                                    }
                                });
                            });
                        </script>

                        <form action="#" id="contact-us-form" method="post" novalidate="novalidate">
                            <div class="row">
                                <div class="text-input-col col col-12 col-md-6 align-self-start">
                                    <label for="contact-page-your-name">Your Name</label>
                                    <div class="input-wrapper">
                                        <input type="text" id="contact-page-your-name" name="your-name" value="" size="40"
                                               class="required" aria-required="true" aria-invalid="false"
                                               placeholder="Name">
                                    </div>

                                    <label for="contact-page-your-email">Your Email</label>
                                    <div class="input-wrapper">
                                        <input type="email" id="contact-page-your-email" name="contact-page-your-email" value="" size="40"
                                               class="required email"
                                               aria-required="true" aria-invalid="false" placeholder="Email">
                                    </div>

                                    <label for="contact-page-your-telephone">Telephone Number</label>
                                    <div class="input-wrapper">
                                        <input type="tel" id="contact-page-your-telephone" name="your-telephone" value="" size="40" class="required telephone"
                                               aria-required="true"
                                               aria-invalid="false" placeholder="Telephone">
                                    </div>

                                    <label for="contact-page-company">Company</label>
                                    <div class="input-wrapper">
                                        <input type="text" id="contact-page-company" name="company" value="" size="40" class="required" aria-invalid="false"
                                               placeholder="Company">
                                    </div>

                                    <label for="contact-page-postcode">Postcode</label>
                                    <div class="input-wrapper">
                                        <input type="text" id="contact-page-postcode" name="postcode" value="" size="40" class="required" aria-invalid="false"
                                               placeholder="Postcode">
                                    </div>

                                    <div class="message-and-checkbox-col">
                                        <label id="contact-page-your-message">Message</label>
                                        <div class="input-wrapper">
                                        <textarea id="contact-page-your-message" name="your-message" cols="40" rows="10" class="required" aria-invalid="false"
                                                  placeholder="Message"></textarea>
                                        </div>

                                        <div class="checkboxes-container">
                                            <div class="content-checkbox-container">
                                                <input type="checkbox" id="content-checkbox" class="required" name="consent" value="">
                                                <label for="content-checkbox"
                                                       class="input-wrapper consent-checkbox-label"><?php echo (get_field("consent_checkbox_paragraph")) ? get_field("consent_checkbox_paragraph") : "By messaging UCC Coffee UK &amp; Ireland you consent to us
                                                    contacting you about our services. We will never pass your contact details on to third parties."; ?></label>
                                            </div>

                                            <?php /* REMOVED AFTER REQUEST FROM THE CLIENT (17-09-2018) */ ?>
                                            <div class="sales-communication-container">
                                                <input type="checkbox" id="sales-comm-checkbox" name="sales-comm" value="">
                                                <label for="sales-comm-checkbox"
                                                       class="input-wrapper sales-communication-label"><?php echo (get_field("sales_communication_checkbox_paragraph")) ? get_field("sales_communication_checkbox_paragraph") : "By ticking this box, you are consenting to
                                                    receiving sales and
                                                    marketing communications from UCC Coffee UK &amp; Ireland ."; ?></label>
                                            </div>
                                            <?php /* */ ?>
                                        </div>
                                    </div>

                                    <div class="error-box alert alert-warning">
                                        <p>It has not been possible to elaborate your request, please try again later. </p>
                                    </div>

                                    <div id="submit-btn" class="submit-btn">
                                        <span>Send</span>
                                        <span class="ajax-loader-container">
                                            <i class="ajax-loader fa fa-spinner fa-spin"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form> -->
                    </div>
                    <!-- <div id="contact-us-form-response">
                        <img src="<?php echo get_template_directory_uri() . "/dist/images/icon-send.png"; ?>" alt="Confirmed"
                             class="img-fluid letter-confirm-img">
                        <h2><?php echo (get_field("success_message")) ? get_field("success_message") : "Thank you for your message..."; ?></h2>
                        <h3><?php echo (get_field("success_subtitle")) ? get_field("success_subtitle") : "One of our team will be in touch as soon as possible."; ?></h3>
                        <p><?php echo (get_field("success_paragraph")) ? get_field("success_paragraph") : ""; ?></p>
                        <a href="/" class="link-btn red-btn d-inline-block" aria-label="Home Link">
                            <span class="link-text">Home</span>
                        </a>
                        <a href="#" class="link-btn red-btn d-inline-block restart-form-btn" aria-label="New Message Link">
                            <span class="link-text">New Message</span>
                        </a>
                    </div> -->
                </div>
            </div>
        </div>
    </section>
    <section class="section-scrollable address-section">
        <div class="map-container thick-pad-l">
            <div class="iframe-container">
                <div class="mirror-quote-container text-left">
                    <div class="mirror-box">
                        <div class="mirror-box-line top"></div>
                        <div class="mirror-box-line right"></div>
                        <div class="mirror-box-line bottom"></div>
                        <div class="mirror-box-line left"></div>
                    </div>
                    <p class="quote"><?php echo (get_field("map_quote")) ? get_field("map_quote") : "Google <br/>Maps"; ?></p>
                    <div class="mirror-image-container">
                        <div class="mirror-image">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2455.9604729838475!2d-0.6929835840317926!3d52.00759818182751!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487654b08b24d413%3A0xa9d54bae0c1c966d!2sUCC+Coffee+UK+%26+Ireland!5e0!3m2!1sen!2suk!4v1530285147187"
                                    width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="address-row row">
            <div class="container-fluid container-fluid-cap">
                <div class="address-col col col-12 col-md-6 offset-md-6 common-pad-l">
                    <?php
                    if (have_rows('main_address')):
                        while (have_rows('main_address')) : the_row();
                            $row_type = get_row_layout();
                            switch ($row_type) {

                                case "guided_elements": ?>
                                    <img src="/wp-content/themes/cygnus_ucc/dist/images/icon-pin.png" alt="Pointer" class="pointer-img">
                                    <?php if (get_sub_field("office_name")) { ?>
                                        <div class="office-name"><?php echo get_sub_field("office_name") ?></div> <?php } ?>
                                    <?php if (get_sub_field("company_name")) { ?>
                                        <div class="company-name"><?php echo get_sub_field("company_name") ?></div> <?php } ?>
                                    <?php if (get_sub_field("address")) { ?>
                                        <div class="address"><?php echo get_sub_field("address") ?></div> <?php } ?>
                                    <?php if (get_sub_field("telephone")) { ?>
                                        <div class="telephone"><span class="red">T</span> <?php echo get_sub_field("telephone") ?></div> <?php } ?>
                                    <?php if (get_sub_field("fax")) { ?>
                                        <div class="fax"><span class="red">F</span> <?php echo get_sub_field("fax") ?></div> <?php } ?>
                                    <?php break;

                                case "free_textarea": ?>
                                    <div class="free-textarea"><?php echo get_sub_field("free_textarea"); ?></div>
                                    <?php break;
                            }
                        endwhile;
                    else:
                        echo "<div></div>";
                    endif;
                    ?>
                </div>
            </div>
        </div>

        <div class="additional-row row">
            <div class="container-fluid container-fluid-cap">
                <?php
                if (have_rows('additional_addresses')):
                    while (have_rows('additional_addresses')) : the_row();
                        $row_type = get_row_layout(); ?>
                        <div class="address-col col col-12 col-md-6 offset-md-6 common-pad-l">
                            <?php
                            switch ($row_type) {

                                case "guided_elements": ?>
                                    <?php if (get_sub_field("office_name")) { ?>
                                        <div class="office-name"><?php echo get_sub_field("office_name") ?></div> <?php } ?>
                                    <?php if (get_sub_field("company_name")) { ?>
                                        <div class="company-name"><?php echo get_sub_field("company_name") ?></div> <?php } ?>
                                    <?php if (get_sub_field("address")) { ?>
                                        <div class="address"><?php echo get_sub_field("address") ?></div> <?php } ?>
                                    <?php if (get_sub_field("telephone")) { ?>
                                        <div class="telephone"><span class="white">T</span> <?php echo get_sub_field("telephone") ?></div> <?php } ?>
                                    <?php if (get_sub_field("fax")) { ?>
                                        <div class="fax"><span class="white">F</span> <?php echo get_sub_field("fax") ?></div> <?php } ?>
                                    <?php break;

                                case "free_textarea": ?>
                                    <div class="free-textarea"><?php echo get_sub_field("free_textarea"); ?></div>
                                    <?php break;
                            } ?>
                        </div>
                    <?php
                    endwhile;
                else:
                    echo "<div></div>";
                endif;
                ?>
            </div>
        </div>
    </section>

<style>
    .faq_question .title, .form_title {
        font-size: 1.3rem !important;
        letter-spacing: 0.1rem !important;
        margin-top:20px !important;
        margin-bottom:10px !important;
    }
    .faq_question .question {
        font-size: 0.9rem !important;
    }

    .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=email], 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=tel], 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=text], 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl label+span>textarea, 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl label .error-msg, 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl label>span>textarea, 
    .page-template-template-contacts .content-row .form-col #footer_form_lbl select {
        margin-bottom:20px;
    }

    .page-template-template-contacts .content-row .form-col #footer_form_lbl .submit-btn, .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=email], .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=tel], .page-template-template-contacts .content-row .form-col #footer_form_lbl input[type=text], .page-template-template-contacts .content-row .form-col #footer_form_lbl textarea, .page-template-template-contacts .content-row .form-col #footer_form_lbl select {
        border: 2px solid #d51216;
        border-radius: 0;
        color: #333d47;
        background-color: transparent;
    }

    @media(min-width:768px) {
        .faq-col-1 {
            border-right:black solid 2px;
        }
        .faq-col-2 {
            padding-left:50px;
        }
        .faq-col-1 {
            padding-right:50px;
        }
    }
</style>

<?php endwhile; ?>
