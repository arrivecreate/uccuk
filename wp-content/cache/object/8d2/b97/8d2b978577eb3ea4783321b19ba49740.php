�QHa<?php exit; ?>a:1:{s:7:"content";a:16:{s:10:"_edit_lock";a:1:{i:0;s:14:"1621333572:956";}s:10:"_edit_last";a:1:{i:0;s:3:"950";}s:17:"_wp_page_template";a:1:{i:0;s:7:"default";}s:43:"_yoast_wpseo_estimated-reading-time-minutes";a:1:{i:0;s:0:"";}s:15:"primary_content";a:1:{i:0;s:446:"As part of this year’s #InternationalWomensDay we’re showcasing some of the amazing women that make up our diverse team. We’ve spoken to a selection of women from across the group to hear their thoughts on gender equality and female empowerment. We hear from:
<ul>
 	<li>Kirsty Pavely, Senior Brand Manager</li>
 	<li>Katherine McCarthy, Coffee Quality Specialist</li>
 	<li>Frannie Santos-Mawdsley, Senior Marketing Manager</li>
</ul>";}s:16:"_primary_content";a:1:{i:0;s:19:"field_5a9fdd86d7317";}s:17:"secondary_content";a:1:{i:0;s:5869:"<strong>The theme for IWD this year is #ChooseToChallenge - what does this mean to you, and how does UCC embrace gender equality in the workplace?</strong>

<strong>FSM: </strong>For me, it is about not accepting the status quo and being positive actors of change; not just in the workplace, but in society as a whole. I was brought up to believe we each matter as individuals and not to focus on nor accept societal brackets as determinant of who you can be or become, including gender. I am pleased to see many colleagues at UCC have a very similar view. This is exemplified by so many females which have been in the business for a long time and by females in senior roles in our business.

&nbsp;

<strong>It could be assumed that many roles in UCC, such as the engineers, will be male-dominated. How does UCC address this? </strong>

<strong>FSM: </strong>I have not been with the company long and, yet, have been pleasantly impressed by seeing women leading or being key players in areas such as Operations, Sales, etc which are often male-dominated. The business invests in training for all of us and opportunities are there for all colleagues – independent of gender, age or ethnicity.

&nbsp;

<strong>Tell us something you wish you'd known at the start of your career? </strong>

<strong>KM:</strong> The thing that would have helped me most is knowing that you don’t always need to be moving up to be progressing. Even when taking what I thought was a step backwards, this provided me with essential time to reflect on where I wanted to be and what I really wanted to do.

<strong>KP:</strong> Things might not always go to plan. I have found being agile and embracing change is exciting, change can challenge and develop you.

&nbsp;

<strong>What motivates you?
</strong>

<strong>KM</strong>: My biggest motivation is knowing I have made an impact in a positive way. This can be small or large, but I feel that success can often be measured by the impact you have made and the impression you leave.

<strong>FSM</strong>: Making myself, my family, and friends proud. Part of that is ensuring their belief in me is realised through a role in that makes me feel valued and where I can see I am adding value to the business and to my work colleagues.

<strong>KP</strong>: Being part of a team and collaborating with people across different functions and disciplines. Not only can you learn so much from others, you can accomplish so much more with a motivated team around you.

&nbsp;

<strong>What one piece of advice would you give women looking to excel in their careers? </strong>

<strong>KM</strong>: I believe that the key contributing factor for any success I have had in my career is passion. Having a passion for the job you are doing or the career you have chosen will mean that work doesn’t feel like work. I have found that at times my opinions may not have been listened to, but I have always strived to learn more, wanting to become 'the expert in the room’. You need to show that you have confidence in your opinions but can also back it up with the knowledge and experience you have gained.

<strong>FSM</strong>: Just be you. Do not allow anyone to judge your personality as a determinant of your talent and capability. Focus on learning and developing yourself both professionally and personally. And above all, be kind to yourself.

<strong>KP</strong>: Be your biggest fan, not your biggest critic. We are so quick to doubt ourselves, but we should use any experience as an opportunity to reflect on what went well and what we could do differently, it’s equally as important to acknowledge and celebrate the small wins too.

&nbsp;

<strong>Which female figure(s) inspire(s) you and why? </strong>

<strong>KP</strong>: There are many females, personally and professionally, that inspire me, but one individual who inspired me in the very early stages of my career was Dame Emma Walmsley, now CEO of GSK. When I worked in the business, she was leading the consumer brands division (something of interest to me) and everything always appeared effortless. She has since become the first female CEO of a pharmaceutical company, has been granted Dame-hood and been leading one of the world’s largest vaccine efforts.

<strong>KM</strong>: I think the most inspirational female figure in my life would be my mum – she may be the strongest person I know. She has always been a role model to me and has pushed me to succeed in the things I love to do. From a young age, I have always had a strong work ethic, and this has come from her. She has shown that it is possible to juggle a busy personal life along with striving to succeed in your chosen career.

<strong>FSM</strong>: It is going to be very corny, but my heroes are my Mother, Grandmother and Great grandmother. They were real examples of what #choosetochallenge is! My Grandmother was the daughter of a First Nation man, who became enslaved and was “freed” by a landowner who only had daughters – one of which was my great grandmother. She made it clear to her dad she was marrying my great grandfather. And from then came the strong, unconventional females in my family! My grandmother never married and single-handed brought up 4 kids in the 50s and 60s in Brazil. She was also a career woman and rose from being a Nanny to Operations Manager for the first Pate company in Brazil. My mum was one of the few women I knew growing up who was a mother, successful entrepreneur and devoted to charity work equally – and yet was not afraid to take time out just for herself or for her and my dad. They were married for 48 years until she passed in 2014. My hope is to be as brave, strong and loving as they were and to continue to challenge conventions and expectations both in my personal and professional lives!";}s:18:"_secondary_content";a:1:{i:0;s:19:"field_5a9fdd8fd7318";}s:5:"links";a:1:{i:0;s:0:"";}s:6:"_links";a:1:{i:0;s:19:"field_5a9ffab4ad916";}s:15:"secondary_image";a:1:{i:0;s:4:"6116";}s:16:"_secondary_image";a:1:{i:0;s:19:"field_5a9fdc4865e11";}s:5:"quote";a:1:{i:0;s:0:"";}s:6:"_quote";a:1:{i:0;s:19:"field_5a9fdc8265e12";}s:19:"contact_us_override";a:1:{i:0;s:1:"0";}s:20:"_contact_us_override";a:1:{i:0;s:19:"field_5d5a99590048b";}}}